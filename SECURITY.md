# Security Policy

## Supported Versions

We release patches for security vulnerabilities.
Which versions are eligible receiving such patches depend on the CVSS v3.0 Rating:

| CVSS v3.0 | Version | Supported          |
| --------- | ------- | ------------------ |
| 5.5-10.0  | > 1.1.x | :heavy_check_mark: |
| 7.5-10.0  |  1.1.x  | :heavy_check_mark: |
|   None    | < 1.0.x | :x:                |


## Reporting a Vulnerability

Please report (suspected) security vulnerabilities to security@royalrand.net.
You will receive a response from us within 48 hours. If the issue is confirmed,
we will release a patch as soon as possible depending on complexity but historically within a few days.
