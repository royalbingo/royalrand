/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This code is an adaptation of George Marsaglia, 1999 original implementation
 * written in C.
 * Source can be found at http://www.cs.yorku.ca/~oz/marsaglia-rng.html
 * 
 * And also an adaptation of KISS11 (MWC (Multiply-With-Carry) RNG), 2011
 * also by Marsaglia.
 * Source & details can be found at https://groups.google.com/g/sci.crypt/c/PLWrRP91jms/m/hWSG3WIAMDoJ
 * 
 * George Marsaglia <geo@stat.fsu.edu>
 */ 
#include <stdio.h>
#include <stdlib.h>
#include "include/kiss.h"


UL znew(kiss_state_t *state)
{
    state->z = 36969 * (state->z & 65535) + (state->z >> 16);
    return state->z;
}

UL wnew(kiss_state_t *state)
{
    state->w = 18000 * (state->w & 65535) + (state->w >> 16);
    return state->w;
}

UL shr3(kiss_state_t *state)
{
    state->jsr^=(state->jsr<<17);
    state->jsr^=(state->jsr>>13);
    state->jsr^=(state->jsr<<5);
    return state->jsr;
}

UL cong(kiss_state_t *state)
{
    state->jcong = 69069*state->jcong+1234567;
    return state->jcong;
}

UL mwc(kiss_state_t *state)
{
    return (znew(state) << 16) + wnew(state);
}

UL lfib4(kiss_state_t *state)
{
    state->c++;
    state->t[state->c] = state->t[state->c] + \
    state->t[UC(state->c+58)]+state->t[UC(state->c+119)] + \
    state->t[UC(state->c+178)];
    return state->t[state->c];
}

UL fib(kiss_state_t *state)
{
    state->b = state->a + state->b;
    state->a = state->b - state->a;
    return state->a;
}

UL swb(kiss_state_t *state)
{
    state->c++;
    state->bro = (state->x < state->y);
    state->x = state->t[UC(state->c+34)];
    state->y = state->t[UC(state->c+19)] + state->bro;
    state->t[state->c] = state->x - state->y;
    return state->t[state->c];
}

UL kiss(kiss_state_t *state)
{
    return (mwc(state) ^ cong(state)) + shr3(state);
}


void settable(kiss_state_t *state, UL i1,UL i2,UL i3,UL i4,UL i5, UL i6)
{
    state->z=i1;state->w=i2,state->jsr=i3;
    state->jcong=i4; state->a=i5; state->b=i6;
    for(int i=0;i<256;i++) state->t[i]=kiss(state);
}

kiss_state_t *create_kiss(void)
{
    kiss_state_t *state = malloc(sizeof(kiss_state_t));
    if (state == NULL) { return NULL; }

    state->a = 224466889; state->b = 7584631; state->c = 0;
    state->v = 2463534242; state->w = 521288629; state->x = 0;
    state->y = 0; state->z = 362436069; state->jsr = 123456789;
    state->jcong = 380116160;
    settable(state, 12345,65435,34221,12345,9983651,95746118);

    return state;
}

void destroy_kiss(kiss_state_t *kiss_state) {
    if (kiss_state != NULL) {
        free(kiss_state);
    }
}

int kiss_self_test(void)
{
    kiss_state_t *state = create_kiss();

    int i;
    UL k;
    
    /* 32-bit only */
    printf("This is a test procedure of KISS program. It should compile and print 7 0's.\n");
    for(i=1;i<1000001;i++){k=lfib4(state);} printf("lfib4 \t= %lu\n", k-1064612766U);
    for(i=1;i<1000001;i++){k=swb(state) ;} printf("swb \t= %lu\n", k- 627749721U);
    for(i=1;i<1000001;i++){k=kiss(state) ;} printf("kiss \t= %lu\n", k-1372460312U);
    for(i=1;i<1000001;i++){k=cong(state) ;} printf("cong \t= %lu\n", k-1529210297U);
    for(i=1;i<1000001;i++){k=shr3(state) ;} printf("shr3 \t= %lu\n", k-2642725982U);
    for(i=1;i<1000001;i++){k=mwc(state) ;} printf("mwc \t= %lu\n", k- 904977562U);
    for(i=1;i<1000001;i++){k=fib(state) ;} printf("fib \t= %lu\n", k-3519793928U);


    destroy_kiss(state);
    return 0;
}

/*
 KISS11 adapt. (MWC, George Marsaglia, 2011 impl.)
*/

unsigned long b32MWC(b32MWC_t *self)
{
    unsigned long t,x;

    self->j = (self->j+1) & 4194303;
    x = self->Q[self->j];
    t = (x<<28) + self->carry;
    self->carry = (x>>4)-(t<x);

    return (self->Q[self->j] = t-x);
}


unsigned long b32MWC_KISS(b32MWC_t *self)
{
    return b32MWC(self) + CNG + XS;
}

b32MWC_t *create_b32MWC(void)
{
    b32MWC_t *self = malloc(sizeof(b32MWC_t));
    if (self == NULL) { exit(1); }

    self->cng = 123456789;
    self->xs = 362436069;
    self->j = 4194303;
    self->carry = 0;

    for (int i = 0; i < 4194304; i++) self->Q[i] = CNG + XS;

    return self;
}

void destroy_b32MWC(b32MWC_t *state)
{
    if (state != NULL) {
        free(state);
    }
}

int b32MWC_self_test(void)
{
    unsigned long x;
    unsigned long i;

    b32MWC_t *mwc = create_b32MWC();

    /* Then generate 10^9 b32MWC()s */
    for(i=0;i<1000000000;i++) x=b32MWC(mwc);
    printf("Does x=2769813733 ?\n x=%lu\n",x);

    /* followed by 10^9 KISSes: */
    for(i=0;i<1000000000;i++) x=b32MWC_KISS(mwc);
    printf("Does x=3545999299 ?\n x=%lu\n",x);

    destroy_b32MWC(mwc);
    return 0;
}