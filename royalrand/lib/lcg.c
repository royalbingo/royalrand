/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo@bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "include/lcg.h"


lcg_ctx* lcg(uint64_t a, uint64_t c, uint64_t m, uint64_t seed) {

    lcg_param params = {
        m, a, c
    };

    lcg_ctx* context = malloc(sizeof(lcg_ctx));

    if (context == NULL) {
        perror("lcg(): Cannot allocate memory!\n");
        return NULL;
    }

    context->seed = seed;
    context->param = params;

    return context;
}

lcg_ctx* lcg_create(void)
{
    lcg_ctx *context = malloc(sizeof(lcg_ctx));
    if (context == NULL) {
        perror("lcg(): Cannot allocate memory!\n");
        return NULL;
    }
    return context;
}

int lcg_set_param(lcg_ctx *context, uint64_t a, uint64_t c, uint64_t m, uint64_t seed)
{
    if (context == NULL)
        return -1;
    
    lcg_param params = {
        m, a, c
    };
    
    context->param = params;
    context->seed = seed;

    return 0;
}

void lcg_free(lcg_ctx* context) {
    free(context);
}

uint64_t lcg_step(lcg_ctx* context) {
    context->seed = ((context->param.a * context->seed + context->param.c) % context->param.m);
    return context->seed;
}

float lcg_float_step(lcg_ctx* context) {
    return lcg_step(context) / (float)(context->param.m);
}
