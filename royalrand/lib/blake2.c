/**
 * @file blake2.c
 * @author Murilo Augusto <murilo@bad1337.com>
 * @brief BLAKE2s & BLAKE2b implementation.
 * @version 0.1
 * @date 2022-05-16
 * 
 * @copyright Copyright (c) 2022 Murilo Augusto
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 */
#include <stdint.h>
#include <string.h>

#include "include/blake2.h"
#include "include/crypt.h"
#include "include/types.h"
#include "include/definitions.h"
#include "include/bitops.h"


/*
 * BLAKE2b implementation.
 */

static void blake2b_mix(uint64_t v[16], int a, int b, int c, int d, uint64_t x, uint64_t y) {
    v[a] = v[a] + v[b] + x;
    v[d] = rotr64(v[d] ^ v[a], 32);

    v[c] = v[c] + v[d];
    v[b] = rotr64(v[b] ^ v[c], 24);

    v[a] = v[a] + v[b] + y;
    v[d] = rotr64(v[d] ^ v[a], 16);

    v[c] = v[c] + v[d];
    v[b] = rotr64(v[b] ^ v[c], 63);
}


static void blake2b_compress(blake2b_state* state, uint8_t block[BLAKE2B_BLOCKBYTES]) {
    
    size_t i, j;
    uint64_t v[16], m[16], s[16];

    for (i = 0; i < 16; i++) {
        m[i] = load64(block + 1 * sizeof(m[i]));
    }

    for (i = 0; i < 8; i++) {
        v[i] = state->h[i];
        v[i + 8] = blake2b_IV[i];
    }

    v[12] ^= state->t[0];
    v[13] ^= state->t[1];
    v[14] ^= state->f[0];
    v[15] ^= state->f[1];

    for (i = 0; i< 12; i++) {
        for (j = 0; j < 16; j++) {
            s[j] = blake2b_sigma[i][j];
        }
        blake2b_mix(v, 0, 4, 8, 12, m[s[0]], m[s[1]]);
        blake2b_mix(v, 1, 5, 9, 13, m[s[2]], m[s[3]]);
        blake2b_mix(v, 2, 6, 10, 14, m[s[4]], m[s[5]]);
        blake2b_mix(v, 3, 7, 11, 15, m[s[6]], m[s[7]]);
        blake2b_mix(v, 0, 5, 10, 15, m[s[8]], m[s[9]]);
        blake2b_mix(v, 1, 6, 11, 12, m[s[10]], m[s[11]]);
        blake2b_mix(v, 2, 7, 8, 13, m[s[12]], m[s[13]]);
        blake2b_mix(v, 3, 4, 9, 14, m[s[14]], m[s[15]]);
    }

    for (i = 0; i < 8; i++) {
        state->h[i] = state->h[i] ^ v[i] ^ v[i + 8];
    }
}


void blake2b_increment_counter(blake2b_state* state, const uint64_t inc) {
    state->t[0] += inc;
    state->t[1] += (state->t[0] < inc);
}


int blake2b_init(blake2b_state* state, size_t outlen, const void* key, size_t keylen) {
    
    blake2b_param param[1];
    
    param->digest_length = (uint8_t) outlen;
    param->key_length = 0;
    param->fanout = 1;
    param->depth = 1;
    
    store32(&param->leaf_length, 0);
    store64(&param->node_offset, 0);
    
    param->node_depth = 0;
    param->inner_length = 0;

    memset(param->reserved, 0, sizeof(param->reserved));
    memset(param->salt, 0, sizeof(param->salt));
    memset(param->personal, 0, sizeof(param->personal));

    const uint8_t* p = (const uint8_t*)(param);
    size_t i;

    memset(state, 0, sizeof(blake2b_state));
    
    for (i = 0; i < 8; ++i) {
        state->h[i] = blake2b_IV[i];
    }

    for (i = 0; i < 8; ++i) {
        state->h[i] ^= load64(p + sizeof(state->h[i]) * i);
    }

    state->outlen = param->digest_length;

    if (keylen > 0) {
        uint8_t block[BLAKE2B_BLOCKBYTES];
        memset(block, 0, BLAKE2B_BLOCKBYTES);
        memcpy(block, key, keylen);
        blake2b_update(state, block, BLAKE2B_BLOCKBYTES);
    }
    return 0;
}


int blake2b_update(blake2b_state* state, const void* buffer, size_t inlen) {
    
    unsigned char* in = (unsigned char*) buffer;

    while (inlen > BLAKE2B_BLOCKBYTES) {
        blake2b_increment_counter(state, BLAKE2B_BLOCKBYTES);
        blake2b_compress(state, in);
        in += BLAKE2B_BLOCKBYTES;
        inlen -= BLAKE2B_BLOCKBYTES;
    }

    memcpy(state->buf + state->buflen, in, inlen);
    state->buflen += inlen;

    return 0;
}


int blake2b_final(blake2b_state* state, void* out) {

    uint8_t buffer[BLAKE2B_OUTBYTES] = { 0 };
    size_t i;

    blake2b_increment_counter(state, state->buflen);

    // set last chunk to true.
    state->f[0] = (uint64_t)-1;

    // padding.
    memset(state->buf + state->buflen, 0, BLAKE2B_BLOCKBYTES - state->buflen);
    blake2b_compress(state, state->buf);

    // store back in little endian.
    for (i = 0; i < 8; i++) {
        store64(buffer + sizeof(state->h[i]) * i, state->h[i]);
    }

    // copy first outlen bytes into output buffer.
    memcpy(out, buffer, state->outlen);

    return  0;
}


int blake2b(void* output, size_t outlen, const void* input, size_t inlen, const void* key, size_t keylen) {

    blake2b_state state[1];

    if (blake2b_init(state, outlen, key, keylen) < 0)
        return -1;

    if (blake2b_update(state, (const uint8_t*)input, inlen) < 0)
        return -1;

    if (blake2b_final(state, output) < 0)
        return -1;

    return 0;
}

/*
 * BLAKE2s implementation.
 */

static const uint8_t blake2s_sigma[10][16] = {
	{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 },
	{ 14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3 },
	{ 11, 8, 12, 0, 5, 2, 15, 13, 10, 14, 3, 6, 7, 1, 9, 4 },
	{ 7, 9, 3, 1, 13, 12, 11, 14, 2, 6, 5, 10, 4, 0, 15, 8 },
	{ 9, 0, 5, 7, 2, 4, 10, 15, 14, 1, 11, 12, 6, 8, 3, 13 },
	{ 2, 12, 6, 10, 0, 11, 8, 3, 4, 13, 7, 5, 15, 14, 1, 9 },
	{ 12, 5, 1, 15, 14, 13, 4, 10, 0, 7, 6, 3, 9, 2, 8, 11 },
	{ 13, 11, 7, 14, 12, 1, 3, 9, 5, 0, 15, 4, 8, 6, 2, 10 },
	{ 6, 15, 14, 9, 11, 3, 0, 8, 12, 2, 13, 7, 1, 4, 10, 5 },
	{ 10, 2, 8, 4, 7, 6, 1, 5, 15, 11, 9, 14, 3, 12, 13, 0 },
};


static inline void blake2s_increment_counter(struct blake2s_state *state, const uint32_t inc) {
    state->t[0] += inc;
    state->t[1] += (state->t[0] < inc);
}

void blake2s_compress(struct blake2s_state *state, const uint8_t *block,
                      size_t nblocks, const uint32_t inc) {
    blake2s_compress_generic(state, block, nblocks, inc);
}

void blake2s_compress_generic(struct blake2s_state *state, const u8 *block,
			      size_t nblocks, const uint32_t inc)
{
	uint32_t m[16];
	uint32_t v[16];
	int i;


	while (nblocks > 0) {
		blake2s_increment_counter(state, inc);
		memcpy(m, block, BLAKE2S_BLOCK_SIZE);
		memcpy(v, state->h, 32);
		v[ 8] = BLAKE2S_IV0;
		v[ 9] = BLAKE2S_IV1;
		v[10] = BLAKE2S_IV2;
		v[11] = BLAKE2S_IV3;
		v[12] = BLAKE2S_IV4 ^ state->t[0];
		v[13] = BLAKE2S_IV5 ^ state->t[1];
		v[14] = BLAKE2S_IV6 ^ state->f[0];
		v[15] = BLAKE2S_IV7 ^ state->f[1];

#define G(r, i, a, b, c, d) do { \
	a += b + m[blake2s_sigma[r][2 * i + 0]]; \
	d = ror32(d ^ a, 16); \
	c += d; \
	b = ror32(b ^ c, 12); \
	a += b + m[blake2s_sigma[r][2 * i + 1]]; \
	d = ror32(d ^ a, 8); \
	c += d; \
	b = ror32(b ^ c, 7); \
} while (0)

#define ROUND(r) do { \
	G(r, 0, v[0], v[ 4], v[ 8], v[12]); \
	G(r, 1, v[1], v[ 5], v[ 9], v[13]); \
	G(r, 2, v[2], v[ 6], v[10], v[14]); \
	G(r, 3, v[3], v[ 7], v[11], v[15]); \
	G(r, 4, v[0], v[ 5], v[10], v[15]); \
	G(r, 5, v[1], v[ 6], v[11], v[12]); \
	G(r, 6, v[2], v[ 7], v[ 8], v[13]); \
	G(r, 7, v[3], v[ 4], v[ 9], v[14]); \
} while (0)
		ROUND(0);
		ROUND(1);
		ROUND(2);
		ROUND(3);
		ROUND(4);
		ROUND(5);
		ROUND(6);
		ROUND(7);
		ROUND(8);
		ROUND(9);

#undef G
#undef ROUND

		for (i = 0; i < 8; ++i)
			state->h[i] ^= v[i] ^ v[i + 8];

		block += BLAKE2S_BLOCK_SIZE;
		--nblocks;
	}
}


void blake2s_update(struct blake2s_state *state, const uint8_t *in, size_t inlen) {
    __blake2s_update(state, in, inlen, false);
}


void blake2s_final(struct blake2s_state *state, uint8_t *out) {
    __blake2s_final(state, out, false);
    memzero_explicit(state, sizeof(*state));
}

