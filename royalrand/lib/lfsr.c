/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo@bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "include/lfsr.h"

/**
 * @brief Create a context for LFSR.
 * 
 * @param state (uint64_t) inintial state.
 * @return lfsr_ctx* instance.
 */
lfsr_ctx* lfsr(uint64_t state) {

    lfsr_ctx* ctx = malloc(sizeof(lfsr_ctx));

    if (ctx == NULL) {
        perror("lfsr(): Cannot allocate memory!\n");
        exit(1);
    }

    ctx->state = state;
    
    return ctx;
}

/**
 * @brief Perform a step of 64 bits (uint64_t) using the current
 * state in context (lfsr).
 * 
 * @param context (lfsr_ctx*) instance.
 * @return uint64_t 64-bit integer step result.
 */
uint64_t lfsr_step(lfsr_ctx* context) {
    
    unsigned int counter = 0;
    uint64_t result = 0;

    while (counter < 64) {
        result = (result << 1) | (context->state & 1);
        context->state = (context->state >> 1) |
                         (((context->state ^ (context->state >> 1)) & 1) << 3);
        counter++;
    }
    return result;
}

/**
 * @brief Free allocated memory for LFSR context.
 * 
 * @param context (lfsr_ctx*) instance.
 */
void lfsr_free(lfsr_ctx *context) {
    context->state = 0;
    free(context);
    context = NULL;
}

/**
 * @brief Perform a fast LFSR with the specified state. This function limits the
 * output to 64-bit integer.
 * 
 * @param _state (uint64_t) initial state.
 * @param nbits (unsigned int) bits wanted.
 * @return (uint64_t) integer with nbits. 
 */
uint64_t lfsr64_fast(uint64_t _state, unsigned int nbits) {

    if (nbits > 64) return 0;

    uint64_t result = 0;
    uint64_t counter = 0;
    uint64_t newbit = 0;
    uint64_t state = _state;
    
    while (counter < nbits) {

        result = (result << 1) | (state & 1);
        newbit = (state ^ (state >> 1)) & 1;
        state = (state >> 1) | (newbit << 3);
        counter++;
    }

    return result;
}

/**
 * @brief Perform a fast LFSR with the specified state. This function limits the
 * output to 32-bit integer.
 * 
 * @param _state (uint64_t) initial state.
 * @param nbits (unsigned int) bits wanted.
 * @return (uint32_t) 32-bit integer. 
 */
uint32_t lfsr_fast(uint32_t _state, unsigned int nbits) {

    if (nbits > 32) return 0;

    uint32_t result = 0;
    uint32_t counter = 0;
    uint32_t newbit = 0;
    uint32_t state = _state;
    
    while (counter < nbits) {

        result = (result << 1) | (state & 1);
        newbit = (state ^ (state >> 1)) & 1;
        state = (state >> 1) | (newbit << 3);
        counter++;
    }

    return result;
}
