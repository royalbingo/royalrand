/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo@bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <time.h>
#include "include/debug.h"

const char* get_time(void) {
    
    time_t rawtime;
    struct tm * timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);

    return (const char*) asctime(timeinfo);
}

clock_t get_clock(void) {
    return clock();
}

int warning(const char* message) {
    return printf("%s[WARN]%s %s", KYEL, KNRM, message);
}

int info(const char* message) {
    return printf("%s[INFO]%s %s", KBLU, KNRM, message);
}

int error(const char* message) {
    return printf("%s[ERROR]%s %s", KRED, KNRM, message);
}

int debug(const char* message) {
    return printf("%s[DEBUG] %s%s", KCYN, message, KNRM);
}
