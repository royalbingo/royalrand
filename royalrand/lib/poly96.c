/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo@bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include "include/poly96.h"

/**
 * @brief Poly96 - A polynomial LCG implementation.
 * 
 * The state vector must not be all zeroes.
 * 
 * Default values for the state vector could be:
 *      state0 = 0x1UL, state1 = 0x0UL, state2 = 0x0UL;
 * 
 * @param state0 State 0.
 * @param state1 State 1.
 * @param state2 State 2.
 * @return (stateVector_t*) Context of Poly96.
 */
stateVector_t* poly96(unsigned long state0, unsigned long state1, unsigned long state2) {
    
    stateVector_t* s = malloc(sizeof(stateVector_t));

    if (s == NULL) {
        perror("poly96(): Cannot allocate memory!\n");
        exit(1);
    }

    s->state0 = state0;
    s->state1 = state1;
    s->state2 = state2;

    return s;
}

/**
 * @brief Free the Poly96 context (state vector).
 * 
 * @param context (stateVector_t*) instance.
 */
void poly96_free(stateVector_t* context) {
    
    context->state0 = 0;
    context->state1 = 0;
    context->state2 = 0;

    free(context);
    context = NULL;
}

double poly96_step(stateVector_t* context) {

    unsigned long e, y0, y1, y2, w0, w1, w2;

    /* Bitwise rotation by 71 bits to the left */
    w0 = (context->state0 >> 25) ^ (context->state2 << 7);
    w1 = (context->state1 >> 25) ^ (context->state0 << 7);
    w2 = (context->state2 >> 25) ^ (context->state1 << 7);

    /* Elimination of bit 84 and verification of bit 59 */
    w2 = w2 & 0xfffff7ffUL;

    if (context->state1 & 0x00000010UL) {
        context->state0 = w0 ^ a0; context->state1 = w1 ^ a1; context->state2 = w2 ^ a2;
    }
    else {
        context->state0 = w0; context->state1 = w1; context->state2 = w2;
    }

    /* Self-tempering with c=32 and d=10 */
    e = (context->state0 ^ context->state1 ^ context->state2 ) << 10;
    y0 = context->state0 ^ e; y1 = context->state1 ^ e; y2 = context->state2 ^ e;

    /* MK-tempering with s1=23 and s2=47 */
    y0 = mk_tempering(y0, y1, y2);

    return (y0 * 2.3283064365e-10);
}

/**
 * @brief MK-tempering with s1=23 and s2=47.
 */
unsigned long mk_tempering(unsigned long y0, unsigned long y1, unsigned long y2) {
    y0 = y0 ^ (((y1 >> 9) ^ (y0 << 23)) & B10);
    y1 = y1 ^ (((y2 >> 9) ^ (y1 << 23)) & B11);
    y2 = y2 ^ ((y2 << 23) & B12);
    y0 = y0 ^ (((y2 >> 17) ^ (y1 << 15)) & B20);
    /* y1 = y1 ˆ ((y2 << 15) & B21); */
    return y0;
}
