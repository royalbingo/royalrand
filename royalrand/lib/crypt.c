/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo@bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <linux/sysctl.h>
#include <linux/random.h>

#include "include/rdrand.h"
#include "include/crypt.h"
#include "include/definitions.h"
#include "include/bitops.h"

#define MINIMUM(a, b) (((a) < (b)) ? (a) : (b))

/**
 * @brief Perform a hardware random number generator sanity check.
 * @return int (status) Returns 1 if OK otherwise 0.
 */
int __rdrand_sanity_check(void) {
    
    uint32_t number, prev;
    uint32_t repeated = 0;

    for (unsigned int i = 0; i < HARDWARE_SANITY_LOOP; i++) {
        rdrand_get_uint32_retry(RDRAND_RETRY, &number);

        if (prev == number)
            repeated++;

        prev = number;
    }

    if (repeated) return 0;
    return 1;
}

int __get_urandom_uint32(void *buf, size_t len) {
    
    struct stat st;
    size_t i;
    int fd, cnt, flags;
    int save_errno = errno;
start:
    flags = O_RDONLY;
#ifdef O_NOFOLLOW
    flags |= O_NOFOLLOW;
#endif
#ifdef O_CLOEXEC
    flags |= O_CLOEXEC;
#endif
    fd = open("/dev/urandom", flags, 0);
    if (fd == -1) {
        if (errno == EINTR)
            goto start;
        goto nodevrandom;
    }
#ifndef O_CLOEXEC
    fcntl(fd, F_SETFD, fcntl(fd, F_GETFD) | FD_CLOEXEC);
#endif
    
	if (fstat(fd, &st) == -1 || !S_ISCHR(st.st_mode)) {
        close(fd);
        goto nodevrandom;
    }
    
    if (ioctl(fd, RNDGETENTCNT, &cnt) == -1) {
        close(fd);
        goto nodevrandom;
    }

    for (i = 0; i < len; i++) {
        size_t wanted = len - i;    
        ssize_t ret = read(fd, (char*)buf + i, wanted);

        if (ret == -1) {
            if (errno == EAGAIN || errno == EINTR)
                continue;
            close(fd);
            goto nodevrandom;
        }
        i += ret;
    }
    close(fd);
    errno = save_errno;
    return 0;

nodevrandom:
    errno = EIO;
    return -1;
}

/**
 * @brief Get the urandom uint32 value.
 * 
 * @param number (uint32_t*) destination.
 */
void get_urandom_uint32(uint32_t *number) {
    __get_urandom_uint32(number, sizeof(uint32_t));
}

void get_external_uint32(uint32_t *number) {
#ifdef EXTERNAL
#else
    get_urandom_uint32(number);
#endif
}

/**
 * @brief Try to fetch 32-bit integer using Intel rdrand implementation;
 * 
 * @param number (uint32_t*) destination.
 */
void get_hardware_uint32(uint32_t *number) {

#ifdef USE_HARDWARE_RNG
    /* This will determine the status of the hardware generator.
       (-1) means unitializied. (1) means OK and (0) means DO NOT USE.*/
    static volatile char __hardware_rng_status = -1;
#endif

#ifdef USE_HARDWARE_RNG

    if (__hardware_rng_status == -1)
        __hardware_rng_status = __rdrand_sanity_check();
    

    if (rdrand_check_support() == 1 &&
        rdseed_check_support() == 1 &&
        __hardware_rng_status == 1) {
        rdrand_get_uint32_retry(RDRAND_RETRY, number);
    } else {
        get_urandom_uint32(number);
    }

#else
    get_urandom_uint32(number);
#endif
}

/**
 * @brief Fetch WIDTH bytes from hardware non-deterministic random
 * bit generator (RDSEED). This function returns a non-zero value
 * if the instruction isn't available.
 * 
 * @param dest Destination pointer (should not be NULL and be at least WIDTH size).
 * @param width Width in blocks of 4 bytes each.
 * @return int -- non-zero value means unavailability of the NDRBG or an error.
 */
int get_hardware_seed(uint32_t *dest, uint32_t width)
{
    if (dest == NULL) { return -1; }

    if (rdrand_check_support() == 1 && rdseed_check_support() == 1) {
        if (rdseed_get_n_uint32_retry(width, 10, dest) == 1)
            return 0;
    } else {
        return 2;
    }

    return 1;
}

/**
 * @brief Get epoch time.
 * 
 * @return uint32_t epoch value.
 */
uint32_t get_epoch(void)
{
    return (uint32_t) time(NULL);
}

/* Here we will use rand(). This part doesn't really matter.
 * We will xor everything later.*/
void get_ns_uint32(uint32_t *number) {
    
    uint32_t seed;
    get_urandom_uint32(&seed);
    srand(seed);

    // rand() must be at least 0x800.
    *number = mersenne_temper(rand() % UINT32_MAX);
}

/**
 * @brief Get uint32_t entropy from the epoch time.
 * 
 * @param number uint32_t* destination.
 */
void get_time_entropy(uint32_t *number) {
    *number = time(NULL) & 0xffffffffUL;
}

/**
 * @brief Write `len` random (syscall getrandom) bytes
 * to the buffer pointed by `buf`.
 * 
 * @param buf (void*) output buffer.
 * @param len (size_t) bytes.
 * @return int (status code)
 */
int get_linux_random(void* buf, size_t len) {
	
    int pre_errno = errno;
	int ret;

	if (len > 256)
		return (-1);
	do {
		ret = syscall(SYS_getrandom, buf, len, GRND_NONBLOCK);
	} while (ret == -1 && errno == EINTR);

	if ((size_t)ret != len)
		return (-1);
	errno = pre_errno;

	return (0);
}

/**
 * @brief Get the entropy sysctl object
 * 
 * @param buf destination buffer.
 * @param len length.
 * @return int -- non-zero on error.
 */
int get_entropy_sysctl(void *buf, size_t len)
{
	static int mib[] = { CTL_KERN, KERN_RANDOM, RANDOM_UUID };
	size_t i;
	int save_errno = errno;

	for (i = 0; i < len; ) {
		size_t chunk = MINIMUM(len - i, 16);

		/* SYS__sysctl because some systems already removed sysctl() */
		struct __sysctl_args args = {
			.name = mib,
			.nlen = 3,
			.oldval = (char *)buf + i,
			.oldlenp = &chunk,
		};
		if (syscall(SYS__sysctl, &args) != 0)
			goto sysctlfailed;
		i += chunk;
	}
	errno = save_errno;
	return (0);			/* satisfied */
sysctlfailed:
	errno = EIO;
	return (-1);
}

/* utils */

uint64_t load64(const void* src) {
#ifdef LITTLE_ENDIAN
    uint64_t word;
    memcpy(&word, src, sizeof(word));
    return word;
#else
    const uint8_t* p = (const uint8_t*)src;
    return ((uint64_t)(p[0]) << 0)  | ((uint64_t)(p[1]) << 8)  |
           ((uint64_t)(p[2]) << 16) | ((uint64_t)(p[3]) << 24) |
           ((uint64_t)(p[4]) << 32) | ((uint64_t)(p[5]) << 40) |
           ((uint64_t)(p[6]) << 48) | ((uint64_t)(p[7]) << 56);
#endif
}


void store64(void* dest, uint64_t word) {
#ifdef LITTLE_ENDIAN
    memcpy(dest, &word, sizeof(word));
#else
    uint8_t* p = (uint8_t*)dest;
    p[0] = (uint8_t)(word >> 0);
    p[1] = (uint8_t)(word >> 8);
    p[2] = (uint8_t)(word >> 16);
    p[3] = (uint8_t)(word >> 24);
    p[4] = (uint8_t)(word >> 32);
    p[5] = (uint8_t)(word >> 40);
    p[6] = (uint8_t)(word >> 48);
    p[7] = (uint8_t)(word >> 56);
#endif
}


void store32(void* dest, uint32_t word) {
#ifdef LITTLE_ENDIAN
    memcpy(dest, &word, sizeof(word));
#else
    uint8_t* p = (uint8_t*)dest;
    p[0] = (uint8_t)(word >> 0);
    p[1] = (uint8_t)(word >> 8);
    p[2] = (uint8_t)(word >> 16);
    p[3] = (uint8_t)(word >> 24);
#endif
}

void _pool32mt_block_swap(uint32_t* x) {

    SWAP(x[ 79], x[514]);  SWAP(x[150], x[482]);  SWAP(x[291], x[452]);  SWAP(x[  3], x[402]);  
    SWAP(x[ 35], x[476]);  SWAP(x[287], x[617]);  SWAP(x[239], x[327]);  SWAP(x[  8], x[552]);  
    SWAP(x[308], x[323]);  SWAP(x[217], x[415]);  SWAP(x[184], x[615]);  SWAP(x[ 12], x[468]);  
    SWAP(x[203], x[448]);  SWAP(x[116], x[436]);  SWAP(x[ 81], x[531]);  SWAP(x[ 22], x[331]);  
    SWAP(x[193], x[456]);  SWAP(x[162], x[324]);  SWAP(x[145], x[473]);  SWAP(x[ 30], x[534]);  
    SWAP(x[306], x[583]);  SWAP(x[ 43], x[369]);  SWAP(x[ 27], x[427]);  SWAP(x[ 33], x[550]);  
    SWAP(x[ 23], x[449]);  SWAP(x[147], x[347]);  SWAP(x[ 47], x[575]);  SWAP(x[ 37], x[340]);  
    SWAP(x[218], x[591]);  SWAP(x[ 31], x[495]);  SWAP(x[ 77], x[577]);  SWAP(x[ 39], x[569]);  
    SWAP(x[191], x[505]);  SWAP(x[ 44], x[547]);  SWAP(x[ 60], x[561]);  SWAP(x[ 42], x[623]);  
    SWAP(x[248], x[398]);  SWAP(x[ 90], x[394]);  SWAP(x[  2], x[595]);  SWAP(x[ 45], x[500]);  
    SWAP(x[135], x[540]);  SWAP(x[260], x[528]);  SWAP(x[  6], x[443]);  SWAP(x[ 46], x[450]);  
    SWAP(x[ 15], x[602]);  SWAP(x[227], x[381]);  SWAP(x[238], x[413]);  SWAP(x[ 50], x[497]);  
    SWAP(x[301], x[594]);  SWAP(x[131], x[428]);  SWAP(x[299], x[493]);  SWAP(x[ 52], x[557]);  
    SWAP(x[137], x[423]);  SWAP(x[279], x[568]);  SWAP(x[189], x[596]);  SWAP(x[ 53], x[567]);  
    SWAP(x[167], x[343]);  SWAP(x[ 48], x[345]);  SWAP(x[ 73], x[403]);  SWAP(x[ 61], x[437]);  
    SWAP(x[ 86], x[492]);  SWAP(x[139], x[543]);  SWAP(x[204], x[605]);  SWAP(x[ 64], x[580]);  
    SWAP(x[295], x[430]);  SWAP(x[ 36], x[581]);  SWAP(x[ 95], x[358]);  SWAP(x[ 65], x[517]);  
    SWAP(x[176], x[434]);  SWAP(x[177], x[498]);  SWAP(x[114], x[480]);  SWAP(x[ 68], x[384]);  
    SWAP(x[ 67], x[527]);  SWAP(x[247], x[560]);  SWAP(x[187], x[488]);  SWAP(x[ 70], x[545]);  
    SWAP(x[ 66], x[541]);  SWAP(x[122], x[388]);  SWAP(x[201], x[336]);  SWAP(x[ 75], x[565]);  
    SWAP(x[ 99], x[588]);  SWAP(x[275], x[455]);  SWAP(x[  7], x[551]);  SWAP(x[ 76], x[383]);  
    SWAP(x[267], x[555]);  SWAP(x[142], x[609]);  SWAP(x[236], x[399]);  SWAP(x[ 82], x[597]);  
    SWAP(x[108], x[483]);  SWAP(x[200], x[312]);  SWAP(x[270], x[598]);  SWAP(x[ 89], x[395]);  
    SWAP(x[302], x[472]);  SWAP(x[183], x[607]);  SWAP(x[253], x[592]);  SWAP(x[ 94], x[454]);  
    SWAP(x[ 55], x[429]);  SWAP(x[254], x[510]);  SWAP(x[182], x[444]);  SWAP(x[ 98], x[604]);  
    SWAP(x[ 16], x[586]);  SWAP(x[165], x[338]);  SWAP(x[255], x[506]);  SWAP(x[102], x[374]);  
    SWAP(x[104], x[407]);  SWAP(x[300], x[471]);  SWAP(x[ 13], x[503]);  SWAP(x[111], x[521]);  
    SWAP(x[138], x[379]);  SWAP(x[ 72], x[496]);  SWAP(x[169], x[421]);  SWAP(x[113], x[351]);  
    SWAP(x[120], x[613]);  SWAP(x[ 69], x[466]);  SWAP(x[ 21], x[401]);  SWAP(x[126], x[440]);  
    SWAP(x[262], x[524]);  SWAP(x[192], x[378]);  SWAP(x[180], x[519]);  SWAP(x[128], x[318]);  
    SWAP(x[266], x[544]);  SWAP(x[232], x[339]);  SWAP(x[264], x[611]);  SWAP(x[136], x[564]);  
    SWAP(x[235], x[458]);  SWAP(x[222], x[603]);  SWAP(x[242], x[606]);  SWAP(x[140], x[558]);  

    SWAP(x[ 56], x[538]);  SWAP(x[210], x[508]);  SWAP(x[ 25], x[526]);  SWAP(x[141], x[562]);  
    SWAP(x[  1], x[620]);  SWAP(x[132], x[518]);  SWAP(x[198], x[451]);  SWAP(x[143], x[536]);  
    SWAP(x[ 83], x[499]);  SWAP(x[ 24], x[432]);  SWAP(x[  4], x[416]);  SWAP(x[146], x[599]);  
    SWAP(x[214], x[523]);  SWAP(x[ 57], x[380]);  SWAP(x[123], x[490]);  SWAP(x[148], x[579]);  
    SWAP(x[197], x[361]);  SWAP(x[103], x[442]);  SWAP(x[282], x[411]);  SWAP(x[152], x[337]);  
    SWAP(x[281], x[335]);  SWAP(x[124], x[584]);  SWAP(x[ 84], x[486]);  SWAP(x[153], x[525]);  
    SWAP(x[283], x[479]);  SWAP(x[154], x[587]);  SWAP(x[ 34], x[418]);  SWAP(x[157], x[414]);  
    SWAP(x[129], x[535]);  SWAP(x[158], x[553]);  SWAP(x[246], x[397]);  SWAP(x[161], x[513]);  
    SWAP(x[110], x[574]);  SWAP(x[101], x[501]);  SWAP(x[ 19], x[332]);  SWAP(x[163], x[409]);  
    SWAP(x[ 40], x[463]);  SWAP(x[307], x[601]);  SWAP(x[268], x[546]);  SWAP(x[164], x[389]);  
    SWAP(x[ 71], x[516]);  SWAP(x[ 26], x[393]);  SWAP(x[107], x[373]);  SWAP(x[171], x[470]);  
    SWAP(x[130], x[321]);  SWAP(x[105], x[382]);  SWAP(x[205], x[341]);  SWAP(x[172], x[387]);  
    SWAP(x[ 87], x[548]);  SWAP(x[280], x[537]);  SWAP(x[293], x[368]);  SWAP(x[175], x[504]);  
    SWAP(x[160], x[589]);  SWAP(x[ 17], x[529]);  SWAP(x[ 29], x[549]);  SWAP(x[178], x[522]);  
    SWAP(x[207], x[570]);  SWAP(x[170], x[422]);  SWAP(x[125], x[469]);  SWAP(x[181], x[509]);  
    SWAP(x[294], x[507]);  SWAP(x[ 41], x[408]);  SWAP(x[259], x[322]);  SWAP(x[185], x[461]);  
    SWAP(x[ 92], x[362]);  SWAP(x[303], x[489]);  SWAP(x[  5], x[447]);  SWAP(x[186], x[316]);  
    SWAP(x[276], x[477]);  SWAP(x[292], x[556]);  SWAP(x[ 85], x[329]);  SWAP(x[199], x[576]);  
    SWAP(x[ 63], x[404]);  SWAP(x[269], x[376]);  SWAP(x[237], x[502]);  SWAP(x[202], x[465]);  
    SWAP(x[144], x[438]);  SWAP(x[224], x[319]);  SWAP(x[256], x[315]);  SWAP(x[212], x[566]);  
    SWAP(x[173], x[433]);  SWAP(x[ 10], x[572]);  SWAP(x[188], x[326]);  SWAP(x[213], x[460]);  
    SWAP(x[151], x[359]);  SWAP(x[297], x[412]);  SWAP(x[ 58], x[491]);  SWAP(x[215], x[410]);  
    SWAP(x[245], x[314]);  SWAP(x[  9], x[313]);  SWAP(x[195], x[445]);  SWAP(x[225], x[419]);  
    SWAP(x[310], x[622]);  SWAP(x[134], x[333]);  SWAP(x[159], x[352]);  SWAP(x[226], x[441]);  
    SWAP(x[ 80], x[364]);  SWAP(x[244], x[417]);  SWAP(x[211], x[585]);  SWAP(x[228], x[424]);  
    SWAP(x[174], x[354]);  SWAP(x[ 62], x[344]);  SWAP(x[ 11], x[431]);  SWAP(x[229], x[346]);  
    SWAP(x[115], x[539]);  SWAP(x[166], x[554]);  SWAP(x[117], x[400]);  SWAP(x[230], x[481]);  
    SWAP(x[155], x[464]);  SWAP(x[196], x[453]);  SWAP(x[ 88], x[474]);  SWAP(x[231], x[618]);  
    SWAP(x[208], x[457]);  SWAP(x[257], x[334]);  SWAP(x[ 74], x[392]);  SWAP(x[234], x[578]);  
    SWAP(x[271], x[328]);  SWAP(x[100], x[559]);  SWAP(x[127], x[385]);  SWAP(x[240], x[348]);  
    SWAP(x[ 28], x[520]);  SWAP(x[ 59], x[446]);  SWAP(x[ 54], x[375]);  SWAP(x[241], x[459]);  
    SWAP(x[206], x[349]);  SWAP(x[118], x[467]);  SWAP(x[284], x[365]);  SWAP(x[249], x[396]);  

    SWAP(x[ 38], x[616]);  SWAP(x[251], x[511]);  SWAP(x[194], x[439]);  SWAP(x[250], x[367]);  
    SWAP(x[190], x[355]);  SWAP(x[156], x[320]);  SWAP(x[149], x[590]);  SWAP(x[252], x[363]);  
    SWAP(x[ 32], x[356]);  SWAP(x[ 96], x[600]);  SWAP(x[121], x[571]);  SWAP(x[265], x[420]);  
    SWAP(x[221], x[317]);  SWAP(x[296], x[330]);  SWAP(x[119], x[435]);  SWAP(x[272], x[391]);  
    SWAP(x[179], x[350]);  SWAP(x[278], x[372]);  SWAP(x[106], x[405]);  SWAP(x[274], x[478]);  
    SWAP(x[219], x[621]);  SWAP(x[263], x[462]);  SWAP(x[133], x[494]);  SWAP(x[277], x[614]);  
    SWAP(x[  0], x[610]);  SWAP(x[ 93], x[371]);  SWAP(x[ 51], x[612]);  SWAP(x[285], x[487]);  
    SWAP(x[311], x[542]);  SWAP(x[209], x[563]);  SWAP(x[220], x[593]);  SWAP(x[286], x[515]);  
    SWAP(x[ 78], x[360]);  SWAP(x[168], x[342]);  SWAP(x[ 14], x[512]);  SWAP(x[288], x[485]);  
    SWAP(x[ 18], x[370]);  SWAP(x[112], x[366]);  SWAP(x[216], x[353]);  SWAP(x[289], x[532]);  
    SWAP(x[ 20], x[386]);  SWAP(x[273], x[425]);  SWAP(x[304], x[377]);  SWAP(x[290], x[475]);  
    SWAP(x[ 91], x[533]);  SWAP(x[223], x[573]);  SWAP(x[233], x[406]);  SWAP(x[298], x[484]);  
    SWAP(x[261], x[530]);  SWAP(x[ 97], x[608]);  SWAP(x[243], x[390]);  SWAP(x[305], x[619]);  
    SWAP(x[ 49], x[325]);  SWAP(x[109], x[426]);  SWAP(x[258], x[357]);  SWAP(x[309], x[582]);  
}

/**
 * @brief Permute the one block (624 positions) of pool32mt pool.
 *        The block size must be 624 * sizeof(uint32_t).
 * 
 */
void _pool32mt_block_permute(uint32_t* x, unsigned int nrounds) {

    unsigned int i;

    for (i = 0; i < nrounds; i++) {
        /* Linear */
        // x[  0] += x[585];  x[429] = rol32(x[429] ^ x[  0],   7);  x[156] = rol32(x[156] ^ 179,   7);
        // x[  1] += x[521];  x[365] = rol32(x[365] ^ x[  1],  12);  x[157] = rol32(x[157] ^ 127,  12);
        // x[  2] += x[537];  x[381] = rol32(x[381] ^ x[  2],   8);  x[158] = rol32(x[158] ^ 271,   8);
        // x[  3] += x[595];  x[439] = rol32(x[439] ^ x[  3],  12);  x[159] = rol32(x[159] ^ 277,  12);
        // x[  4] += x[384];  x[228] = rol32(x[228] ^ x[  4],   8);  x[160] = rol32(x[160] ^  43,   8);
        // x[  5] += x[536];  x[380] = rol32(x[380] ^ x[  5],   7);  x[161] = rol32(x[161] ^ 269,   7);
        // x[  6] += x[424];  x[268] = rol32(x[268] ^ x[  6],  16);  x[162] = rol32(x[162] ^ 223,  16);
        // x[  7] += x[561];  x[405] = rol32(x[405] ^ x[  7],   7);  x[163] = rol32(x[163] ^  41,   7);
        // x[  8] += x[474];  x[318] = rol32(x[318] ^ x[  8],   7);  x[164] = rol32(x[164] ^ 227,   7);
        // x[  9] += x[335];  x[179] = rol32(x[179] ^ x[  9],  16);  x[165] = rol32(x[165] ^  37,  16);
        // x[ 10] += x[490];  x[334] = rol32(x[334] ^ x[ 10],   8);  x[166] = rol32(x[166] ^ 101,   8);
        // x[ 11] += x[617];  x[461] = rol32(x[461] ^ x[ 11],  16);  x[167] = rol32(x[167] ^ 151,  16);
        // x[ 12] += x[363];  x[207] = rol32(x[207] ^ x[ 12],  16);  x[168] = rol32(x[168] ^ 281,  16);
        // x[ 13] += x[354];  x[198] = rol32(x[198] ^ x[ 13],   8);  x[169] = rol32(x[169] ^ 251,   8);
        // x[ 14] += x[503];  x[347] = rol32(x[347] ^ x[ 14],  12);  x[170] = rol32(x[170] ^  47,  12);
        // x[ 15] += x[547];  x[391] = rol32(x[391] ^ x[ 15],  12);  x[171] = rol32(x[171] ^ 149,  12);
        // x[ 16] += x[442];  x[286] = rol32(x[286] ^ x[ 16],  12);  x[172] = rol32(x[172] ^ 191,  12);
        // x[ 17] += x[565];  x[409] = rol32(x[409] ^ x[ 17],  16);  x[173] = rol32(x[173] ^  71,  16);
        // x[ 18] += x[505];  x[349] = rol32(x[349] ^ x[ 18],  16);  x[174] = rol32(x[174] ^ 281,  16);
        // x[ 19] += x[460];  x[304] = rol32(x[304] ^ x[ 19],   8);  x[175] = rol32(x[175] ^   3,   8);
        // x[ 20] += x[575];  x[419] = rol32(x[419] ^ x[ 20],  12);  x[176] = rol32(x[176] ^  67,  12);
        // x[ 21] += x[361];  x[205] = rol32(x[205] ^ x[ 21],   8);  x[177] = rol32(x[177] ^  61,   8);
        // x[ 22] += x[405];  x[249] = rol32(x[249] ^ x[ 22],   8);  x[178] = rol32(x[178] ^  29,   8);
        // x[ 23] += x[453];  x[297] = rol32(x[297] ^ x[ 23],   7);  x[179] = rol32(x[179] ^  23,   7);
        // x[ 24] += x[437];  x[281] = rol32(x[281] ^ x[ 24],  12);  x[180] = rol32(x[180] ^ 257,  12);
        // x[ 25] += x[527];  x[371] = rol32(x[371] ^ x[ 25],   7);  x[181] = rol32(x[181] ^ 137,   7);
        // x[ 26] += x[459];  x[303] = rol32(x[303] ^ x[ 26],  12);  x[182] = rol32(x[182] ^ 233,  12);
        // x[ 27] += x[581];  x[425] = rol32(x[425] ^ x[ 27],   8);  x[183] = rol32(x[183] ^ 271,   8);
        // x[ 28] += x[534];  x[378] = rol32(x[378] ^ x[ 28],   7);  x[184] = rol32(x[184] ^ 109,   7);
        // x[ 29] += x[421];  x[265] = rol32(x[265] ^ x[ 29],   8);  x[185] = rol32(x[185] ^  61,   8);
        // x[ 30] += x[544];  x[388] = rol32(x[388] ^ x[ 30],   8);  x[186] = rol32(x[186] ^  43,   8);
        // x[ 31] += x[613];  x[457] = rol32(x[457] ^ x[ 31],  16);  x[187] = rol32(x[187] ^ 107,  16);

        // x[ 32] += x[513];  x[357] = rol32(x[357] ^ x[ 32],  16);  x[188] = rol32(x[188] ^ 173,  16);
        // x[ 33] += x[523];  x[367] = rol32(x[367] ^ x[ 33],  16);  x[189] = rol32(x[189] ^ 107,  16);
        // x[ 34] += x[622];  x[466] = rol32(x[466] ^ x[ 34],   7);  x[190] = rol32(x[190] ^  41,   7);
        // x[ 35] += x[438];  x[282] = rol32(x[282] ^ x[ 35],   8);  x[191] = rol32(x[191] ^ 181,   8);
        // x[ 36] += x[372];  x[216] = rol32(x[216] ^ x[ 36],   7);  x[192] = rol32(x[192] ^ 197,   7);
        // x[ 37] += x[336];  x[180] = rol32(x[180] ^ x[ 37],   8);  x[193] = rol32(x[193] ^   3,   8);
        // x[ 38] += x[356];  x[200] = rol32(x[200] ^ x[ 38],   7);  x[194] = rol32(x[194] ^  97,   7);
        // x[ 39] += x[568];  x[412] = rol32(x[412] ^ x[ 39],  16);  x[195] = rol32(x[195] ^ 263,  16);
        // x[ 40] += x[619];  x[463] = rol32(x[463] ^ x[ 40],  12);  x[196] = rol32(x[196] ^ 103,  12);
        // x[ 41] += x[519];  x[363] = rol32(x[363] ^ x[ 41],  16);  x[197] = rol32(x[197] ^ 173,  16);
        // x[ 42] += x[488];  x[332] = rol32(x[332] ^ x[ 42],   8);  x[198] = rol32(x[198] ^  29,   8);
        // x[ 43] += x[427];  x[271] = rol32(x[271] ^ x[ 43],  12);  x[199] = rol32(x[199] ^ 149,  12);
        // x[ 44] += x[353];  x[197] = rol32(x[197] ^ x[ 44],  16);  x[200] = rol32(x[200] ^ 239,  16);
        // x[ 45] += x[355];  x[199] = rol32(x[199] ^ x[ 45],   7);  x[201] = rol32(x[201] ^ 137,   7);
        // x[ 46] += x[463];  x[307] = rol32(x[307] ^ x[ 46],   8);  x[202] = rol32(x[202] ^ 113,   8);
        // x[ 47] += x[481];  x[325] = rol32(x[325] ^ x[ 47],   8);  x[203] = rol32(x[203] ^ 199,   8);
        // x[ 48] += x[314];  x[158] = rol32(x[158] ^ x[ 48],  12);  x[204] = rol32(x[204] ^ 167,  12);
        // x[ 49] += x[439];  x[283] = rol32(x[283] ^ x[ 49],  12);  x[205] = rol32(x[205] ^ 257,  12);
        // x[ 50] += x[594];  x[438] = rol32(x[438] ^ x[ 50],  12);  x[206] = rol32(x[206] ^ 257,  12);
        // x[ 51] += x[461];  x[305] = rol32(x[305] ^ x[ 51],  12);  x[207] = rol32(x[207] ^ 233,  12);
        // x[ 52] += x[458];  x[302] = rol32(x[302] ^ x[ 52],   7);  x[208] = rol32(x[208] ^ 137,   7);
        // x[ 53] += x[403];  x[247] = rol32(x[247] ^ x[ 53],   7);  x[209] = rol32(x[209] ^  97,   7);
        // x[ 54] += x[434];  x[278] = rol32(x[278] ^ x[ 54],  16);  x[210] = rol32(x[210] ^  53,  16);
        // x[ 55] += x[332];  x[176] = rol32(x[176] ^ x[ 55],   7);  x[211] = rol32(x[211] ^  73,   7);
        // x[ 56] += x[317];  x[161] = rol32(x[161] ^ x[ 56],  16);  x[212] = rol32(x[212] ^  37,  16);
        // x[ 57] += x[587];  x[431] = rol32(x[431] ^ x[ 57],   8);  x[213] = rol32(x[213] ^ 163,   8);
        // x[ 58] += x[514];  x[358] = rol32(x[358] ^ x[ 58],  16);  x[214] = rol32(x[214] ^ 239,  16);
        // x[ 59] += x[313];  x[157] = rol32(x[157] ^ x[ 59],  12);  x[215] = rol32(x[215] ^ 127,  12);
        // x[ 60] += x[601];  x[445] = rol32(x[445] ^ x[ 60],   8);  x[216] = rol32(x[216] ^  13,   8);
        // x[ 61] += x[522];  x[366] = rol32(x[366] ^ x[ 61],  12);  x[217] = rol32(x[217] ^  83,  12);
        // x[ 62] += x[348];  x[192] = rol32(x[192] ^ x[ 62],   8);  x[218] = rol32(x[218] ^  79,   8);
        // x[ 63] += x[465];  x[309] = rol32(x[309] ^ x[ 63],  12);  x[219] = rol32(x[219] ^ 191,  12);

        // x[ 64] += x[419];  x[263] = rol32(x[263] ^ x[ 64],   7);  x[220] = rol32(x[220] ^ 197,   7);
        // x[ 65] += x[339];  x[183] = rol32(x[183] ^ x[ 65],   7);  x[221] = rol32(x[221] ^  23,   7);
        // x[ 66] += x[559];  x[403] = rol32(x[403] ^ x[ 66],   7);  x[222] = rol32(x[222] ^ 109,   7);
        // x[ 67] += x[330];  x[174] = rol32(x[174] ^ x[ 67],   8);  x[223] = rol32(x[223] ^   3,   8);
        // x[ 68] += x[446];  x[290] = rol32(x[290] ^ x[ 68],   7);  x[224] = rol32(x[224] ^ 109,   7);
        // x[ 69] += x[440];  x[284] = rol32(x[284] ^ x[ 69],   8);  x[225] = rol32(x[225] ^  13,   8);
        // x[ 70] += x[571];  x[415] = rol32(x[415] ^ x[ 70],  16);  x[226] = rol32(x[226] ^ 173,  16);
        // x[ 71] += x[343];  x[187] = rol32(x[187] ^ x[ 71],  12);  x[227] = rol32(x[227] ^   5,  12);
        // x[ 72] += x[418];  x[262] = rol32(x[262] ^ x[ 72],  12);  x[228] = rol32(x[228] ^ 277,  12);
        // x[ 73] += x[590];  x[434] = rol32(x[434] ^ x[ 73],   8);  x[229] = rol32(x[229] ^ 251,   8);
        // x[ 74] += x[549];  x[393] = rol32(x[393] ^ x[ 74],   8);  x[230] = rol32(x[230] ^ 271,   8);
        // x[ 75] += x[614];  x[458] = rol32(x[458] ^ x[ 75],   8);  x[231] = rol32(x[231] ^ 139,   8);
        // x[ 76] += x[391];  x[235] = rol32(x[235] ^ x[ 76],  12);  x[232] = rol32(x[232] ^  67,  12);
        // x[ 77] += x[393];  x[237] = rol32(x[237] ^ x[ 77],  12);  x[233] = rol32(x[233] ^ 191,  12);
        // x[ 78] += x[394];  x[238] = rol32(x[238] ^ x[ 78],  12);  x[234] = rol32(x[234] ^ 149,  12);
        // x[ 79] += x[491];  x[335] = rol32(x[335] ^ x[ 79],   8);  x[235] = rol32(x[235] ^ 293,   8);
        // x[ 80] += x[600];  x[444] = rol32(x[444] ^ x[ 80],  12);  x[236] = rol32(x[236] ^ 167,  12);
        // x[ 81] += x[400];  x[244] = rol32(x[244] ^ x[ 81],  16);  x[237] = rol32(x[237] ^  53,  16);
        // x[ 82] += x[364];  x[208] = rol32(x[208] ^ x[ 82],  16);  x[238] = rol32(x[238] ^ 263,  16);
        // x[ 83] += x[318];  x[162] = rol32(x[162] ^ x[ 83],   8);  x[239] = rol32(x[239] ^  13,   8);
        // x[ 84] += x[422];  x[266] = rol32(x[266] ^ x[ 84],   8);  x[240] = rol32(x[240] ^  61,   8);
        // x[ 85] += x[480];  x[324] = rol32(x[324] ^ x[ 85],   7);  x[241] = rol32(x[241] ^  73,   7);
        // x[ 86] += x[497];  x[341] = rol32(x[341] ^ x[ 86],   7);  x[242] = rol32(x[242] ^ 227,   7);
        // x[ 87] += x[546];  x[390] = rol32(x[390] ^ x[ 87],  12);  x[243] = rol32(x[243] ^ 167,  12);
        // x[ 88] += x[608];  x[452] = rol32(x[452] ^ x[ 88],   8);  x[244] = rol32(x[244] ^  29,   8);
        // x[ 89] += x[430];  x[274] = rol32(x[274] ^ x[ 89],   8);  x[245] = rol32(x[245] ^ 101,   8);
        // x[ 90] += x[445];  x[289] = rol32(x[289] ^ x[ 90],  16);  x[246] = rol32(x[246] ^ 281,  16);
        // x[ 91] += x[447];  x[291] = rol32(x[291] ^ x[ 91],   8);  x[247] = rol32(x[247] ^ 163,   8);
        // x[ 92] += x[504];  x[348] = rol32(x[348] ^ x[ 92],   7);  x[248] = rol32(x[248] ^ 157,   7);
        // x[ 93] += x[369];  x[213] = rol32(x[213] ^ x[ 93],  12);  x[249] = rol32(x[249] ^ 307,  12);
        // x[ 94] += x[598];  x[442] = rol32(x[442] ^ x[ 94],   8);  x[250] = rol32(x[250] ^ 271,   8);
        // x[ 95] += x[392];  x[236] = rol32(x[236] ^ x[ 95],  16);  x[251] = rol32(x[251] ^  53,  16);

        // x[ 96] += x[524];  x[368] = rol32(x[368] ^ x[ 96],  12);  x[252] = rol32(x[252] ^ 211,  12);
        // x[ 97] += x[588];  x[432] = rol32(x[432] ^ x[ 97],   7);  x[253] = rol32(x[253] ^ 283,   7);
        // x[ 98] += x[464];  x[308] = rol32(x[308] ^ x[ 98],  16);  x[254] = rol32(x[254] ^ 107,  16);
        // x[ 99] += x[584];  x[428] = rol32(x[428] ^ x[ 99],  12);  x[255] = rol32(x[255] ^  17,  12);
        // x[100] += x[576];  x[420] = rol32(x[420] ^ x[100],  12);  x[256] = rol32(x[256] ^ 211,  12);
        // x[101] += x[579];  x[423] = rol32(x[423] ^ x[101],   8);  x[257] = rol32(x[257] ^ 163,   8);
        // x[102] += x[320];  x[164] = rol32(x[164] ^ x[102],  16);  x[258] = rol32(x[258] ^  89,  16);
        // x[103] += x[358];  x[202] = rol32(x[202] ^ x[103],  16);  x[259] = rol32(x[259] ^   7,  16);
        // x[104] += x[432];  x[276] = rol32(x[276] ^ x[104],   8);  x[260] = rol32(x[260] ^  43,   8);
        // x[105] += x[560];  x[404] = rol32(x[404] ^ x[105],   7);  x[261] = rol32(x[261] ^  59,   7);
        // x[106] += x[542];  x[386] = rol32(x[386] ^ x[106],  16);  x[262] = rol32(x[262] ^ 131,  16);
        // x[107] += x[566];  x[410] = rol32(x[410] ^ x[107],  16);  x[263] = rol32(x[263] ^  19,  16);
        // x[108] += x[620];  x[464] = rol32(x[464] ^ x[108],   7);  x[264] = rol32(x[264] ^ 269,   7);
        // x[109] += x[574];  x[418] = rol32(x[418] ^ x[109],   7);  x[265] = rol32(x[265] ^ 227,   7);
        // x[110] += x[374];  x[218] = rol32(x[218] ^ x[110],   8);  x[266] = rol32(x[266] ^  61,   8);
        // x[111] += x[599];  x[443] = rol32(x[443] ^ x[111],  16);  x[267] = rol32(x[267] ^ 263,  16);
        // x[112] += x[326];  x[170] = rol32(x[170] ^ x[112],  16);  x[268] = rol32(x[268] ^ 281,  16);
        // x[113] += x[457];  x[301] = rol32(x[301] ^ x[113],  12);  x[269] = rol32(x[269] ^ 127,  12);
        // x[114] += x[470];  x[314] = rol32(x[314] ^ x[114],   7);  x[270] = rol32(x[270] ^  23,   7);
        // x[115] += x[483];  x[327] = rol32(x[327] ^ x[115],   7);  x[271] = rol32(x[271] ^  97,   7);
        // x[116] += x[593];  x[437] = rol32(x[437] ^ x[116],   8);  x[272] = rol32(x[272] ^ 163,   8);
        // x[117] += x[368];  x[212] = rol32(x[212] ^ x[117],   7);  x[273] = rol32(x[273] ^ 283,   7);
        // x[118] += x[548];  x[392] = rol32(x[392] ^ x[118],  16);  x[274] = rol32(x[274] ^ 151,  16);
        // x[119] += x[528];  x[372] = rol32(x[372] ^ x[119],   8);  x[275] = rol32(x[275] ^ 293,   8);
        // x[120] += x[494];  x[338] = rol32(x[338] ^ x[120],   8);  x[276] = rol32(x[276] ^ 113,   8);
        // x[121] += x[500];  x[344] = rol32(x[344] ^ x[121],  16);  x[277] = rol32(x[277] ^ 151,  16);
        // x[122] += x[502];  x[346] = rol32(x[346] ^ x[122],  12);  x[278] = rol32(x[278] ^ 211,  12);
        // x[123] += x[478];  x[322] = rol32(x[322] ^ x[123],  12);  x[279] = rol32(x[279] ^ 149,  12);
        // x[124] += x[360];  x[204] = rol32(x[204] ^ x[124],  12);  x[280] = rol32(x[280] ^  47,  12);
        // x[125] += x[412];  x[256] = rol32(x[256] ^ x[125],   7);  x[281] = rol32(x[281] ^ 227,   7);
        // x[126] += x[580];  x[424] = rol32(x[424] ^ x[126],  16);  x[282] = rol32(x[282] ^  19,  16);
        // x[127] += x[589];  x[433] = rol32(x[433] ^ x[127],  12);  x[283] = rol32(x[283] ^   5,  12);

        // x[128] += x[315];  x[159] = rol32(x[159] ^ x[128],   8);  x[284] = rol32(x[284] ^ 181,   8);
        // x[129] += x[473];  x[317] = rol32(x[317] ^ x[129],   8);  x[285] = rol32(x[285] ^ 139,   8);
        // x[130] += x[482];  x[326] = rol32(x[326] ^ x[130],  16);  x[286] = rol32(x[286] ^ 223,  16);
        // x[131] += x[322];  x[166] = rol32(x[166] ^ x[131],   7);  x[287] = rol32(x[287] ^   2,   7);
        // x[132] += x[346];  x[190] = rol32(x[190] ^ x[132],  16);  x[288] = rol32(x[288] ^  53,  16);
        // x[133] += x[431];  x[275] = rol32(x[275] ^ x[133],  16);  x[289] = rol32(x[289] ^ 151,  16);
        // x[134] += x[530];  x[374] = rol32(x[374] ^ x[134],  12);  x[290] = rol32(x[290] ^  83,  12);
        // x[135] += x[444];  x[288] = rol32(x[288] ^ x[135],   7);  x[291] = rol32(x[291] ^  97,   7);
        // x[136] += x[569];  x[413] = rol32(x[413] ^ x[136],   7);  x[292] = rol32(x[292] ^ 241,   7);
        // x[137] += x[551];  x[395] = rol32(x[395] ^ x[137],   8);  x[293] = rol32(x[293] ^ 293,   8);
        // x[138] += x[469];  x[313] = rol32(x[313] ^ x[138],   8);  x[294] = rol32(x[294] ^  79,   8);
        // x[139] += x[475];  x[319] = rol32(x[319] ^ x[139],  12);  x[295] = rol32(x[295] ^   5,  12);
        // x[140] += x[558];  x[402] = rol32(x[402] ^ x[140],  16);  x[296] = rol32(x[296] ^ 239,  16);
        // x[141] += x[487];  x[331] = rol32(x[331] ^ x[141],  12);  x[297] = rol32(x[297] ^  17,  12);
        // x[142] += x[507];  x[351] = rol32(x[351] ^ x[142],   7);  x[298] = rol32(x[298] ^ 241,   7);
        // x[143] += x[556];  x[400] = rol32(x[400] ^ x[143],   7);  x[299] = rol32(x[299] ^ 137,   7);
        // x[144] += x[370];  x[214] = rol32(x[214] ^ x[144],  12);  x[300] = rol32(x[300] ^ 233,  12);
        // x[145] += x[382];  x[226] = rol32(x[226] ^ x[145],  16);  x[301] = rol32(x[301] ^  53,  16);
        // x[146] += x[553];  x[397] = rol32(x[397] ^ x[146],   8);  x[302] = rol32(x[302] ^ 181,   8);
        // x[147] += x[455];  x[299] = rol32(x[299] ^ x[147],  16);  x[303] = rol32(x[303] ^ 239,  16);
        // x[148] += x[510];  x[354] = rol32(x[354] ^ x[148],  12);  x[304] = rol32(x[304] ^   5,  12);
        // x[149] += x[420];  x[264] = rol32(x[264] ^ x[149],  16);  x[305] = rol32(x[305] ^  89,  16);
        // x[150] += x[515];  x[359] = rol32(x[359] ^ x[150],  12);  x[306] = rol32(x[306] ^  67,  12);
        // x[151] += x[331];  x[175] = rol32(x[175] ^ x[151],   7);  x[307] = rol32(x[307] ^  59,   7);
        // x[152] += x[390];  x[234] = rol32(x[234] ^ x[152],  12);  x[308] = rol32(x[308] ^ 103,  12);
        // x[153] += x[508];  x[352] = rol32(x[352] ^ x[153],   8);  x[309] = rol32(x[309] ^  43,   8);
        // x[154] += x[603];  x[447] = rol32(x[447] ^ x[154],  12);  x[310] = rol32(x[310] ^ 127,  12);
        // x[155] += x[518];  x[362] = rol32(x[362] ^ x[155],  12);  x[311] = rol32(x[311] ^ 307,  12);
        // x[156] += x[597];  x[441] = rol32(x[441] ^ x[156],   8);  x[312] = rol32(x[312] ^ 229,   8);
        // x[157] += x[509];  x[353] = rol32(x[353] ^ x[157],  12);  x[313] = rol32(x[313] ^   5,  12);
        // x[158] += x[406];  x[250] = rol32(x[250] ^ x[158],  16);  x[314] = rol32(x[314] ^   7,  16);
        // x[159] += x[367];  x[211] = rol32(x[211] ^ x[159],  16);  x[315] = rol32(x[315] ^ 311,  16);

        // x[160] += x[501];  x[345] = rol32(x[345] ^ x[160],   8);  x[316] = rol32(x[316] ^ 293,   8);
        // x[161] += x[609];  x[453] = rol32(x[453] ^ x[161],   7);  x[317] = rol32(x[317] ^ 197,   7);
        // x[162] += x[550];  x[394] = rol32(x[394] ^ x[162],  16);  x[318] = rol32(x[318] ^ 131,  16);
        // x[163] += x[373];  x[217] = rol32(x[217] ^ x[163],   7);  x[319] = rol32(x[319] ^ 137,   7);
        // x[164] += x[562];  x[406] = rol32(x[406] ^ x[164],  16);  x[320] = rol32(x[320] ^  19,  16);
        // x[165] += x[366];  x[210] = rol32(x[210] ^ x[165],  12);  x[321] = rol32(x[321] ^  17,  12);
        // x[166] += x[398];  x[242] = rol32(x[242] ^ x[166],  16);  x[322] = rol32(x[322] ^  71,  16);
        // x[167] += x[583];  x[427] = rol32(x[427] ^ x[167],  12);  x[323] = rol32(x[323] ^ 211,  12);
        // x[168] += x[413];  x[257] = rol32(x[257] ^ x[168],  16);  x[324] = rol32(x[324] ^ 223,  16);
        // x[169] += x[399];  x[243] = rol32(x[243] ^ x[169],   7);  x[325] = rol32(x[325] ^  23,   7);
        // x[170] += x[425];  x[269] = rol32(x[269] ^ x[170],   7);  x[326] = rol32(x[326] ^ 179,   7);
        // x[171] += x[484];  x[328] = rol32(x[328] ^ x[171],  12);  x[327] = rol32(x[327] ^ 103,  12);
        // x[172] += x[448];  x[292] = rol32(x[292] ^ x[172],  12);  x[328] = rol32(x[328] ^  17,  12);
        // x[173] += x[578];  x[422] = rol32(x[422] ^ x[173],  12);  x[329] = rol32(x[329] ^  31,  12);
        // x[174] += x[324];  x[168] = rol32(x[168] ^ x[174],  16);  x[330] = rol32(x[330] ^   7,  16);
        // x[175] += x[529];  x[373] = rol32(x[373] ^ x[175],   7);  x[331] = rol32(x[331] ^  11,   7);
        // x[176] += x[570];  x[414] = rol32(x[414] ^ x[176],  12);  x[332] = rol32(x[332] ^  67,  12);
        // x[177] += x[586];  x[430] = rol32(x[430] ^ x[177],  16);  x[333] = rol32(x[333] ^ 193,  16);
        // x[178] += x[451];  x[295] = rol32(x[295] ^ x[178],   7);  x[334] = rol32(x[334] ^  97,   7);
        // x[179] += x[533];  x[377] = rol32(x[377] ^ x[179],  12);  x[335] = rol32(x[335] ^ 307,  12);
        // x[180] += x[359];  x[203] = rol32(x[203] ^ x[180],   7);  x[336] = rol32(x[336] ^  41,   7);
        // x[181] += x[466];  x[310] = rol32(x[310] ^ x[181],   8);  x[337] = rol32(x[337] ^ 229,   8);
        // x[182] += x[344];  x[188] = rol32(x[188] ^ x[182],  12);  x[338] = rol32(x[338] ^  31,  12);
        // x[183] += x[573];  x[417] = rol32(x[417] ^ x[183],   7);  x[339] = rol32(x[339] ^   2,   7);
        // x[184] += x[563];  x[407] = rol32(x[407] ^ x[184],   7);  x[340] = rol32(x[340] ^ 241,   7);
        // x[185] += x[449];  x[293] = rol32(x[293] ^ x[185],   7);  x[341] = rol32(x[341] ^   2,   7);
        // x[186] += x[340];  x[184] = rol32(x[184] ^ x[186],   7);  x[342] = rol32(x[342] ^  59,   7);
        // x[187] += x[441];  x[285] = rol32(x[285] ^ x[187],  12);  x[343] = rol32(x[343] ^ 103,  12);
        // x[188] += x[328];  x[172] = rol32(x[172] ^ x[188],   7);  x[344] = rol32(x[344] ^  59,   7);
        // x[189] += x[411];  x[255] = rol32(x[255] ^ x[189],   7);  x[345] = rol32(x[345] ^ 179,   7);
        // x[190] += x[456];  x[300] = rol32(x[300] ^ x[190],   8);  x[346] = rol32(x[346] ^ 181,   8);
        // x[191] += x[545];  x[389] = rol32(x[389] ^ x[191],   7);  x[347] = rol32(x[347] ^  73,   7);

        // x[192] += x[592];  x[436] = rol32(x[436] ^ x[192],   7);  x[348] = rol32(x[348] ^  73,   7);
        // x[193] += x[423];  x[267] = rol32(x[267] ^ x[193],   7);  x[349] = rol32(x[349] ^  73,   7);
        // x[194] += x[352];  x[196] = rol32(x[196] ^ x[194],   8);  x[350] = rol32(x[350] ^ 199,   8);
        // x[195] += x[477];  x[321] = rol32(x[321] ^ x[195],  12);  x[351] = rol32(x[351] ^  83,  12);
        // x[196] += x[591];  x[435] = rol32(x[435] ^ x[196],   8);  x[352] = rol32(x[352] ^ 113,   8);
        // x[197] += x[596];  x[440] = rol32(x[440] ^ x[197],  16);  x[353] = rol32(x[353] ^ 223,  16);
        // x[198] += x[426];  x[270] = rol32(x[270] ^ x[198],   7);  x[354] = rol32(x[354] ^ 157,   7);
        // x[199] += x[342];  x[186] = rol32(x[186] ^ x[199],  12);  x[355] = rol32(x[355] ^  47,  12);
        // x[200] += x[540];  x[384] = rol32(x[384] ^ x[200],   7);  x[356] = rol32(x[356] ^ 197,   7);
        // x[201] += x[365];  x[209] = rol32(x[209] ^ x[201],  16);  x[357] = rol32(x[357] ^  37,  16);
        // x[202] += x[525];  x[369] = rol32(x[369] ^ x[202],   8);  x[358] = rol32(x[358] ^ 181,   8);
        // x[203] += x[485];  x[329] = rol32(x[329] ^ x[203],  12);  x[359] = rol32(x[359] ^ 307,  12);
        // x[204] += x[610];  x[454] = rol32(x[454] ^ x[204],   8);  x[360] = rol32(x[360] ^  79,   8);
        // x[205] += x[312];  x[156] = rol32(x[156] ^ x[205],   7);  x[361] = rol32(x[361] ^ 109,   7);
        // x[206] += x[471];  x[315] = rol32(x[315] ^ x[206],   8);  x[362] = rol32(x[362] ^ 199,   8);
        // x[207] += x[379];  x[223] = rol32(x[223] ^ x[207],   8);  x[363] = rol32(x[363] ^   3,   8);
        // x[208] += x[616];  x[460] = rol32(x[460] ^ x[208],   7);  x[364] = rol32(x[364] ^ 157,   7);
        // x[209] += x[462];  x[306] = rol32(x[306] ^ x[209],   8);  x[365] = rol32(x[365] ^ 113,   8);
        // x[210] += x[489];  x[333] = rol32(x[333] ^ x[210],   8);  x[366] = rol32(x[366] ^  13,   8);
        // x[211] += x[618];  x[462] = rol32(x[462] ^ x[211],  16);  x[367] = rol32(x[367] ^  71,  16);
        // x[212] += x[377];  x[221] = rol32(x[221] ^ x[212],  16);  x[368] = rol32(x[368] ^ 107,  16);
        // x[213] += x[493];  x[337] = rol32(x[337] ^ x[213],   7);  x[369] = rol32(x[369] ^  11,   7);
        // x[214] += x[316];  x[160] = rol32(x[160] ^ x[214],  16);  x[370] = rol32(x[370] ^ 151,  16);
        // x[215] += x[582];  x[426] = rol32(x[426] ^ x[215],   8);  x[371] = rol32(x[371] ^ 101,   8);
        // x[216] += x[512];  x[356] = rol32(x[356] ^ x[216],  16);  x[372] = rol32(x[372] ^  19,  16);
        // x[217] += x[557];  x[401] = rol32(x[401] ^ x[217],   7);  x[373] = rol32(x[373] ^ 157,   7);
        // x[218] += x[454];  x[298] = rol32(x[298] ^ x[218],  12);  x[374] = rol32(x[374] ^ 103,  12);
        // x[219] += x[383];  x[227] = rol32(x[227] ^ x[219],   7);  x[375] = rol32(x[375] ^  59,   7);
        // x[220] += x[416];  x[260] = rol32(x[260] ^ x[220],  16);  x[376] = rol32(x[376] ^  37,  16);
        // x[221] += x[564];  x[408] = rol32(x[408] ^ x[221],  16);  x[377] = rol32(x[377] ^ 131,  16);
        // x[222] += x[472];  x[316] = rol32(x[316] ^ x[222],   7);  x[378] = rol32(x[378] ^ 283,   7);
        // x[223] += x[506];  x[350] = rol32(x[350] ^ x[223],  16);  x[379] = rol32(x[379] ^ 107,  16);

        // x[224] += x[414];  x[258] = rol32(x[258] ^ x[224],   8);  x[380] = rol32(x[380] ^ 199,   8);
        // x[225] += x[408];  x[252] = rol32(x[252] ^ x[225],  12);  x[381] = rol32(x[381] ^  47,  12);
        // x[226] += x[396];  x[240] = rol32(x[240] ^ x[226],  12);  x[382] = rol32(x[382] ^  31,  12);
        // x[227] += x[532];  x[376] = rol32(x[376] ^ x[227],  16);  x[383] = rol32(x[383] ^  71,  16);
        // x[228] += x[410];  x[254] = rol32(x[254] ^ x[228],   7);  x[384] = rol32(x[384] ^ 197,   7);
        // x[229] += x[350];  x[194] = rol32(x[194] ^ x[229],   8);  x[385] = rol32(x[385] ^ 139,   8);
        // x[230] += x[397];  x[241] = rol32(x[241] ^ x[230],   7);  x[386] = rol32(x[386] ^   2,   7);
        // x[231] += x[333];  x[177] = rol32(x[177] ^ x[231],  12);  x[387] = rol32(x[387] ^ 211,  12);
        // x[232] += x[520];  x[364] = rol32(x[364] ^ x[232],  12);  x[388] = rol32(x[388] ^ 257,  12);
        // x[233] += x[387];  x[231] = rol32(x[231] ^ x[233],   8);  x[389] = rol32(x[389] ^ 113,   8);
        // x[234] += x[395];  x[239] = rol32(x[239] ^ x[234],  12);  x[390] = rol32(x[390] ^ 257,  12);
        // x[235] += x[605];  x[449] = rol32(x[449] ^ x[235],   8);  x[391] = rol32(x[391] ^ 229,   8);
        // x[236] += x[381];  x[225] = rol32(x[225] ^ x[236],   7);  x[392] = rol32(x[392] ^  11,   7);
        // x[237] += x[486];  x[330] = rol32(x[330] ^ x[237],  16);  x[393] = rol32(x[393] ^ 173,  16);
        // x[238] += x[623];  x[467] = rol32(x[467] ^ x[238],  16);  x[394] = rol32(x[394] ^  89,  16);
        // x[239] += x[321];  x[165] = rol32(x[165] ^ x[239],  12);  x[395] = rol32(x[395] ^ 167,  12);
        // x[240] += x[388];  x[232] = rol32(x[232] ^ x[240],  12);  x[396] = rol32(x[396] ^ 167,  12);
        // x[241] += x[531];  x[375] = rol32(x[375] ^ x[241],  16);  x[397] = rol32(x[397] ^ 193,  16);
        // x[242] += x[577];  x[421] = rol32(x[421] ^ x[242],   7);  x[398] = rol32(x[398] ^  41,   7);
        // x[243] += x[435];  x[279] = rol32(x[279] ^ x[243],  16);  x[399] = rol32(x[399] ^  71,  16);
        // x[244] += x[452];  x[296] = rol32(x[296] ^ x[244],   8);  x[400] = rol32(x[400] ^  79,   8);
        // x[245] += x[516];  x[360] = rol32(x[360] ^ x[245],   8);  x[401] = rol32(x[401] ^  61,   8);
        // x[246] += x[476];  x[320] = rol32(x[320] ^ x[246],  16);  x[402] = rol32(x[402] ^ 173,  16);
        // x[247] += x[450];  x[294] = rol32(x[294] ^ x[247],   8);  x[403] = rol32(x[403] ^ 199,   8);
        // x[248] += x[492];  x[336] = rol32(x[336] ^ x[248],   7);  x[404] = rol32(x[404] ^ 283,   7);
        // x[249] += x[362];  x[206] = rol32(x[206] ^ x[249],   8);  x[405] = rol32(x[405] ^   3,   8);
        // x[250] += x[543];  x[387] = rol32(x[387] ^ x[250],  16);  x[406] = rol32(x[406] ^ 263,  16);
        // x[251] += x[327];  x[171] = rol32(x[171] ^ x[251],  16);  x[407] = rol32(x[407] ^  37,  16);
        // x[252] += x[325];  x[169] = rol32(x[169] ^ x[252],   7);  x[408] = rol32(x[408] ^ 241,   7);
        // x[253] += x[541];  x[385] = rol32(x[385] ^ x[253],   7);  x[409] = rol32(x[409] ^ 241,   7);
        // x[254] += x[347];  x[191] = rol32(x[191] ^ x[254],  16);  x[410] = rol32(x[410] ^   7,  16);
        // x[255] += x[517];  x[361] = rol32(x[361] ^ x[255],   7);  x[411] = rol32(x[411] ^  11,   7);

        // x[256] += x[389];  x[233] = rol32(x[233] ^ x[256],   8);  x[412] = rol32(x[412] ^ 229,   8);
        // x[257] += x[499];  x[343] = rol32(x[343] ^ x[257],   7);  x[413] = rol32(x[413] ^ 179,   7);
        // x[258] += x[371];  x[215] = rol32(x[215] ^ x[258],  16);  x[414] = rol32(x[414] ^ 131,  16);
        // x[259] += x[357];  x[201] = rol32(x[201] ^ x[259],  16);  x[415] = rol32(x[415] ^ 193,  16);
        // x[260] += x[621];  x[465] = rol32(x[465] ^ x[260],   7);  x[416] = rol32(x[416] ^ 269,   7);
        // x[261] += x[428];  x[272] = rol32(x[272] ^ x[261],   8);  x[417] = rol32(x[417] ^ 229,   8);
        // x[262] += x[380];  x[224] = rol32(x[224] ^ x[262],  12);  x[418] = rol32(x[418] ^  47,  12);
        // x[263] += x[402];  x[246] = rol32(x[246] ^ x[263],   8);  x[419] = rol32(x[419] ^ 251,   8);
        // x[264] += x[479];  x[323] = rol32(x[323] ^ x[264],   7);  x[420] = rol32(x[420] ^ 157,   7);
        // x[265] += x[468];  x[312] = rol32(x[312] ^ x[265],  16);  x[421] = rol32(x[421] ^   7,  16);
        // x[266] += x[329];  x[173] = rol32(x[173] ^ x[266],   8);  x[422] = rol32(x[422] ^ 101,   8);
        // x[267] += x[337];  x[181] = rol32(x[181] ^ x[267],  12);  x[423] = rol32(x[423] ^  31,  12);
        // x[268] += x[319];  x[163] = rol32(x[163] ^ x[268],  16);  x[424] = rol32(x[424] ^ 311,  16);
        // x[269] += x[378];  x[222] = rol32(x[222] ^ x[269],  16);  x[425] = rol32(x[425] ^ 239,  16);
        // x[270] += x[404];  x[248] = rol32(x[248] ^ x[270],  16);  x[426] = rol32(x[426] ^ 311,  16);
        // x[271] += x[385];  x[229] = rol32(x[229] ^ x[271],  12);  x[427] = rol32(x[427] ^ 191,  12);
        // x[272] += x[467];  x[311] = rol32(x[311] ^ x[272],  16);  x[428] = rol32(x[428] ^ 263,  16);
        // x[273] += x[407];  x[251] = rol32(x[251] ^ x[273],   8);  x[429] = rol32(x[429] ^ 101,   8);
        // x[274] += x[495];  x[339] = rol32(x[339] ^ x[274],  12);  x[430] = rol32(x[430] ^  31,  12);
        // x[275] += x[615];  x[459] = rol32(x[459] ^ x[275],  12);  x[431] = rol32(x[431] ^ 191,  12);
        // x[276] += x[602];  x[446] = rol32(x[446] ^ x[276],   8);  x[432] = rol32(x[432] ^  43,   8);
        // x[277] += x[572];  x[416] = rol32(x[416] ^ x[277],   7);  x[433] = rol32(x[433] ^  11,   7);
        // x[278] += x[612];  x[456] = rol32(x[456] ^ x[278],   8);  x[434] = rol32(x[434] ^  29,   8);
        // x[279] += x[554];  x[398] = rol32(x[398] ^ x[279],   8);  x[435] = rol32(x[435] ^  13,   8);
        // x[280] += x[443];  x[287] = rol32(x[287] ^ x[280],  12);  x[436] = rol32(x[436] ^  83,  12);
        // x[281] += x[526];  x[370] = rol32(x[370] ^ x[281],  16);  x[437] = rol32(x[437] ^  89,  16);
        // x[282] += x[375];  x[219] = rol32(x[219] ^ x[282],  12);  x[438] = rol32(x[438] ^ 127,  12);
        // x[283] += x[567];  x[411] = rol32(x[411] ^ x[283],   8);  x[439] = rol32(x[439] ^ 139,   8);
        // x[284] += x[376];  x[220] = rol32(x[220] ^ x[284],   7);  x[440] = rol32(x[440] ^  23,   7);
        // x[285] += x[401];  x[245] = rol32(x[245] ^ x[285],  12);  x[441] = rol32(x[441] ^ 149,  12);
        // x[286] += x[386];  x[230] = rol32(x[230] ^ x[286],  12);  x[442] = rol32(x[442] ^  67,  12);
        // x[287] += x[436];  x[280] = rol32(x[280] ^ x[287],   7);  x[443] = rol32(x[443] ^ 227,   7);

        // x[288] += x[606];  x[450] = rol32(x[450] ^ x[288],  16);  x[444] = rol32(x[444] ^ 223,  16);
        // x[289] += x[415];  x[259] = rol32(x[259] ^ x[289],  12);  x[445] = rol32(x[445] ^ 277,  12);
        // x[290] += x[607];  x[451] = rol32(x[451] ^ x[290],   7);  x[446] = rol32(x[446] ^   2,   7);
        // x[291] += x[345];  x[189] = rol32(x[189] ^ x[291],   8);  x[447] = rol32(x[447] ^  79,   8);
        // x[292] += x[429];  x[273] = rol32(x[273] ^ x[292],  12);  x[448] = rol32(x[448] ^ 233,  12);
        // x[293] += x[334];  x[178] = rol32(x[178] ^ x[293],   8);  x[449] = rol32(x[449] ^  29,   8);
        // x[294] += x[409];  x[253] = rol32(x[253] ^ x[294],   7);  x[450] = rol32(x[450] ^ 109,   7);
        // x[295] += x[511];  x[355] = rol32(x[355] ^ x[295],  16);  x[451] = rol32(x[451] ^  19,  16);
        // x[296] += x[349];  x[193] = rol32(x[193] ^ x[296],  16);  x[452] = rol32(x[452] ^ 131,  16);
        // x[297] += x[539];  x[383] = rol32(x[383] ^ x[297],  12);  x[453] = rol32(x[453] ^ 277,  12);
        // x[298] += x[351];  x[195] = rol32(x[195] ^ x[298],   8);  x[454] = rol32(x[454] ^ 139,   8);
        // x[299] += x[323];  x[167] = rol32(x[167] ^ x[299],   8);  x[455] = rol32(x[455] ^ 251,   8);
        // x[300] += x[338];  x[182] = rol32(x[182] ^ x[300],   8);  x[456] = rol32(x[456] ^ 163,   8);
        // x[301] += x[498];  x[342] = rol32(x[342] ^ x[301],   7);  x[457] = rol32(x[457] ^  41,   7);
        // x[302] += x[555];  x[399] = rol32(x[399] ^ x[302],  12);  x[458] = rol32(x[458] ^ 233,  12);
        // x[303] += x[496];  x[340] = rol32(x[340] ^ x[303],   7);  x[459] = rol32(x[459] ^ 179,   7);
        // x[304] += x[535];  x[379] = rol32(x[379] ^ x[304],  12);  x[460] = rol32(x[460] ^  83,  12);
        // x[305] += x[604];  x[448] = rol32(x[448] ^ x[305],  16);  x[461] = rol32(x[461] ^ 311,  16);
        // x[306] += x[341];  x[185] = rol32(x[185] ^ x[306],  16);  x[462] = rol32(x[462] ^  89,  16);
        // x[307] += x[538];  x[382] = rol32(x[382] ^ x[307],  12);  x[463] = rol32(x[463] ^  17,  12);
        // x[308] += x[611];  x[455] = rol32(x[455] ^ x[308],   8);  x[464] = rol32(x[464] ^ 251,   8);
        // x[309] += x[552];  x[396] = rol32(x[396] ^ x[309],  16);  x[465] = rol32(x[465] ^ 193,  16);
        // x[310] += x[417];  x[261] = rol32(x[261] ^ x[310],  16);  x[466] = rol32(x[466] ^ 193,  16);
        // x[311] += x[433];  x[277] = rol32(x[277] ^ x[311],   7);  x[467] = rol32(x[467] ^ 269,   7);

        /* Non-linar */
        x[ 71] += x[333];  x[177] = rol32(x[177] ^ x[ 71],   7);  x[227] = rol32(x[227] ^   2,   7);
        x[115] += x[560];  x[404] = rol32(x[404] ^ x[115],   8);  x[271] = rol32(x[271] ^   3,   8);
        x[302] += x[424];  x[268] = rol32(x[268] ^ x[302],  12);  x[458] = rol32(x[458] ^   5,  12);
        x[ 80] += x[527];  x[371] = rol32(x[371] ^ x[ 80],  16);  x[236] = rol32(x[236] ^   7,  16);
        x[254] += x[580];  x[424] = rol32(x[424] ^ x[254],   7);  x[410] = rol32(x[410] ^  11,   7);
        x[214] += x[318];  x[162] = rol32(x[162] ^ x[214],   8);  x[370] = rol32(x[370] ^  13,   8);
        x[260] += x[591];  x[435] = rol32(x[435] ^ x[260],  12);  x[416] = rol32(x[416] ^  17,  12);
        x[194] += x[583];  x[427] = rol32(x[427] ^ x[194],  16);  x[350] = rol32(x[350] ^  19,  16);
        x[204] += x[518];  x[362] = rol32(x[362] ^ x[204],   7);  x[360] = rol32(x[360] ^  23,   7);
        x[139] += x[480];  x[324] = rol32(x[324] ^ x[139],   8);  x[295] = rol32(x[295] ^  29,   8);
        x[158] += x[581];  x[425] = rol32(x[425] ^ x[158],  12);  x[314] = rol32(x[314] ^  31,  12);
        x[ 83] += x[525];  x[369] = rol32(x[369] ^ x[ 83],  16);  x[239] = rol32(x[239] ^  37,  16);
        x[307] += x[451];  x[295] = rol32(x[295] ^ x[307],   7);  x[463] = rol32(x[463] ^  41,   7);
        x[ 35] += x[417];  x[261] = rol32(x[261] ^ x[ 35],   8);  x[191] = rol32(x[191] ^  43,   8);
        x[174] += x[508];  x[352] = rol32(x[352] ^ x[174],  12);  x[330] = rol32(x[330] ^  47,  12);
        x[ 65] += x[443];  x[287] = rol32(x[287] ^ x[ 65],  16);  x[221] = rol32(x[221] ^  53,  16);
        x[299] += x[521];  x[365] = rol32(x[365] ^ x[299],   7);  x[455] = rol32(x[455] ^  59,   7);
        x[ 11] += x[519];  x[363] = rol32(x[363] ^ x[ 11],   8);  x[167] = rol32(x[167] ^  61,   8);
        x[269] += x[320];  x[164] = rol32(x[164] ^ x[269],  12);  x[425] = rol32(x[425] ^  67,  12);
        x[ 44] += x[359];  x[203] = rol32(x[203] ^ x[ 44],  16);  x[200] = rol32(x[200] ^  71,  16);
        x[ 81] += x[554];  x[398] = rol32(x[398] ^ x[ 81],   7);  x[237] = rol32(x[237] ^  73,   7);
        x[276] += x[408];  x[252] = rol32(x[252] ^ x[276],   8);  x[432] = rol32(x[432] ^  79,   8);
        x[266] += x[421];  x[265] = rol32(x[265] ^ x[266],  12);  x[422] = rol32(x[422] ^  83,  12);
        x[176] += x[444];  x[288] = rol32(x[288] ^ x[176],  16);  x[332] = rol32(x[332] ^  89,  16);
        x[118] += x[415];  x[259] = rol32(x[259] ^ x[118],   7);  x[274] = rol32(x[274] ^  97,   7);
        x[234] += x[471];  x[315] = rol32(x[315] ^ x[234],   8);  x[390] = rol32(x[390] ^ 101,   8);
        x[ 68] += x[577];  x[421] = rol32(x[421] ^ x[ 68],  12);  x[224] = rol32(x[224] ^ 103,  12);
        x[116] += x[376];  x[220] = rol32(x[220] ^ x[116],  16);  x[272] = rol32(x[272] ^ 107,  16);
        x[165] += x[392];  x[236] = rol32(x[236] ^ x[165],   7);  x[321] = rol32(x[321] ^ 109,   7);
        x[145] += x[336];  x[180] = rol32(x[180] ^ x[145],   8);  x[301] = rol32(x[301] ^ 113,   8);
        x[136] += x[329];  x[173] = rol32(x[173] ^ x[136],  12);  x[292] = rol32(x[292] ^ 127,  12);
        x[ 93] += x[593];  x[437] = rol32(x[437] ^ x[ 93],  16);  x[249] = rol32(x[249] ^ 131,  16);
        x[263] += x[610];  x[454] = rol32(x[454] ^ x[263],   7);  x[419] = rol32(x[419] ^ 137,   7);
        x[ 64] += x[404];  x[248] = rol32(x[248] ^ x[ 64],   8);  x[220] = rol32(x[220] ^ 139,   8);
        x[ 90] += x[439];  x[283] = rol32(x[283] ^ x[ 90],  12);  x[246] = rol32(x[246] ^ 149,  12);
        x[270] += x[584];  x[428] = rol32(x[428] ^ x[270],  16);  x[426] = rol32(x[426] ^ 151,  16);
        x[ 78] += x[427];  x[271] = rol32(x[271] ^ x[ 78],   7);  x[234] = rol32(x[234] ^ 157,   7);
        x[ 77] += x[319];  x[163] = rol32(x[163] ^ x[ 77],   8);  x[233] = rol32(x[233] ^ 163,   8);
        x[230] += x[611];  x[455] = rol32(x[455] ^ x[230],  12);  x[386] = rol32(x[386] ^ 167,  12);
        x[ 53] += x[588];  x[432] = rol32(x[432] ^ x[ 53],  16);  x[209] = rol32(x[209] ^ 173,  16);
        x[265] += x[334];  x[178] = rol32(x[178] ^ x[265],   7);  x[421] = rol32(x[421] ^ 179,   7);
        x[  3] += x[354];  x[198] = rol32(x[198] ^ x[  3],   8);  x[159] = rol32(x[159] ^ 181,   8);
        x[  8] += x[589];  x[433] = rol32(x[433] ^ x[  8],  12);  x[164] = rol32(x[164] ^ 191,  12);
        x[175] += x[489];  x[333] = rol32(x[333] ^ x[175],  16);  x[331] = rol32(x[331] ^ 193,  16);
        x[ 19] += x[537];  x[381] = rol32(x[381] ^ x[ 19],   7);  x[175] = rol32(x[175] ^ 197,   7);
        x[290] += x[472];  x[316] = rol32(x[316] ^ x[290],   8);  x[446] = rol32(x[446] ^ 199,   8);
        x[253] += x[526];  x[370] = rol32(x[370] ^ x[253],  12);  x[409] = rol32(x[409] ^ 211,  12);
        x[205] += x[555];  x[399] = rol32(x[399] ^ x[205],  16);  x[361] = rol32(x[361] ^ 223,  16);
        x[130] += x[463];  x[307] = rol32(x[307] ^ x[130],   7);  x[286] = rol32(x[286] ^ 227,   7);
        x[275] += x[497];  x[341] = rol32(x[341] ^ x[275],   8);  x[431] = rol32(x[431] ^ 229,   8);
        x[281] += x[553];  x[397] = rol32(x[397] ^ x[281],  12);  x[437] = rol32(x[437] ^ 233,  12);
        x[159] += x[395];  x[239] = rol32(x[239] ^ x[159],  16);  x[315] = rol32(x[315] ^ 239,  16);
        x[149] += x[438];  x[282] = rol32(x[282] ^ x[149],   7);  x[305] = rol32(x[305] ^ 241,   7);
        x[140] += x[426];  x[270] = rol32(x[270] ^ x[140],   8);  x[296] = rol32(x[296] ^ 251,   8);
        x[ 70] += x[416];  x[260] = rol32(x[260] ^ x[ 70],  12);  x[226] = rol32(x[226] ^ 257,  12);
        x[188] += x[445];  x[289] = rol32(x[289] ^ x[188],  16);  x[344] = rol32(x[344] ^ 263,  16);
        x[129] += x[513];  x[357] = rol32(x[357] ^ x[129],   7);  x[285] = rol32(x[285] ^ 269,   7);
        x[ 31] += x[350];  x[194] = rol32(x[194] ^ x[ 31],   8);  x[187] = rol32(x[187] ^ 271,   8);
        x[ 66] += x[403];  x[247] = rol32(x[247] ^ x[ 66],  12);  x[222] = rol32(x[222] ^ 277,  12);
        x[170] += x[510];  x[354] = rol32(x[354] ^ x[170],  16);  x[326] = rol32(x[326] ^ 281,  16);
        x[310] += x[401];  x[245] = rol32(x[245] ^ x[310],   7);  x[466] = rol32(x[466] ^ 283,   7);
        x[134] += x[338];  x[182] = rol32(x[182] ^ x[134],   8);  x[290] = rol32(x[290] ^ 293,   8);
        x[ 56] += x[325];  x[169] = rol32(x[169] ^ x[ 56],  12);  x[212] = rol32(x[212] ^ 307,  12);
        x[ 41] += x[371];  x[215] = rol32(x[215] ^ x[ 41],  16);  x[197] = rol32(x[197] ^ 311,  16);
        x[107] += x[348];  x[192] = rol32(x[192] ^ x[107],   7);  x[263] = rol32(x[263] ^   2,   7);
        x[143] += x[388];  x[232] = rol32(x[232] ^ x[143],   8);  x[299] = rol32(x[299] ^   3,   8);
        x[ 14] += x[317];  x[161] = rol32(x[161] ^ x[ 14],  12);  x[170] = rol32(x[170] ^   5,  12);
        x[109] += x[618];  x[462] = rol32(x[462] ^ x[109],  16);  x[265] = rol32(x[265] ^   7,  16);
        x[152] += x[432];  x[276] = rol32(x[276] ^ x[152],   7);  x[308] = rol32(x[308] ^  11,   7);
        x[  5] += x[477];  x[321] = rol32(x[321] ^ x[  5],   8);  x[161] = rol32(x[161] ^  13,   8);
        x[ 37] += x[552];  x[396] = rol32(x[396] ^ x[ 37],  12);  x[193] = rol32(x[193] ^  17,  12);
        x[213] += x[490];  x[334] = rol32(x[334] ^ x[213],  16);  x[369] = rol32(x[369] ^  19,  16);
        x[135] += x[473];  x[317] = rol32(x[317] ^ x[135],   7);  x[291] = rol32(x[291] ^  23,   7);
        x[229] += x[543];  x[387] = rol32(x[387] ^ x[229],   8);  x[385] = rol32(x[385] ^  29,   8);
        x[242] += x[578];  x[422] = rol32(x[422] ^ x[242],  12);  x[398] = rol32(x[398] ^  31,  12);
        x[261] += x[339];  x[183] = rol32(x[183] ^ x[261],  16);  x[417] = rol32(x[417] ^  37,  16);
        x[221] += x[612];  x[456] = rol32(x[456] ^ x[221],   7);  x[377] = rol32(x[377] ^  41,   7);
        x[187] += x[450];  x[294] = rol32(x[294] ^ x[187],   8);  x[343] = rol32(x[343] ^  43,   8);
        x[298] += x[366];  x[210] = rol32(x[210] ^ x[298],  12);  x[454] = rol32(x[454] ^  47,  12);
        x[169] += x[621];  x[465] = rol32(x[465] ^ x[169],  16);  x[325] = rol32(x[325] ^  53,  16);
        x[ 21] += x[315];  x[159] = rol32(x[159] ^ x[ 21],   7);  x[177] = rol32(x[177] ^  59,   7);
        x[198] += x[582];  x[426] = rol32(x[426] ^ x[198],   8);  x[354] = rol32(x[354] ^  61,   8);
        x[ 85] += x[501];  x[345] = rol32(x[345] ^ x[ 85],  12);  x[241] = rol32(x[241] ^  67,  12);
        x[301] += x[365];  x[209] = rol32(x[209] ^ x[301],  16);  x[457] = rol32(x[457] ^  71,  16);
        x[237] += x[393];  x[237] = rol32(x[237] ^ x[237],   7);  x[393] = rol32(x[393] ^  73,   7);
        x[ 87] += x[507];  x[351] = rol32(x[351] ^ x[ 87],   8);  x[243] = rol32(x[243] ^  79,   8);
        x[ 27] += x[328];  x[172] = rol32(x[172] ^ x[ 27],  12);  x[183] = rol32(x[183] ^  83,  12);
        x[ 89] += x[324];  x[168] = rol32(x[168] ^ x[ 89],  16);  x[245] = rol32(x[245] ^  89,  16);
        x[ 59] += x[337];  x[181] = rol32(x[181] ^ x[ 59],   7);  x[215] = rol32(x[215] ^  97,   7);
        x[239] += x[475];  x[319] = rol32(x[319] ^ x[239],   8);  x[395] = rol32(x[395] ^ 101,   8);
        x[ 28] += x[529];  x[373] = rol32(x[373] ^ x[ 28],  12);  x[184] = rol32(x[184] ^ 103,  12);
        x[251] += x[511];  x[355] = rol32(x[355] ^ x[251],  16);  x[407] = rol32(x[407] ^ 107,  16);
        x[287] += x[412];  x[256] = rol32(x[256] ^ x[287],   7);  x[443] = rol32(x[443] ^ 109,   7);
        x[ 13] += x[535];  x[379] = rol32(x[379] ^ x[ 13],   8);  x[169] = rol32(x[169] ^ 113,   8);
        x[114] += x[465];  x[309] = rol32(x[309] ^ x[114],  12);  x[270] = rol32(x[270] ^ 127,  12);
        x[155] += x[440];  x[284] = rol32(x[284] ^ x[155],  16);  x[311] = rol32(x[311] ^ 131,  16);
        x[ 48] += x[442];  x[286] = rol32(x[286] ^ x[ 48],   7);  x[204] = rol32(x[204] ^ 137,   7);
        x[ 60] += x[533];  x[377] = rol32(x[377] ^ x[ 60],   8);  x[216] = rol32(x[216] ^ 139,   8);
        x[119] += x[532];  x[376] = rol32(x[376] ^ x[119],  12);  x[275] = rol32(x[275] ^ 149,  12);
        x[215] += x[571];  x[415] = rol32(x[415] ^ x[215],  16);  x[371] = rol32(x[371] ^ 151,  16);
        x[227] += x[557];  x[401] = rol32(x[401] ^ x[227],   7);  x[383] = rol32(x[383] ^ 157,   7);
        x[103] += x[429];  x[273] = rol32(x[273] ^ x[103],   8);  x[259] = rol32(x[259] ^ 163,   8);
        x[  1] += x[330];  x[174] = rol32(x[174] ^ x[  1],  12);  x[157] = rol32(x[157] ^ 167,  12);
        x[238] += x[352];  x[196] = rol32(x[196] ^ x[238],  16);  x[394] = rol32(x[394] ^ 173,  16);
        x[117] += x[449];  x[293] = rol32(x[293] ^ x[117],   7);  x[273] = rol32(x[273] ^ 179,   7);
        x[250] += x[446];  x[290] = rol32(x[290] ^ x[250],   8);  x[406] = rol32(x[406] ^ 181,   8);
        x[154] += x[604];  x[448] = rol32(x[448] ^ x[154],  12);  x[310] = rol32(x[310] ^ 191,  12);
        x[ 73] += x[546];  x[390] = rol32(x[390] ^ x[ 73],  16);  x[229] = rol32(x[229] ^ 193,  16);
        x[ 29] += x[592];  x[436] = rol32(x[436] ^ x[ 29],   7);  x[185] = rol32(x[185] ^ 197,   7);
        x[ 30] += x[369];  x[213] = rol32(x[213] ^ x[ 30],   8);  x[186] = rol32(x[186] ^ 199,   8);
        x[164] += x[590];  x[434] = rol32(x[434] ^ x[164],  12);  x[320] = rol32(x[320] ^ 211,  12);
        x[ 49] += x[372];  x[216] = rol32(x[216] ^ x[ 49],  16);  x[205] = rol32(x[205] ^ 223,  16);
        x[168] += x[594];  x[438] = rol32(x[438] ^ x[168],   7);  x[324] = rol32(x[324] ^ 227,   7);
        x[222] += x[355];  x[199] = rol32(x[199] ^ x[222],   8);  x[378] = rol32(x[378] ^ 229,   8);
        x[ 52] += x[572];  x[416] = rol32(x[416] ^ x[ 52],  12);  x[208] = rol32(x[208] ^ 233,  12);
        x[ 88] += x[362];  x[206] = rol32(x[206] ^ x[ 88],  16);  x[244] = rol32(x[244] ^ 239,  16);
        x[262] += x[540];  x[384] = rol32(x[384] ^ x[262],   7);  x[418] = rol32(x[418] ^ 241,   7);
        x[ 69] += x[498];  x[342] = rol32(x[342] ^ x[ 69],   8);  x[225] = rol32(x[225] ^ 251,   8);
        x[ 45] += x[367];  x[211] = rol32(x[211] ^ x[ 45],  12);  x[201] = rol32(x[201] ^ 257,  12);
        x[185] += x[433];  x[277] = rol32(x[277] ^ x[185],  16);  x[341] = rol32(x[341] ^ 263,  16);
        x[127] += x[623];  x[467] = rol32(x[467] ^ x[127],   7);  x[283] = rol32(x[283] ^ 269,   7);
        x[180] += x[431];  x[275] = rol32(x[275] ^ x[180],   8);  x[336] = rol32(x[336] ^ 271,   8);
        x[206] += x[457];  x[301] = rol32(x[301] ^ x[206],  12);  x[362] = rol32(x[362] ^ 277,  12);
        x[ 20] += x[316];  x[160] = rol32(x[160] ^ x[ 20],  16);  x[176] = rol32(x[176] ^ 281,  16);
        x[ 22] += x[520];  x[364] = rol32(x[364] ^ x[ 22],   7);  x[178] = rol32(x[178] ^ 283,   7);
        x[  9] += x[576];  x[420] = rol32(x[420] ^ x[  9],   8);  x[165] = rol32(x[165] ^ 293,   8);
        x[209] += x[360];  x[204] = rol32(x[204] ^ x[209],  12);  x[365] = rol32(x[365] ^ 307,  12);
        x[106] += x[524];  x[368] = rol32(x[368] ^ x[106],  16);  x[262] = rol32(x[262] ^ 311,  16);
        x[ 74] += x[454];  x[298] = rol32(x[298] ^ x[ 74],   7);  x[230] = rol32(x[230] ^   2,   7);
        x[249] += x[344];  x[188] = rol32(x[188] ^ x[249],   8);  x[405] = rol32(x[405] ^   3,   8);
        x[278] += x[536];  x[380] = rol32(x[380] ^ x[278],  12);  x[434] = rol32(x[434] ^   5,  12);
        x[277] += x[523];  x[367] = rol32(x[367] ^ x[277],  16);  x[433] = rol32(x[433] ^   7,  16);
        x[122] += x[391];  x[235] = rol32(x[235] ^ x[122],   7);  x[278] = rol32(x[278] ^  11,   7);
        x[300] += x[384];  x[228] = rol32(x[228] ^ x[300],   8);  x[456] = rol32(x[456] ^  13,   8);
        x[ 23] += x[377];  x[221] = rol32(x[221] ^ x[ 23],  12);  x[179] = rol32(x[179] ^  17,  12);
        x[202] += x[559];  x[403] = rol32(x[403] ^ x[202],  16);  x[358] = rol32(x[358] ^  19,  16);
        x[131] += x[452];  x[296] = rol32(x[296] ^ x[131],   7);  x[287] = rol32(x[287] ^  23,   7);
        x[ 99] += x[405];  x[249] = rol32(x[249] ^ x[ 99],   8);  x[255] = rol32(x[255] ^  29,   8);
        x[211] += x[579];  x[423] = rol32(x[423] ^ x[211],  12);  x[367] = rol32(x[367] ^  31,  12);
        x[264] += x[615];  x[459] = rol32(x[459] ^ x[264],  16);  x[420] = rol32(x[420] ^  37,  16);
        x[ 57] += x[561];  x[405] = rol32(x[405] ^ x[ 57],   7);  x[213] = rol32(x[213] ^  41,   7);
        x[285] += x[321];  x[165] = rol32(x[165] ^ x[285],   8);  x[441] = rol32(x[441] ^  43,   8);
        x[ 76] += x[484];  x[328] = rol32(x[328] ^ x[ 76],  12);  x[232] = rol32(x[232] ^  47,  12);
        x[246] += x[460];  x[304] = rol32(x[304] ^ x[246],  16);  x[402] = rol32(x[402] ^  53,  16);
        x[292] += x[522];  x[366] = rol32(x[366] ^ x[292],   7);  x[448] = rol32(x[448] ^  59,   7);
        x[203] += x[389];  x[233] = rol32(x[233] ^ x[203],   8);  x[359] = rol32(x[359] ^  61,   8);
        x[ 79] += x[509];  x[353] = rol32(x[353] ^ x[ 79],  12);  x[235] = rol32(x[235] ^  67,  12);
        x[ 72] += x[563];  x[407] = rol32(x[407] ^ x[ 72],  16);  x[228] = rol32(x[228] ^  71,  16);
        x[142] += x[332];  x[176] = rol32(x[176] ^ x[142],   7);  x[298] = rol32(x[298] ^  73,   7);
        x[293] += x[430];  x[274] = rol32(x[274] ^ x[293],   8);  x[449] = rol32(x[449] ^  79,   8);
        x[ 96] += x[335];  x[179] = rol32(x[179] ^ x[ 96],  12);  x[252] = rol32(x[252] ^  83,  12);
        x[ 75] += x[565];  x[409] = rol32(x[409] ^ x[ 75],  16);  x[231] = rol32(x[231] ^  89,  16);
        x[210] += x[399];  x[243] = rol32(x[243] ^ x[210],   7);  x[366] = rol32(x[366] ^  97,   7);
        x[179] += x[514];  x[358] = rol32(x[358] ^ x[179],   8);  x[335] = rol32(x[335] ^ 101,   8);
        x[ 10] += x[323];  x[167] = rol32(x[167] ^ x[ 10],  12);  x[166] = rol32(x[166] ^ 103,  12);
        x[  7] += x[558];  x[402] = rol32(x[402] ^ x[  7],  16);  x[163] = rol32(x[163] ^ 107,  16);
        x[101] += x[458];  x[302] = rol32(x[302] ^ x[101],   7);  x[257] = rol32(x[257] ^ 109,   7);
        x[283] += x[327];  x[171] = rol32(x[171] ^ x[283],   8);  x[439] = rol32(x[439] ^ 113,   8);
        x[ 40] += x[586];  x[430] = rol32(x[430] ^ x[ 40],  12);  x[196] = rol32(x[196] ^ 127,  12);
        x[208] += x[380];  x[224] = rol32(x[224] ^ x[208],  16);  x[364] = rol32(x[364] ^ 131,  16);
        x[126] += x[567];  x[411] = rol32(x[411] ^ x[126],   7);  x[282] = rol32(x[282] ^ 137,   7);
        x[ 98] += x[487];  x[331] = rol32(x[331] ^ x[ 98],   8);  x[254] = rol32(x[254] ^ 139,   8);
        x[235] += x[447];  x[291] = rol32(x[291] ^ x[235],  12);  x[391] = rol32(x[391] ^ 149,  12);
        x[201] += x[551];  x[395] = rol32(x[395] ^ x[201],  16);  x[357] = rol32(x[357] ^ 151,  16);
        x[ 63] += x[368];  x[212] = rol32(x[212] ^ x[ 63],   7);  x[219] = rol32(x[219] ^ 157,   7);
        x[280] += x[539];  x[383] = rol32(x[383] ^ x[280],   8);  x[436] = rol32(x[436] ^ 163,   8);
        x[225] += x[541];  x[385] = rol32(x[385] ^ x[225],  12);  x[381] = rol32(x[381] ^ 167,  12);
        x[257] += x[556];  x[400] = rol32(x[400] ^ x[257],  16);  x[413] = rol32(x[413] ^ 173,  16);
        x[196] += x[506];  x[350] = rol32(x[350] ^ x[196],   7);  x[352] = rol32(x[352] ^ 179,   7);
        x[184] += x[418];  x[262] = rol32(x[262] ^ x[184],   8);  x[340] = rol32(x[340] ^ 181,   8);
        x[ 97] += x[602];  x[446] = rol32(x[446] ^ x[ 97],  12);  x[253] = rol32(x[253] ^ 191,  12);
        x[295] += x[570];  x[414] = rol32(x[414] ^ x[295],  16);  x[451] = rol32(x[451] ^ 193,  16);
        x[ 92] += x[378];  x[222] = rol32(x[222] ^ x[ 92],   7);  x[248] = rol32(x[248] ^ 197,   7);
        x[240] += x[353];  x[197] = rol32(x[197] ^ x[240],   8);  x[396] = rol32(x[396] ^ 199,   8);
        x[ 32] += x[606];  x[450] = rol32(x[450] ^ x[ 32],  12);  x[188] = rol32(x[188] ^ 211,  12);
        x[267] += x[313];  x[157] = rol32(x[157] ^ x[267],  16);  x[423] = rol32(x[423] ^ 223,  16);
        x[ 26] += x[448];  x[292] = rol32(x[292] ^ x[ 26],   7);  x[182] = rol32(x[182] ^ 227,   7);
        x[282] += x[456];  x[300] = rol32(x[300] ^ x[282],   8);  x[438] = rol32(x[438] ^ 229,   8);
        x[305] += x[396];  x[240] = rol32(x[240] ^ x[305],  12);  x[461] = rol32(x[461] ^ 233,  12);
        x[110] += x[617];  x[461] = rol32(x[461] ^ x[110],  16);  x[266] = rol32(x[266] ^ 239,  16);
        x[172] += x[486];  x[330] = rol32(x[330] ^ x[172],   7);  x[328] = rol32(x[328] ^ 241,   7);
        x[173] += x[545];  x[389] = rol32(x[389] ^ x[173],   8);  x[329] = rol32(x[329] ^ 251,   8);
        x[279] += x[387];  x[231] = rol32(x[231] ^ x[279],  12);  x[435] = rol32(x[435] ^ 257,  12);
        x[156] += x[453];  x[297] = rol32(x[297] ^ x[156],  16);  x[312] = rol32(x[312] ^ 263,  16);
        x[ 18] += x[423];  x[267] = rol32(x[267] ^ x[ 18],   7);  x[174] = rol32(x[174] ^ 269,   7);
        x[ 82] += x[326];  x[170] = rol32(x[170] ^ x[ 82],   8);  x[238] = rol32(x[238] ^ 271,   8);
        x[128] += x[349];  x[193] = rol32(x[193] ^ x[128],  12);  x[284] = rol32(x[284] ^ 277,  12);
        x[182] += x[482];  x[326] = rol32(x[326] ^ x[182],  16);  x[338] = rol32(x[338] ^ 281,  16);
        x[288] += x[462];  x[306] = rol32(x[306] ^ x[288],   7);  x[444] = rol32(x[444] ^ 283,   7);
        x[124] += x[341];  x[185] = rol32(x[185] ^ x[124],   8);  x[280] = rol32(x[280] ^ 293,   8);
        x[223] += x[596];  x[440] = rol32(x[440] ^ x[223],  12);  x[379] = rol32(x[379] ^ 307,  12);
        x[ 39] += x[605];  x[449] = rol32(x[449] ^ x[ 39],  16);  x[195] = rol32(x[195] ^ 311,  16);
        x[255] += x[345];  x[189] = rol32(x[189] ^ x[255],   7);  x[411] = rol32(x[411] ^   2,   7);
        x[104] += x[534];  x[378] = rol32(x[378] ^ x[104],   8);  x[260] = rol32(x[260] ^   3,   8);
        x[162] += x[531];  x[375] = rol32(x[375] ^ x[162],  12);  x[318] = rol32(x[318] ^   5,  12);
        x[105] += x[428];  x[272] = rol32(x[272] ^ x[105],  16);  x[261] = rol32(x[261] ^   7,  16);
        x[273] += x[547];  x[391] = rol32(x[391] ^ x[273],   7);  x[429] = rol32(x[429] ^  11,   7);
        x[217] += x[538];  x[382] = rol32(x[382] ^ x[217],   8);  x[373] = rol32(x[373] ^  13,   8);
        x[102] += x[379];  x[223] = rol32(x[223] ^ x[102],  12);  x[258] = rol32(x[258] ^  17,  12);
        x[291] += x[358];  x[202] = rol32(x[202] ^ x[291],  16);  x[447] = rol32(x[447] ^  19,  16);
        x[284] += x[340];  x[184] = rol32(x[184] ^ x[284],   7);  x[440] = rol32(x[440] ^  23,   7);
        x[146] += x[363];  x[207] = rol32(x[207] ^ x[146],   8);  x[302] = rol32(x[302] ^  29,   8);
        x[ 34] += x[512];  x[356] = rol32(x[356] ^ x[ 34],  12);  x[190] = rol32(x[190] ^  31,  12);
        x[274] += x[564];  x[408] = rol32(x[408] ^ x[274],  16);  x[430] = rol32(x[430] ^  37,  16);
        x[200] += x[616];  x[460] = rol32(x[460] ^ x[200],   7);  x[356] = rol32(x[356] ^  41,   7);
        x[111] += x[312];  x[156] = rol32(x[156] ^ x[111],   8);  x[267] = rol32(x[267] ^  43,   8);
        x[218] += x[495];  x[339] = rol32(x[339] ^ x[218],  12);  x[374] = rol32(x[374] ^  47,  12);
        x[199] += x[503];  x[347] = rol32(x[347] ^ x[199],  16);  x[355] = rol32(x[355] ^  53,  16);
        x[ 58] += x[469];  x[313] = rol32(x[313] ^ x[ 58],   7);  x[214] = rol32(x[214] ^  59,   7);
        x[108] += x[476];  x[320] = rol32(x[320] ^ x[108],   8);  x[264] = rol32(x[264] ^  61,   8);
        x[236] += x[607];  x[451] = rol32(x[451] ^ x[236],  12);  x[392] = rol32(x[392] ^  67,  12);
        x[121] += x[549];  x[393] = rol32(x[393] ^ x[121],  16);  x[277] = rol32(x[277] ^  71,  16);
        x[191] += x[601];  x[445] = rol32(x[445] ^ x[191],   7);  x[347] = rol32(x[347] ^  73,   7);
        x[189] += x[455];  x[299] = rol32(x[299] ^ x[189],   8);  x[345] = rol32(x[345] ^  79,   8);
        x[ 25] += x[382];  x[226] = rol32(x[226] ^ x[ 25],  12);  x[181] = rol32(x[181] ^  83,  12);
        x[ 84] += x[494];  x[338] = rol32(x[338] ^ x[ 84],  16);  x[240] = rol32(x[240] ^  89,  16);
        x[195] += x[474];  x[318] = rol32(x[318] ^ x[195],   7);  x[351] = rol32(x[351] ^  97,   7);
        x[186] += x[397];  x[241] = rol32(x[241] ^ x[186],   8);  x[342] = rol32(x[342] ^ 101,   8);
        x[259] += x[343];  x[187] = rol32(x[187] ^ x[259],  12);  x[415] = rol32(x[415] ^ 103,  12);
        x[247] += x[574];  x[418] = rol32(x[418] ^ x[247],  16);  x[403] = rol32(x[403] ^ 107,  16);
        x[125] += x[420];  x[264] = rol32(x[264] ^ x[125],   7);  x[281] = rol32(x[281] ^ 109,   7);
        x[132] += x[459];  x[303] = rol32(x[303] ^ x[132],   8);  x[288] = rol32(x[288] ^ 113,   8);
        x[ 36] += x[314];  x[158] = rol32(x[158] ^ x[ 36],  12);  x[192] = rol32(x[192] ^ 127,  12);
        x[183] += x[437];  x[281] = rol32(x[281] ^ x[183],  16);  x[339] = rol32(x[339] ^ 131,  16);
        x[  4] += x[436];  x[280] = rol32(x[280] ^ x[  4],   7);  x[160] = rol32(x[160] ^ 137,   7);
        x[ 67] += x[398];  x[242] = rol32(x[242] ^ x[ 67],   8);  x[223] = rol32(x[223] ^ 139,   8);
        x[ 16] += x[548];  x[392] = rol32(x[392] ^ x[ 16],  12);  x[172] = rol32(x[172] ^ 149,  12);
        x[150] += x[569];  x[413] = rol32(x[413] ^ x[150],  16);  x[306] = rol32(x[306] ^ 151,  16);
        x[294] += x[419];  x[263] = rol32(x[263] ^ x[294],   7);  x[450] = rol32(x[450] ^ 157,   7);
        x[190] += x[502];  x[346] = rol32(x[346] ^ x[190],   8);  x[346] = rol32(x[346] ^ 163,   8);
        x[160] += x[370];  x[214] = rol32(x[214] ^ x[160],  12);  x[316] = rol32(x[316] ^ 167,  12);
        x[138] += x[461];  x[305] = rol32(x[305] ^ x[138],  16);  x[294] = rol32(x[294] ^ 173,  16);
        x[303] += x[410];  x[254] = rol32(x[254] ^ x[303],   7);  x[459] = rol32(x[459] ^ 179,   7);
        x[ 38] += x[550];  x[394] = rol32(x[394] ^ x[ 38],   8);  x[194] = rol32(x[194] ^ 181,   8);
        x[ 12] += x[481];  x[325] = rol32(x[325] ^ x[ 12],  12);  x[168] = rol32(x[168] ^ 191,  12);
        x[167] += x[422];  x[266] = rol32(x[266] ^ x[167],  16);  x[323] = rol32(x[323] ^ 193,  16);
        x[241] += x[492];  x[336] = rol32(x[336] ^ x[241],   7);  x[397] = rol32(x[397] ^ 197,   7);
        x[112] += x[342];  x[186] = rol32(x[186] ^ x[112],   8);  x[268] = rol32(x[268] ^ 199,   8);
        x[220] += x[530];  x[374] = rol32(x[374] ^ x[220],  12);  x[376] = rol32(x[376] ^ 211,  12);
        x[151] += x[562];  x[406] = rol32(x[406] ^ x[151],  16);  x[307] = rol32(x[307] ^ 223,  16);
        x[244] += x[356];  x[200] = rol32(x[200] ^ x[244],   7);  x[400] = rol32(x[400] ^ 227,   7);
        x[ 50] += x[374];  x[218] = rol32(x[218] ^ x[ 50],   8);  x[206] = rol32(x[206] ^ 229,   8);
        x[137] += x[608];  x[452] = rol32(x[452] ^ x[137],  12);  x[293] = rol32(x[293] ^ 233,  12);
        x[258] += x[331];  x[175] = rol32(x[175] ^ x[258],  16);  x[414] = rol32(x[414] ^ 239,  16);
        x[311] += x[357];  x[201] = rol32(x[201] ^ x[311],   7);  x[467] = rol32(x[467] ^ 241,   7);
        x[171] += x[347];  x[191] = rol32(x[191] ^ x[171],   8);  x[327] = rol32(x[327] ^ 251,   8);
        x[271] += x[386];  x[230] = rol32(x[230] ^ x[271],  12);  x[427] = rol32(x[427] ^ 257,  12);
        x[100] += x[528];  x[372] = rol32(x[372] ^ x[100],  16);  x[256] = rol32(x[256] ^ 263,  16);
        x[226] += x[499];  x[343] = rol32(x[343] ^ x[226],   7);  x[382] = rol32(x[382] ^ 269,   7);
        x[ 46] += x[467];  x[311] = rol32(x[311] ^ x[ 46],   8);  x[202] = rol32(x[202] ^ 271,   8);
        x[178] += x[585];  x[429] = rol32(x[429] ^ x[178],  12);  x[334] = rol32(x[334] ^ 277,  12);
        x[308] += x[566];  x[410] = rol32(x[410] ^ x[308],  16);  x[464] = rol32(x[464] ^ 281,  16);
        x[286] += x[351];  x[195] = rol32(x[195] ^ x[286],   7);  x[442] = rol32(x[442] ^ 283,   7);
        x[  2] += x[383];  x[227] = rol32(x[227] ^ x[  2],   8);  x[158] = rol32(x[158] ^ 293,   8);
        x[ 15] += x[496];  x[340] = rol32(x[340] ^ x[ 15],  12);  x[171] = rol32(x[171] ^ 307,  12);
        x[148] += x[516];  x[360] = rol32(x[360] ^ x[148],  16);  x[304] = rol32(x[304] ^ 311,  16);
        x[ 61] += x[346];  x[190] = rol32(x[190] ^ x[ 61],   7);  x[217] = rol32(x[217] ^   2,   7);
        x[306] += x[573];  x[417] = rol32(x[417] ^ x[306],   8);  x[462] = rol32(x[462] ^   3,   8);
        x[233] += x[464];  x[308] = rol32(x[308] ^ x[233],  12);  x[389] = rol32(x[389] ^   5,  12);
        x[133] += x[598];  x[442] = rol32(x[442] ^ x[133],  16);  x[289] = rol32(x[289] ^   7,  16);
        x[ 24] += x[411];  x[255] = rol32(x[255] ^ x[ 24],   7);  x[180] = rol32(x[180] ^  11,   7);
        x[177] += x[394];  x[238] = rol32(x[238] ^ x[177],   8);  x[333] = rol32(x[333] ^  13,   8);
        x[ 43] += x[603];  x[447] = rol32(x[447] ^ x[ 43],  12);  x[199] = rol32(x[199] ^  17,  12);
        x[268] += x[400];  x[244] = rol32(x[244] ^ x[268],  16);  x[424] = rol32(x[424] ^  19,  16);
        x[228] += x[385];  x[229] = rol32(x[229] ^ x[228],   7);  x[384] = rol32(x[384] ^  23,   7);
        x[245] += x[485];  x[329] = rol32(x[329] ^ x[245],   8);  x[401] = rol32(x[401] ^  29,   8);
        x[ 86] += x[599];  x[443] = rol32(x[443] ^ x[ 86],  12);  x[242] = rol32(x[242] ^  31,  12);
        x[147] += x[517];  x[361] = rol32(x[361] ^ x[147],  16);  x[303] = rol32(x[303] ^  37,  16);
        x[231] += x[375];  x[219] = rol32(x[219] ^ x[231],   7);  x[387] = rol32(x[387] ^  41,   7);
        x[ 42] += x[568];  x[412] = rol32(x[412] ^ x[ 42],   8);  x[198] = rol32(x[198] ^  43,   8);
        x[ 62] += x[407];  x[251] = rol32(x[251] ^ x[ 62],  12);  x[218] = rol32(x[218] ^  47,  12);
        x[161] += x[587];  x[431] = rol32(x[431] ^ x[161],  16);  x[317] = rol32(x[317] ^  53,  16);
        x[163] += x[544];  x[388] = rol32(x[388] ^ x[163],   7);  x[319] = rol32(x[319] ^  59,   7);
        x[207] += x[409];  x[253] = rol32(x[253] ^ x[207],   8);  x[363] = rol32(x[363] ^  61,   8);
        x[296] += x[600];  x[444] = rol32(x[444] ^ x[296],  12);  x[452] = rol32(x[452] ^  67,  12);
        x[304] += x[609];  x[453] = rol32(x[453] ^ x[304],  16);  x[460] = rol32(x[460] ^  71,  16);
        x[212] += x[413];  x[257] = rol32(x[257] ^ x[212],   7);  x[368] = rol32(x[368] ^  73,   7);
        x[123] += x[500];  x[344] = rol32(x[344] ^ x[123],   8);  x[279] = rol32(x[279] ^  79,   8);
        x[309] += x[595];  x[439] = rol32(x[439] ^ x[309],  12);  x[465] = rol32(x[465] ^  83,  12);
        x[248] += x[425];  x[269] = rol32(x[269] ^ x[248],  16);  x[404] = rol32(x[404] ^  89,  16);
        x[ 94] += x[381];  x[225] = rol32(x[225] ^ x[ 94],   7);  x[250] = rol32(x[250] ^  97,   7);
        x[157] += x[491];  x[335] = rol32(x[335] ^ x[157],   8);  x[313] = rol32(x[313] ^ 101,   8);
        x[232] += x[373];  x[217] = rol32(x[217] ^ x[232],  12);  x[388] = rol32(x[388] ^ 103,  12);
        x[ 17] += x[479];  x[323] = rol32(x[323] ^ x[ 17],  16);  x[173] = rol32(x[173] ^ 107,  16);
        x[243] += x[435];  x[279] = rol32(x[279] ^ x[243],   7);  x[399] = rol32(x[399] ^ 109,   7);
        x[224] += x[620];  x[464] = rol32(x[464] ^ x[224],   8);  x[380] = rol32(x[380] ^ 113,   8);
        x[193] += x[483];  x[327] = rol32(x[327] ^ x[193],  12);  x[349] = rol32(x[349] ^ 127,  12);
        x[144] += x[597];  x[441] = rol32(x[441] ^ x[144],  16);  x[300] = rol32(x[300] ^ 131,  16);
        x[252] += x[493];  x[337] = rol32(x[337] ^ x[252],   7);  x[408] = rol32(x[408] ^ 137,   7);
        x[153] += x[619];  x[463] = rol32(x[463] ^ x[153],   8);  x[309] = rol32(x[309] ^ 139,   8);
        x[ 95] += x[504];  x[348] = rol32(x[348] ^ x[ 95],  12);  x[251] = rol32(x[251] ^ 149,  12);
        x[141] += x[364];  x[208] = rol32(x[208] ^ x[141],  16);  x[297] = rol32(x[297] ^ 151,  16);
        x[113] += x[488];  x[332] = rol32(x[332] ^ x[113],   7);  x[269] = rol32(x[269] ^ 157,   7);
        x[  6] += x[434];  x[278] = rol32(x[278] ^ x[  6],   8);  x[162] = rol32(x[162] ^ 163,   8);
        x[ 51] += x[361];  x[205] = rol32(x[205] ^ x[ 51],  12);  x[207] = rol32(x[207] ^ 167,  12);
        x[ 55] += x[505];  x[349] = rol32(x[349] ^ x[ 55],  16);  x[211] = rol32(x[211] ^ 173,  16);
        x[166] += x[614];  x[458] = rol32(x[458] ^ x[166],   7);  x[322] = rol32(x[322] ^ 179,   7);
        x[256] += x[478];  x[322] = rol32(x[322] ^ x[256],   8);  x[412] = rol32(x[412] ^ 181,   8);
        x[  0] += x[613];  x[457] = rol32(x[457] ^ x[  0],  12);  x[156] = rol32(x[156] ^ 191,  12);
        x[120] += x[468];  x[312] = rol32(x[312] ^ x[120],  16);  x[276] = rol32(x[276] ^ 193,  16);
        x[197] += x[466];  x[310] = rol32(x[310] ^ x[197],   7);  x[353] = rol32(x[353] ^ 197,   7);
        x[181] += x[515];  x[359] = rol32(x[359] ^ x[181],   8);  x[337] = rol32(x[337] ^ 199,   8);
        x[192] += x[406];  x[250] = rol32(x[250] ^ x[192],  12);  x[348] = rol32(x[348] ^ 211,  12);
        x[216] += x[622];  x[466] = rol32(x[466] ^ x[216],  16);  x[372] = rol32(x[372] ^ 223,  16);
        x[ 91] += x[542];  x[386] = rol32(x[386] ^ x[ 91],   7);  x[247] = rol32(x[247] ^ 227,   7);
        x[289] += x[390];  x[234] = rol32(x[234] ^ x[289],   8);  x[445] = rol32(x[445] ^ 229,   8);
        x[297] += x[402];  x[246] = rol32(x[246] ^ x[297],  12);  x[453] = rol32(x[453] ^ 233,  12);
        x[ 54] += x[441];  x[285] = rol32(x[285] ^ x[ 54],  16);  x[210] = rol32(x[210] ^ 239,  16);
        x[219] += x[322];  x[166] = rol32(x[166] ^ x[219],   7);  x[375] = rol32(x[375] ^ 241,   7);
        x[272] += x[575];  x[419] = rol32(x[419] ^ x[272],   8);  x[428] = rol32(x[428] ^ 251,   8);
        x[ 33] += x[414];  x[258] = rol32(x[258] ^ x[ 33],  12);  x[189] = rol32(x[189] ^ 257,  12);
        x[ 47] += x[470];  x[314] = rol32(x[314] ^ x[ 47],  16);  x[203] = rol32(x[203] ^ 263,  16);
    }
}

