/* 
   A C-program for MT19937, with initialization improved 2002/1/26.
   Coded by Takuji Nishimura and Makoto Matsumoto.

   Before using, initialize the state by using mt_init_genrand(seed)  
   or mt_init_by_array(init_key, key_length).

   Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
   All rights reserved.                          

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

     1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

     2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

     3. The names of its contributors may not be used to endorse or promote 
        products derived from this software without specific prior written 
        permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


   Any feedback is very welcome.
   http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html
   email: m-mat @ math.sci.hiroshima-u.ac.jp (remove space)
*/

#include <stdio.h>
#include <stdlib.h>
#include "include/mt19937.h"

// static unsigned long mt[N]; /* the array for the state vector  */
// static int mti=N+1; /* mti==N+1 means mt[N] is not initialized */

mt19937_ctx* mt19937(void) {

    mt19937_ctx* mt = malloc(sizeof(mt19937_ctx));
    
    if (mt == NULL) {
        perror("mt19937(): Cannot allocated memory!\n");
        exit(1);
    }

    mt->mti = N + 1;

    return mt;
}

void mt19937_free(mt19937_ctx* context) {
    free(context);
}

/* initializes mt[N] with a seed */
void mt_init_genrand(mt19937_ctx* context, unsigned long s)
{
    context->mt[0]= s & 0xffffffffUL;
    for (context->mti=1; context->mti<N; context->mti++) {
        context->mt[context->mti] = 
	    (1812433253UL * (context->mt[context->mti-1] ^ (context->mt[context->mti-1] >> 30)) + context->mti); 
        /* See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier. */
        /* In the previous versions, MSBs of the seed affect   */
        /* only MSBs of the array mt[].                        */
        /* 2002/01/09 modified by Makoto Matsumoto             */
        context->mt[context->mti] &= 0xffffffffUL;
        /* for >32 bit machines */
    }
}

/* initialize by an array with array-length */
/* init_key is the array for initializing keys */
/* key_length is its length */
/* slight change for C++, 2004/2/26 */
void mt_init_by_array(mt19937_ctx* context, unsigned long init_key[], int key_length)
{
    int i, j, k;
    unsigned long *mt = context->mt;
    
    mt_init_genrand(context, 19650218UL);
    
    i=1; j=0;
    k = (N>key_length ? N : key_length);
    for (; k; k--) {
        mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1664525UL))
          + init_key[j] + j; /* non linear */
        mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        i++; j++;
        if (i>=N) { mt[0] = mt[N-1]; i=1; }
        if (j>=key_length) j=0;
    }
    for (k=N-1; k; k--) {
        mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1566083941UL))
          - i; /* non linear */
        mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        i++;
        if (i>=N) { mt[0] = mt[N-1]; i=1; }
    }

    mt[0] = 0x80000000UL; /* MSB is 1; assuring non-zero initial array */ 
}

/* generates a random number on [0,0xffffffff]-interval */
unsigned long mt_genrand_int32(mt19937_ctx* context)
{
    unsigned long y;
    static unsigned long mag01[2]={0x0UL, MATRIX_A};

    unsigned long* mt = context->mt;
    int* mti = &context->mti;

    /* mag01[x] = x * MATRIX_A  for x=0,1 */

    if (*mti >= N) { /* generate N words at one time */
        int kk;

        if (*mti == N+1)   /* if mt_init_genrand() has not been called, */
            mt_init_genrand(context, 5489UL); /* a default initial seed is used */

        for (kk=0;kk<N-M;kk++) {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+M] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }
        for (;kk<N-1;kk++) {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+(M-N)] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }
        y = (mt[N-1]&UPPER_MASK)|(mt[0]&LOWER_MASK);
        mt[N-1] = mt[M-1] ^ (y >> 1) ^ mag01[y & 0x1UL];

        *mti = 0;
    }
  
    y = mt[(*mti)++];

    /* Tempering */
    y ^= (y >> 11);
    y ^= (y << 7) & 0x9d2c5680UL;
    y ^= (y << 15) & 0xefc60000UL;
    y ^= (y >> 18);

    return y;
}

/* generates a random number on [0,0x7fffffff]-interval */
long mt_genrand_int31(mt19937_ctx* context)
{
    return (long)(mt_genrand_int32(context)>>1);
}

/* generates a random number on [0,1]-real-interval */
double mt_genrand_real1(mt19937_ctx* context)
{
    return mt_genrand_int32(context)*(1.0/4294967295.0); 
    /* divided by 2^32-1 */ 
}

/* generates a random number on [0,1)-real-interval */
double mt_genrand_real2(mt19937_ctx* context)
{
    return mt_genrand_int32(context)*(1.0/4294967296.0); 
    /* divided by 2^32 */
}

/* generates a random number on (0,1)-real-interval */
double mt_genrand_real3(mt19937_ctx* context)
{
    return (((double)mt_genrand_int32(context)) + 0.5)*(1.0/4294967296.0); 
    /* divided by 2^32 */
}

/* generates a random number on [0,1) with 53-bit resolution*/
double mt_genrand_res53(mt19937_ctx* context) 
{ 
    unsigned long a=mt_genrand_int32(context)>>5, b=mt_genrand_int32(context)>>6; 
    return(a*67108864.0+b)*(1.0/9007199254740992.0); 
} 
/* These real versions are due to Isaku Wada, 2002/01/09 added */

/*
 * Calculate a uniformly distributed random number less than upper_bound
 * avoiding "modulo bias".
 *
 * Uniformity is achieved by generating new random numbers until the one
 * returned is outside the range [0, 2**32 % upper_bound).  This
 * guarantees the selected random number will be inside
 * [2**32 % upper_bound, 2**32) which maps back to [0, upper_bound)
 * after reduction modulo upper_bound.
 *
 * original source: arc4random_uniform (arc4random_uniform.c)
 */
uint32_t mt_uniform32(mt19937_ctx *context, uint32_t upper)
{
    uint32_t r, min;
	unsigned iterations = 0;

    if (upper < 2)
        return 0;

    /* 2**32 % x == (2**32 - x) % x */
    min = -upper % upper;

    /*
	 * This could theoretically loop forever but each retry has
	 * p > 0.5 (worst case, usually far better) of selecting a
	 * number inside the range we need, so it should rarely need
	 * to re-roll.
	 */
    for (;;)
    {
		iterations++;
        r = mt_genrand_int32(context);
        if (r >= min)
            break;
		
		/* Safety check to prevent inifinte loop */
		if (iterations > 1000)
			return 0;
    }
    return r % upper;
}

