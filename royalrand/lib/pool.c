/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo@bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>
#include <signal.h>
#include "include/pool.h"
#include "include/crypt.h"
#include "include/chacha.h"
#include "include/aes.h"
#include "include/lfsr.h"
#include "include/definitions.h"
#include "include/blake3/blake3.h"

#ifdef __DEBUG__
#include "include/hexdump.h"
#include "include/debug.h"
#include <stdio.h>
#endif

// #define _TEST_THREAD_

/// DEV NOTES /////////////////////
//
// All function names ended with
// `_routine` should be strictly
// used in threads.
//
///////////////////////////////////

/**
 * @brief Initialize the pool16.
 * 
 * The pool is initialized with zeroes.
 * 
 * @param pool pool16_t* instance.
 */
void pool16_init(pool16_t* pool) {
    pool->count = 0;
    pool->cursor = 0;
    pool->iteration = 0;
    
    for (unsigned int i = 0; i < POOL16_SIZE; i++) {
        pool->pool[i] = 0;
    }
}

/**
 * @brief Insert a uint16_t value into the pool16 pool.
 * 
 * The pool iteration counter is automatically incremeneted here.
 * 
 * @param pool pool16_t* instance.
 * @param value uint16_t value.
 */
void pool16_insert(pool16_t* pool, uint16_t value) {
    
    uint16_t cur = (pool->cursor + 1) % POOL16_SIZE;

    if (pool->count < POOL16_SIZE) {
        /* add to next position */
        pool->pool[pool->count++] = (uint16_t)value;
    } else {
        /* insert in 0 */
        pool->pool[cur] = (uint16_t)value;
    }
    pool->cursor = cur;
    pool->iteration++;
}

/**
 * @brief Insert entropy into the pool16 pool.
 * 
 * @param pool pool16_t* instance.
 */
void pool16_insert_entropy(pool16_t* pool) {

    uint32_t ex, hw, os, r;
    uint16_t res;

    get_external_uint32(&ex);
    get_hardware_uint32(&hw);
    get_urandom_uint32(&os);
    get_ns_uint32(&r);

    res = ((ex ^ hw) & 0xffff) ^ ((os ^ r) & 0xffff);
#ifdef FORCE_16_BIT
    res = (0x7fff - (res & 0xfff)) + 0xfff;
#endif
    pool16_insert(pool, res);
}

/**
 * @brief Initialize the pool32 instance.
 * 
 * The pool is initialized with zeroes.
 * 
 * @param pool pool32_t* instance.
 */
void pool32_init(pool32_t* pool) {
    pool->count = 0;
    pool->cursor = 0;
    pool->iteration = 0;

    for (unsigned int i = 0; i < POOL32_SIZE; i++) {
        pool->pool[i] = 0;
    }
}
/**
 * @brief Insert a uint32_t value into the pool32 pool.
 * 
 * The pool iteration counter is automatically incremented here.
 * 
 * @param pool pool32_t* instance.
 * @param value uint32_t value.
 */
void pool32_insert(pool32_t* pool, uint32_t value) {
    
    uint16_t cur = (pool->cursor + 1) % POOL32_SIZE;

    if (pool->iteration == POOL32_RESET) {
        pool->iteration = 0;
        pool32_chacha(pool);
    }

    if (pool->count < POOL32_SIZE) {
        /* add to next position */
        pool->pool[pool->count++] = (uint32_t)value;
    } else {
        /* insert in 0 */
        pool->pool[cur] = (uint32_t)value;
    }
    pool->cursor = cur;
    pool->iteration++;
}

/**
 * @brief Insert entropy (16 bytes) into pool32.
 * 
 * Entropy might comes from external source (ex),
 * hardware (hw), operating system interfaces (os)
 * or from built-in random modules.
 * 
 * Everything is xored together before insertion.
 * 
 * @param pool pool32_t* instance.
 */
void pool32_insert_entropy(pool32_t* pool) {

    uint32_t ex, hw, os, r;
    uint32_t array[4];
    uint8_t input[16];
    uint32_t res;

    get_external_uint32(&ex);
    get_hardware_uint32(&hw);
    get_urandom_uint32(&os);
    get_ns_uint32(&r);

    // Encrypt the array using AES with random key.
    array[0] = ex; array[2] = hw;
    array[1] = os; array[3] =  r;

    aes128_random_encrypt(array, input);
    memcpy(&res, input, 16);

    res = ((ex ^ hw) & 0xffffffff) ^ ((os ^ r) & 0xffffffff);
#ifdef FORCE_32_BIT
    res = 0x7fffffff - (res & 0xfffffff) + 0xfffffff;
#endif

    pool32_insert(pool, res);
}

/**
 * @brief Apply ChaCha20 in the entirely pool.
 * 
 * This function will split the pool into 16 bytes blocks
 * and apply ChaCha with 20 rounds on each block and reassign back.
 * 
 * @param pool pool32_t* instance.
 */
void pool32_chacha(pool32_t* pool) {
    
    uint32_t blocks[POOL32_CHACHA_BLOCK][16];
    int i, j;
    int c = 0;

    // copy the entirely pool to 16-byte blocks.
    for (i = 0; i < POOL32_CHACHA_BLOCK; i++) {
        for (j = 0; j < 16; j++) {
            blocks[i][j] = pool->pool[c++];
        }
    }
    
    // perform a chacha with 20 rounds on each 16 bytes block.
    for (i = 0; i < POOL32_CHACHA_BLOCK; i++) {
        chacha20_block(blocks[i]);
    }

    // copy the blocks back to pool.
    c = 0;
    for (i = 0; i < POOL32_CHACHA_BLOCK; i++) {
        for (j = 0; j < 16; j++) {
            pool->pool[c++] = blocks[i][j];
        }
    }
}

/* Mersenne Twister (MT19937) pool implementation.
 * This pool is implemented on a 2x blocks of 624*4 bytes.
 */
void pool32mt_init(pool32mt_t* pool32mt) {
    pool32mt->count = 0;
    pool32mt->cursor = 0;
    pool32mt->iteration = 0;
    pool32mt->main_block = POOL32MT_BLOCK_A;
    pool32mt->lock.state = 0;
    pool32mt->active_threads = 0;

    for (unsigned int i = 0; i < POOL32MT_SIZE; i++) {
        pool32mt->pool[i] = 0;
    }

    //pthread_spin_init(&pool32mt->spinlock, 0);
}

/**
 * @brief Lock the Mersenne Twister pool block.
 * 
 * @param pool pool32mt_t* instance.
 * @param block Pool block to be locked. [POOL32MT_BLOCK_A, POOL32MT_BLOCK_B]
 */
void pool32mt_set_lock(pool32mt_t* pool, uint32_t block) {

    if (block == POOL32MT_BLOCK_A) {
        pool->lock.state = -1;
#ifndef DO_NOT_SWITCH_BLOCK
        pool->main_block = POOL32MT_BLOCK_B;
#endif
    }
    else if (block == POOL32MT_BLOCK_B) {
        pool->lock.state = 1;
#ifndef DO_NOT_SWITCH_BLOCK
        pool->main_block = POOL32MT_BLOCK_A;
#endif
    }
}

/**
 * @brief Automatically lock the pool and switch the main pool used in mt.
 * Return the current main block after lock.
 * 
 * @param pool pool32mt_t* instance.
 * @return uint32_t Main block after the lock.
 */
uint32_t pool32mt_auto_lock(pool32mt_t* pool) {

    /* The lock must be unlocked before acquire a new one. */
    if (pool->lock.state != 0)
        return -1;

    if (pool->main_block == POOL32MT_BLOCK_A) {
        pool32mt_set_lock(pool, POOL32MT_BLOCK_A);
        return POOL32MT_BLOCK_B;
    } else if (pool->main_block == POOL32MT_BLOCK_B) {
        pool32mt_set_lock(pool, POOL32MT_BLOCK_B);
        return POOL32MT_BLOCK_A;
    }
    return -1;
}

/**
 * @brief Automatically unlock the pool but doesn't switch the main pool.
 * 
 * @param pool pool32mt_t* instance.
 */
void pool32mt_auto_unlock(pool32mt_t* pool) {
    
    if (pool->lock.state == 0)
        return;
    
    pool->lock.state = 0;
}

/**
 * @brief Unlock the Mersenne Twister pool block.
 * This will automatically reset the main block of mersenne twister to be
 * the POOL32MT_BLOCK_A.
 * 
 * @param pool pool32mt_t* instance.
 */
void pool32mt_unlock(pool32mt_t* pool) {
    pool->lock.state = 0;
#ifndef DO_NOT_SWITCH_BLOCK
    pool->main_block = POOL32MT_BLOCK_A;
#endif
}

/**\
 * @brief This function will increase the iteration counter of pool.
 * 
 * Everytime that the pool is "touched" in we must credit an iteration to it.
 * Don't use pool->iteration++ directly, use this instead.
 * 
 * @param pool pool32mt_t* instance.
 */
void pool32mt_touch(pool32mt_t* pool) {

    pool->iteration++;

    /* Check if iteration has trapped then apply ChaCha */
    if (pool->iteration == POOL32MT_MAXIT) {

#ifndef _TEST_THREAD_
        /* DO NOT USE THE THREAD FOR NOW */
        int i = 10;
        while (i--) {
            uint32_t main_block = pool32mt_auto_lock(pool);
            //pool32mt_diffuse_block(pool, main_block, 5);
            pool32mt_diffuse(pool, 1);
            pool32mt_block_swap(pool, main_block);
            pool32mt_swap(pool);
            pool32mt_aes(pool, POOL_DONT_SWAP);
            pool32mt_auto_unlock(pool);
        };
        pool->iteration = 0;

        /* always feed back the pool when iteration occurs */
        pool32mt_insert_entropy(pool);
#endif
        /* Threads are falling into SEGFAULT (general protection fault) */
#ifdef _TEST_THREAD_
        pthread_t thread;

        struct pool32mt_touch_routine_args args;
        args.pool = pool;
        args.rounds = 1;

        // struct pool32mt_touch_routine_args *args = (struct pool32mt_touch_routine_args*) malloc(sizeof(struct pool32mt_touch_routine_args));
        // args->pool = pool;
        // args->rounds = 1;

        // printf("Rounds before: %d\n", args.rounds);

        if (pool->active_threads < 16) {
            if (pthread_create(&thread, NULL, &__pool32mt_touch_routine, &args)) {
                perror("Cannot create the touch routine thread.\n");
                return;
            }
        } else {
            printf("Skipping thread creation. %d\n", pool->active_threads);
        }

        /* Is this really joinable? */
        // if (pthread_join(thread, NULL)) {
        //     perror("Cannot join the thread.\n");
        //     return;
        // }

        // pthread_detach(thread);
        pool->iteration = 0;

        /* always fed back the pool when iteration occurs */
        pool32mt_insert_entropy(pool);
#endif
    }
}
/**
 * @brief Thread function started by `pool32mt_touch`.
 * 
 * This function will diffuse the entirely block of pool32mt.
 * This process could take a while and shoud be run on a detachable
 * thread.
 * 
 * @param _arguments pool32mt_touch_routine_args.
 * @return void* (NULL).
 */
void *__pool32mt_touch_routine(void* _arguments) {

    struct pool32mt_touch_routine_args *args = (struct pool32mt_touch_routine_args*)_arguments;
    pool32mt_t* pool = (pool32mt_t*)&args->pool;

    int rounds = args->rounds;
    /*
     * For production release, please use pthread_spin_lock, instead of
     * pthread_spin_trylock.
     *
     * Please, refer to: pthread_spin_lock(3) — Linux manual page for details. 
     */

    // printf("Your rounds are: %d\n", ((struct pool32mt_touch_routine_args *)_arguments)->rounds);
    pool->active_threads++;    

    if (pthread_spin_lock(&pool->spinlock) == EBUSY) {
        return NULL;
    }
    // raise(SIGSEGV);
    

    do {
        uint32_t main_block = pool32mt_auto_lock(pool);
        for (int j = 0; j < 128; j++) {
            pool32mt_diffuse_block(pool, main_block, 5);

            pool32mt_diffuse(pool, 1);
            pool32mt_block_swap(pool, main_block);
            pool32mt_swap(pool);
            
            pool32mt_auto_unlock(pool);
        }
        pool32mt_auto_unlock(pool);
    } while (rounds--);

    /* we really need apply chacha here? */
    // pool32mt_chacha(pool, POOL_SWAP);
    pool32mt_aes(pool, POOL_SWAP);

    pool->active_threads--;

    pthread_spin_unlock(&pool->spinlock);
    return NULL;
}

/**
 * @brief Insert a value into the secondary block of pool.
 *
 * @param pool pool32mt_t* instance.
 * @param value (uint32_t) input.
 */
void pool32mt_insert(pool32mt_t* pool, uint32_t value) {

    uint16_t cur;

    if (pool->main_block == POOL32MT_BLOCK_A)
        cur = (pool->cursor + 1) % POOL32MT_BLOCK_B;
    else
        cur = (pool->cursor + 1) % POOL32_SIZE;

    pool32mt_touch(pool);

    pool->pool[cur] = value;
    pool->cursor = cur;
}

/**
 * @brief Shift all elements of pool to the right by 
 * 'offset' positions.
 * 
 * @param pool (poolt32mt_t*) instance.
 * @param offset (uint32_t) shift value.
 */
void pool32mt_shift_right(pool32mt_t* pool, const uint32_t offset) {

    for (uint32_t r = 0; r < offset % POOL32MT_SIZE; r++) {
        for (uint32_t i = POOL32MT_SIZE; i > 0; i--) {
            pool->pool[i] = pool->pool[i-1];
            pool->pool[i-1] = 0;
        }
    }
}

/**
 * @brief Shift all elements of pool to the left by 
 * 'offset' positions.
 * 
 * @param pool (poolt32mt_t*) instance.
 * @param offset (uint32_t) shift value.
 */
void pool32mt_shift_left(pool32mt_t* pool, const uint32_t offset) {

    for (uint32_t r = 0; r < offset % POOL32MT_SIZE; r++) {
        for (uint32_t i = 0; i < POOL32MT_SIZE; i++) {
            if (i < POOL32MT_SIZE-1)
                    pool->pool[i] = pool->pool[i+1];
            else
                pool->pool[i] = 0;
        }
    }
}

/**
 * @brief Rotate all elements of pool to the left by 'offset'
 * positions.
 * 
 * @param pool (pool32mt_t*) instance. 
 * @param offset (uint32_t) shift value.
 */
void pool32mt_rotate_left(pool32mt_t* pool, const uint32_t offset) {
 
    uint32_t temp[POOL32MT_SIZE] = {0};

    for (uint32_t r = 0; r < offset % POOL32MT_SIZE; r++) {
        for (uint32_t i = 0; i < POOL32MT_SIZE; i++) {
            temp[i] = pool->pool[(i+1) % POOL32MT_SIZE];
        }

        // Realloc the pool.
        for (uint32_t i = 0; i < POOL32MT_SIZE; i++)
            pool->pool[i] = temp[i];
    }

}

/**
 * @brief Rotate all elements of pool to the right by 'offset'
 * positions.
 * 
 * @param pool (pool32mt_t*) instance. 
 * @param offset (uint32_t) shift value.
 */
void pool32mt_rotate_right(pool32mt_t* pool, const uint32_t offset) {

    uint32_t temp[POOL32MT_SIZE] = {0};

    for (uint32_t r = 0; r < offset; r++) {
        for (uint32_t i = POOL32MT_SIZE-1; i > 0; i--) {
            temp[i] = pool->pool[i-1];
        }
        temp[0] = pool->pool[POOL32MT_SIZE-1];

        for (uint32_t i = 0; i < POOL32MT_SIZE; i++)
            pool->pool[i] = temp[i];
    }
}

/**
 * @brief pool32mt - Add entropy into the pools.
 * This function is describe as add_entropy() on RoyalRand
 * Specification (white paper).  
 * 
 * @param pool (pool32mt_t*) instance.
 * @param source (const void*) pointer to buffer of entropy. 
 * @param size (size_t) size of buffer.
 */
void pool32mt_add_entropy(pool32mt_t* pool, const void* source, size_t size) {
    
    uint32_t input;
    uint8_t *buffer;
    uint8_t output[BLAKE3_OUT_LEN];

    if (size > API_INPUT_MAX_SIZE) {
        buffer = malloc(API_INPUT_MAX_SIZE * sizeof(uint8_t));
        if (buffer == NULL) {
            perror("pool32mt_add_entropy(): Can't allocate memory!");
            return;
        }
        memcpy(buffer, source, API_INPUT_MAX_SIZE * sizeof(uint8_t));
    } else {
        if (size < 1) {
            perror("pool32mt_add_entropy(): Bad size for buffer.");
            return;
        }
        buffer = malloc(size * sizeof(uint8_t));
        if (buffer == NULL) {
            perror("pool32mt_add_entropy(): Can't allocate memory!");
            return;
        }
        memcpy(buffer, source, size * sizeof(uint8_t));
    }

    aes128_ctr_random(buffer, buffer, size * sizeof(uint8_t));
 
    blake3_hasher hasher;
    blake3_hasher_init(&hasher);
    blake3_hasher_update(&hasher, buffer, size * sizeof(uint8_t));
    blake3_hasher_finalize(&hasher, output, BLAKE3_OUT_LEN);

    for (size_t i = 0; i < BLAKE3_OUT_LEN; i += sizeof(input)) {
        memcpy(&input, &output[i], sizeof(input));
        pool32mt_insert(pool, input);
    }

    free(buffer);
}

/**
 * @brief Insert random entropy into the pool32mt.
 * 
 * Entropy is extracted from ex, hw, os and r. Everything is encrypted (AES 128)
 * before inserting into the pool.
 *  
 * @param pool pool32mt_t* instance.
 */
void pool32mt_insert_entropy(pool32mt_t* pool) {
    
    uint32_t ex, hw, os, r;
    uint32_t res;
    uint32_t array[4];
    uint8_t input[16];

    get_external_uint32(&ex);
    get_hardware_uint32(&hw);
    get_urandom_uint32(&os);
    get_ns_uint32(&r);

    // Encrypt the array using AES with random key.
    array[0] = ex; array[2] = hw;
    array[1] = os; array[3] = r;

    aes128_random_encrypt(array, input);
    memcpy(&res, input, 16);

    res = ((ex ^ hw) & 0xffffffff) ^ ((os ^ r) & 0xffffffff);
#ifdef FORCE_32_BIT
    res = 0x7fffffff - (res & 0xfffffff) + 0xfffffff;
#endif
    pool32mt_insert(pool, res);
}

/**
 * @brief Mersenne Twister Pool - ChaCha block calculation.
 * This function will split the first block of the pool to various blocks of 16
 * elements and will apply ChaCha20 to all sub-blocks and reassign back.
 * 
 * If `swap` is 1 we will swap the first block to the other.
 * 
 * @param pool pool32mt_t* instance.
 * @param swap swap or not. [POOL_SWAP, POOL_DONT_SWAP]
 */
void pool32mt_chacha(pool32mt_t* pool, int swap) {

    uint32_t blocks[(POOL32MT_SIZE/2)/16][16];
    unsigned int i, j, c;

    // copy (POOLMT32_SIZE/2) bytes to ((POOLMT32_SIZE/2)/16) blocks of 16 bytes.
    c = 0;
    for (i = 0; i < (POOL32MT_SIZE/2)/16; i++) {
        for (j = 0; j < 16; j++) {
            blocks[i][j] = pool->pool[c++];
        }
    }

    // apply ChaCha20 to all 16 bytes blocks.
    for (i = 0; i < (POOL32MT_SIZE/2)/16; i++) {
        chacha20_block(blocks[i]);
    }

    // reassign the result to the pool.
    c = 0;
    for (i = 0; i < (POOL32MT_SIZE/2)/16; i++) {
        for (j = 0; j < 16; j++) {
            pool->pool[c++] = blocks[i][j];
        }
    }

    // swap the first (POOLMT32/2) bytes to the other if needed.
    if (swap == 1) {
        pool32mt_swap(pool);
    }
}
/**
 * @brief Randomly encrypt `size` bytes of the buffer pointed by `in`
 * using AES-128 in CTR mode of cipher. The key are choosen by randomly 
 * extracting 128 bit of /dev/urandom. The same process is applied
 * to nonce (96 bits). The output is written in the buffer pointed by `out`.
 * 
 * @param in (const void*) pointer of input buffer. 
 * @param out (void*) pointer to output buffer.
 * @param size (size_t) size in bytes of buffer.
 */
void aes128_ctr_random(const void* in, void* out, size_t size) {

    AES_KEY key;
    uint32_t random_key[4];  // 128-bit key.
    uint32_t random_nonce[3];  // 96-bit nonce. 

    for (size_t i = 0; i < 4; i++) {
        get_urandom_uint32(&random_key[i]);
        if (i < 3)
            get_urandom_uint32(&random_nonce[i]);
    }

    AES_set_encryption_key((const uint8_t*)random_key, 128, &key);
    AES_ctr(in, out, size, (uint8_t*)random_nonce, &key);
}
/**
 * @brief Randomly encrypt 16 bytes pointed by "in" and writes the
 * output to "out" using a 128-bit key randomly extracted from OS.
 * 
 * @param in (void*) input buffer.
 * @param out (void*) output buffer.
 */
void aes128_random_encrypt(const void* in, void* out) {

    AES_KEY key;
    uint32_t random_key[4]; // 128-bit key
    uint32_t temp, xor;

    /* The 16 bytes array are filled with 4 32-bit integers XORed with a random
       value extracted from the hardware rng. */
    get_hardware_uint32(&xor);
    for (unsigned int i = 0; i < 4; i++) {
        get_urandom_uint32(&temp);
        random_key[i] = temp ^ xor;
    }

    AES_set_encryption_key((uint8_t*)random_key, 128, &key);
    AES_encrypt(in, out, &key);
}
/**
 * @brief Mersenne Twister Pool - AES cipher.
 * 
 * This function will split the pool into (POOL32MT_SIZE/16) blocks of 16
 * bytes and apply the AES cipher.
 * 
 * We will use a 128-bit key extracted from the pool.
 * 
 * The extraction of the key will happen in sequence starting 
 * on the cursor current position.
 * 
 * @param pool pool32mt_t* instance.
 * @param swap swap or not. [POOL_SWAP, POOL_DONT_SWAP]
 */
void pool32mt_aes(pool32mt_t* pool, int swap) {

    AES_KEY key;
    uint8_t random_key[16];
    uint8_t blocks[(POOL32MT_SIZE * sizeof(uint32_t))/16][16];
    uint8_t output[16];
    unsigned int i, c;

    /*reallocate cursor position before grabbing the entropy.
      This is necessary in the case when the cursor points to
      the very end of the array. So we must reallocate at least
      16 bytes (backwards) before copying that to the random_key.*/
    uint16_t cp = pool->cursor;
    if (cp + (16/sizeof(uint32_t)) > POOL32MT_SIZE)
        cp -= (16/sizeof(uint32_t));

    memcpy(random_key, &pool->pool[cp], (16/sizeof(uint32_t)));
    /* AES key setup: here we will use a 128-bit key. */
    AES_set_encryption_key(random_key, 128, &key);

    c = 0;
    for (i = 0; i < (POOL32MT_SIZE * sizeof(uint32_t)/16); i++) {
        memcpy(&blocks[i], &pool->pool[c], 16);
        c += (16/sizeof(uint32_t));
    }

    for (i = 0; i < (POOL32MT_SIZE * sizeof(uint32_t)/16); i++) {
        AES_encrypt(blocks[i], output, &key);
        memcpy(blocks[i], output, 16);
    }

    c = 0;
    for (i = 0; i < (POOL32MT_SIZE * sizeof(uint32_t)/16); i++) {
        memcpy(&pool->pool[c], &blocks[i], 16);
        c += (16/sizeof(uint32_t));
    }

    if (swap == POOL_SWAP)
        pool32mt_swap(pool);
}

/**
 * @brief Swap the first BLOCK of pool to the other.
 * 
 * @param pool pool32mt_t* instance.
 */
void pool32mt_swap(pool32mt_t* pool) {

    uint32_t block_a[POOL32MT_BLOCK_SIZE];
    uint32_t block_b[POOL32MT_BLOCK_SIZE];

    memcpy(block_a, &pool->pool[POOL32MT_BLOCK_A], POOL32MT_BLOCK_SIZE * sizeof(uint32_t));
    memcpy(block_b, &pool->pool[POOL32MT_BLOCK_B], POOL32MT_BLOCK_SIZE * sizeof(uint32_t));
    
    memcpy(&pool->pool[POOL32MT_BLOCK_A], block_b, POOL32MT_BLOCK_SIZE * sizeof(uint32_t));
    memcpy(&pool->pool[POOL32MT_BLOCK_B], block_a, POOL32MT_BLOCK_SIZE * sizeof(uint32_t));
}

/**
 * @brief Fill the entirely pool with the specified value.
 * 
 * @param pool pool32mt_t* instance.
 * @param value (uint32_t) value.
 */
void pool32mt_fill(pool32mt_t* pool, uint32_t value) {

    for (unsigned int i = 0; i < POOL32MT_SIZE; i++) {
        pool->pool[i] = (uint32_t)value;
    }
}

/**
 * @brief Fill a partial block of pool with the specified value.
 * This could be used as a reseting mechanism for the pool.
 * 
 * @param pool pool32mt_t* instance.
 * @param block Block to be filled. [POOL32MT_BLOCK_A, POOL32MT_BLOCK_B]
 * @param value (uint32_t) Value.
 */
void pool32mt_fill_block(pool32mt_t* pool, unsigned int block, uint32_t value) {

    for (unsigned int i = block; i < POOL32MT_SIZE; i++) {
        pool->pool[i] = (uint32_t)value;
    }
}

/**
 * @brief Perform a XOR operation on each element (uint32_t) of pool
 * with `xor` value.
 * 
 * @param pool pool32mt_t* instance.
 * @param xor (uint32_t) Xor value.
 */
void pool32mt_xor(pool32mt_t* pool, uint32_t xor) {
    
    for (unsigned int i = 0; i < POOL32MT_SIZE; i++) {
        pool->pool[i] ^= (uint32_t)xor;
    }
}

/**
 * @brief This function will permute the specified block of the pool32mt
 * using a special permutation table.
 * 
 * @param pool pool32mt_t* instance.
 * @param block block to be permuted. [POOL32MT_BLOCK_A, POOL32MT_BLOCK_B]
 * @param nrounds how many rounds.
 */
void pool32mt_block_permute(pool32mt_t* pool, unsigned int block, unsigned int nrounds) {
    
    uint32_t temp_block[POOL32MT_BLOCK_SIZE];

    memcpy(temp_block, &pool->pool[block], POOL32MT_BLOCK_SIZE * sizeof(uint32_t));
    _pool32mt_block_permute(temp_block, nrounds);
    memcpy(&pool->pool[block], temp_block, POOL32MT_BLOCK_SIZE * sizeof(uint32_t));

}

/**
 * @brief This function will swap all uint32_t elements of the block
 * using the special swap table.
 * 
 * @param pool pool32mt_t* instance.
 * @param block block to be swapped. [POOL32MT_BLOCK_A, POOL32MT_BLOCK_B]
 */
void pool32mt_block_swap(pool32mt_t* pool, unsigned int block) {

    uint32_t temp_block[POOL32MT_BLOCK_SIZE];

    memcpy(temp_block, &pool->pool[block], POOL32MT_BLOCK_SIZE * sizeof(uint32_t));
    _pool32mt_block_swap(temp_block);
    memcpy(&pool->pool[block], temp_block, POOL32MT_BLOCK_SIZE * sizeof(uint32_t));
}

/**
 * @brief Mersenne Twister Pool Diffuser.
 *        This function will shuffle both blocks of pool32mt.
 * 
 * @param pool pool32mt_t* instance.
 * @param nrounds How many rounds.
 */
void pool32mt_diffuse(pool32mt_t* pool, unsigned int nrounds) {

    unsigned int i;
    uint32_t xor_value;

    for (i = 0; i < nrounds; i++) {
        /* Diffuse block A */
        pool32mt_block_permute(pool, POOL32MT_BLOCK_A, 20);
        pool32mt_block_swap(pool, POOL32MT_BLOCK_A);

        /* Diffuse block B */
        pool32mt_block_permute(pool, POOL32MT_BLOCK_B, 20);
        pool32mt_block_swap(pool, POOL32MT_BLOCK_B);

        /* Swap block */
        pool32mt_swap(pool);

        /* Xor each element with the first element */
        xor_value = pool->pool[POOL32MT_BLOCK_A] ^ pool->pool[POOL32MT_BLOCK_B];
        pool32mt_xor(pool, xor_value);
    }

    /* Finally apply the ChaCha20. */
    pool32mt_chacha(pool, POOL_DONT_SWAP);
}

/**
 * @brief Mersenne Twister Pool Diffuser.
 *        This function will shuffle a specific block of the pool.
 * 
 * @param pool pool32mt_t* instance.
 * @param block Block to be shuffled. [POOL32MT_BLOCK_A, POOL32MT_BLOCK_B]
 * @param nrounds how many rounds.
 */
void pool32mt_diffuse_block(pool32mt_t* pool, unsigned int block, unsigned int nrounds) {
    
    unsigned int i;
    uint32_t xor_value;

    for (i = 0; i < nrounds; i++) {
        pool32mt_block_permute(pool, block, 1);
        pool32mt_block_swap(pool, block);
        xor_value = mersenne_temper(pool->pool[block] ^ pool->pool[block + POOL32MT_BLOCK_SIZE]);
        
        for (uint32_t n = block; n < block + POOL32MT_BLOCK_SIZE; n++) {
            pool->pool[n] ^= xor_value;
        }
    }
    pool32mt_block_permute(pool, block, 12);
}
