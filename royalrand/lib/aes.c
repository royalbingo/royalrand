/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo@bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <string.h>

#include "include/rijndael-alg-fst.h"
#include "include/aes.h"

/**
 * @brief Expand the cipher key into the encryption key schedule.
 * 
 * @param keychar (const uint8_t*) Key.
 * @param bits Key size in bits. (128/192/256)
 * @param key (AES_KEY*) AES key.
 * @return (int) non-zero on failure.
 */
int AES_set_encryption_key(const uint8_t *keychar, const int bits, AES_KEY* key) {
    
    key->rounds = rijndaelKeySetupEnc(key->key, keychar, bits);
    if (key->rounds == 0)
        return -1;
    return 0;
}

/**
 * @brief Expand the cipher key into the decryption key schedule.
 * 
 * @param keychar (const uint8_t*) Key.
 * @param bits Key size in bits. (128/192/256)
 * @param key (AES_KEY*) AES key.
 * @return (int) non-zero on failure.
 */
int AES_set_decryption_key(const uint8_t *keychar, const int bits, AES_KEY* key) {

    key->rounds = rijndaelKeySetupDec(key->key, keychar, bits);
    if (key->rounds == 0)
        return -1;
    return 0;
}

/**
 * @brief AES encryption routine.
 * 
 * @param in (const uint8_t*) input buffer.
 * @param out (uint8_t*) output buffer.
 * @param key (const AES_KEY*) AES key.
 */
void AES_encrypt(const uint8_t* in, uint8_t* out, const AES_KEY *key) {

    rijndaelEncrypt(key->key, key->rounds, in, out);
}

/**
 * @brief AES decryption routine.
 * 
 * @param in (const uint8_t*) input buffer.
 * @param out (uint8_t*) output buffer.
 * @param key (const AES_KEY*) AES key.
 */
void AES_decrypt(const uint8_t* in, uint8_t* out, const AES_KEY *key) {

    rijndaelDecrypt(key->key, key->rounds, in, out);
}

/**
 * @brief Apply AES in CTR mode (stream cipher).
 * 
 * @param in (const uint8_t*) input buffer.
 * @param out (uint8_t*) output buffer.
 * @param nonce (uint8_t*) nonce buffer. (96 bits)
 * @param key (AES_KEY*) pointer to the initialized AES key.
 */
void AES_ctr(const uint8_t* in, uint8_t* out, size_t size,
             const uint8_t* nonce, const AES_KEY* key) {

    /*
    This implementation defines that counter has 32 bit long
    and the nonce has the 96 bits remaining.
    */

    uint32_t counter = 0; // 32 bits long
    uint8_t iv[12] = {0}; // 96 bits nonce
    uint8_t data[AES_BLOCK_SIZE] = {0};  // iv || counter
    uint8_t ciphertext[AES_BLOCK_SIZE] = {0};  // rjindael output.
    size_t length;  // total stream cipher processed.

    memcpy(iv, nonce, 12);
    memcpy(&data[0], iv, 12);
    memcpy(&data[13], &counter, 4);

    while (size > 0) {

        AES_encrypt(data, ciphertext, key);
        length = (size < AES_BLOCK_SIZE) ? size : AES_BLOCK_SIZE;
        
        for (size_t i = 0; i < length; i++)
            out[i] = in[i] ^ ciphertext[i];
        
        out += length;
        in += length;
        size -= length;
        counter++;
    }
}

/**
 * @brief Apply AES in CBC mode of operation.
 * 
 * @param in (const uint8_t*) input buffer.
 * @param out (uint8_t*) output buffer.
 * @param size (size_t) buffer size.
 * @param key (const AES_KEY* key) AES key.
 * @param iv (uint8_t*) initialization vector.
 * @param forward_encrypt 
 */
void AES_cbc_encrypt(const uint8_t* in, uint8_t* out, unsigned long size, 
                     const AES_KEY* key, uint8_t* iv, int forward_encrypt) {

    uint8_t temp[AES_BLOCK_SIZE];
    long unsigned int i;

    if (forward_encrypt) {
        while (size >= AES_BLOCK_SIZE) {
            
            for (i = 0; i < AES_BLOCK_SIZE; i++)
                temp[i] = in[i] ^ iv[i];
            
            AES_encrypt(temp, out, key);
            memcpy(iv, out, AES_BLOCK_SIZE);

            size -= AES_BLOCK_SIZE;
            in += AES_BLOCK_SIZE;
            out += AES_BLOCK_SIZE;
        }
        if (size) {
            for (i = 0; i < size; i++) {
                temp[i] = in[i] ^ iv[i];
            }

            for (i = 0; i < AES_BLOCK_SIZE; i++) {
                temp[i] = iv[i];
            }
            AES_encrypt(temp, out, key);
            memcpy(iv, out, AES_BLOCK_SIZE);
        }
    } else {
        while (size >= AES_BLOCK_SIZE) {

            memcpy(temp, in, AES_BLOCK_SIZE);
            AES_decrypt(temp, out, key);

            for (i = 0; i < AES_BLOCK_SIZE; i++) {
                out[i] ^= iv[i];
            }

            memcpy(iv, temp, AES_BLOCK_SIZE);

            size -= AES_BLOCK_SIZE;
            in += AES_BLOCK_SIZE;
            out += AES_BLOCK_SIZE;
        }
        if (size) {
            memcpy(temp, in, AES_BLOCK_SIZE);
            AES_decrypt(temp, out, key);

            for (i = 0; i < size; i++) {
                out[i] ^= iv[i];
            }
            memcpy(iv, temp, AES_BLOCK_SIZE);
        }
    }
}
