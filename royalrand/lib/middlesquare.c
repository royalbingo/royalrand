/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo@bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>

#include "middlesquare.h"


ms_ctx* middle_square(uint64_t seed) {

    ms_ctx* context = malloc(sizeof(ms_ctx));
    
    if (context == NULL)  {
        perror("middle_square(): Cannot allocate memory!\n");
        return NULL;
    }
    
    context->seed = seed;
    context->digits = floor(log10(seed) + 1);

    return context;
}


uint64_t ms_step(ms_ctx* context) {
    
    uint64_t n = context->seed * context->seed;
    size_t size = floor(log10(n) + 1) * sizeof(uint8_t);

    char *buffer = malloc(context->digits * 2);
    char *temp = malloc(size);

    if (buffer == NULL || temp == NULL) {
        perror("ms_step(): Cannot allocate memory!\n");
        exit(1);
    }
    

    memset(buffer, 0x30, context->digits * 2);
    sprintf(temp, "%ld", n);

    /* Copy temporary buffer to end of `buffer`. */
    int j = (context->digits * 2) - size;
    for (size_t i = 0; i < size; i++, j++) {
        buffer[j] = temp[i];
    }

    memset(temp, 0, size);
    uint32_t start = floor(context->digits / 2);
    j = 0;

    for (uint32_t i = start; i< start + context->digits; i++, j++) {
        temp[j] = buffer[i];
    }

    context->seed = atoll(temp);

    free(buffer);
    free(temp);

    return context->seed;
}


float ms_float_step(ms_ctx* context) {
    return (float)ms_step(context) / (float)(pow(10, context->digits) - 1);
}


void ms_free(ms_ctx* context) {
    free(context);
}
