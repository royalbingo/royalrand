/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo@bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>

long int unbit_shift_right_xor(long int value , long int shift) {
    
    long int i = 0;
    long int result = 0;
    long long int part_mask;
    long int part;

    while (i * shift < 32) {
        part_mask = (0U << (32 - shift)) >> (shift * i);
        part = value & part_mask;
        value ^= part >> shift;
        result |= part;
        i++;
    }

    return result;
}

long int unbit_shift_left_xor(long int value, long int shift, long int mask) {

    long int i = 0;
    long long int result = 0;
    long int part_mask;
    long int part;

    while (i * shift < 32) {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wshift-negative-value"
        part_mask = (-1 >> (32 - shift)) << (shift * i);
#pragma GCC diagnostic pop
        part = value & part_mask;
        value ^= (part << shift) & mask;
        result |= part;
        i++;
    }
    return result;
}

long int reverse(long int output) {
    long int value = output;
    value = unbit_shift_right_xor(value, 18);
    value = unbit_shift_left_xor(value, 15, 0xefc60000);
    value = unbit_shift_left_xor(value, 7, 0x9d2c5680);
    value = unbit_shift_right_xor(value, 11);
    return value;
}
