/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo@bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>

/**
 * @brief Convert an byte-array to uint32_t. (little-endian)
 * 
 * @param bytes pointer to byte-array.
 * @return uint32_t 
 */
uint32_t uint32_from_bytearray(uint8_t *bytes) {
    return bytes[0] + (bytes[1] << 8) + (bytes[2] << 16) + (bytes[3] << 24);
}

/**
 * @brief Convert an byte-array to uint32_t. (big-endian)
 * 
 * @param bytes pointer to byte-array.
 * @return uint32_t 
 */
uint32_t uint32_big_from_bytearray(uint8_t *bytes) {
    return bytes[3] + (bytes[2] << 8) + (bytes[1] << 16) + (bytes[0] << 24);
}

uint32_t uint32_mod_reduction(uint32_t value, uint32_t min, uint32_t max) {
    return (value % (max - min + 1)) + min;
}

uint32_t uint32_fast_reduction(uint32_t value, uint32_t min, uint32_t max) {
    return (((uint64_t)value * ((uint64_t)max - min + 1) >> 32) + min);
}
