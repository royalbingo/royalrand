/**
 * @file mt19937c.c
 * @author Murilo Augusto <murilo@bad1337.com>
 * @brief Customization of MT19937 to work with custom pools.
 * @version 0.1
 * @date 2022-05-16
 * 
 * @copyright Copyright (c) 2022 Murilo Augusto
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 */

/* 
   A C-program for MT19937, with initialization improved 2002/1/26.
   Coded by Takuji Nishimura and Makoto Matsumoto.

   Before using, initialize the state by using mt_init_genrand(seed)  
   or mt_init_by_array(init_key, key_length).

   Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
   All rights reserved.                          

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

     1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

     2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

     3. The names of its contributors may not be used to endorse or promote 
        products derived from this software without specific prior written 
        permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


   Any feedback is very welcome.
   http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html
   email: m-mat @ math.sci.hiroshima-u.ac.jp (remove space)
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "include/mt19937c.h"
#include "include/crypt.h"
#include "include/pool.h"
#include "include/lfsr.h"

#ifdef __DEBUG__
#include "include/debug.h"
#endif

/*
 Pool Interfaces
*/

/**
 * @brief pool32mt getter function. This will translate the desired index position
 * to the correct one, based on POOL_BLOCK and cursor position. This will also
 * fed back the pool with the first 32-bit of LFSR (with initial state being the
 * extracted value). 
 * 
 * @param self (mt19937c_ctx*) Custom Mersenne Twister instance.
 * @param index (unsigned int) desired index.
 * @return (uint32_t) extracted value. 
 */
uint32_t pool32mt_getter(mt19937c_ctx* self, unsigned int index) {

    /* This will translate the desired index, based on
     * current POOL_BLOCK and current cursor position.*/
    uint32_t position = ((index + self->pool.cursor) % POOL32MT_BLOCK_SIZE) + self->pool.main_block;
    uint32_t value = self->pool.pool[position];

    /* Feed back the pool with fast LFSR.

       Here we will not use the pool32mt_insert because this function
       will perform a touch and consequently it will increase the
       execution time in at least 3x. 

       But replacing the element directly using index/pointer don't
       fall into the touch. So the performance is not affected.
    */
    self->pool.pool[position] = value ^ lfsr_fast(value, 32);
    
    return value;
}

/**
 * @brief pool32mt setter function. This function will set an integer (uint32_t)
 * value on the specified index. This index is automatically calculated using the 
 * same mechanism of pool32mt_getter, but it also perform a `touch` on the pool.
 * 
 * @param self (mt19937c_ctx*) instance.
 * @param index (unsigned int) index.
 * @param value (uint32_t) value to be inserted.
 */
void pool32mt_setter(mt19937c_ctx* self, unsigned int index, uint32_t value) {
    uint32_t position = ((index + self->pool.cursor) % POOL32MT_BLOCK_SIZE) + self->pool.main_block;
    self->pool.pool[position] = value;
    pool32mt_touch(&self->pool);
}

/**
 * @brief Create an mt19937c context object.
 * 
 * @return mt19937c_ctx* created instance.
 */
mt19937c_ctx* mt19937c(void) {

    mt19937c_ctx* mt = malloc(sizeof(mt19937c_ctx));

    if (mt == NULL) {
        perror("mt19937c(): Cannot allocate memory.\n");
        exit(1);
    }

    pool32mt_init(&mt->pool);
    pthread_spin_init(&mt->pool.spinlock, 0);

    mt->get = &pool32mt_getter;
    mt->set = &pool32mt_setter;
    mt->mti = N + 1;

    return mt;
}

/**
 * @brief Free allocated memory for mt19937c context.
 * 
 * @param context (mt19937c_ctx*) instance.
 */
void mt19937c_free(mt19937c_ctx* context) {
    pthread_spin_destroy(&context->pool.spinlock);
    free(context);
}

/**
 * @brief Random initialize the pools of mt19937c.
 * 
 * This function only peform a pool (POOL_BLOCK_A)
 * random initialization. The MT uses the init_by_array
 * method to initialize.
 * 
 * @param context 
 */
void mtc_random_init(mt19937c_ctx* context) {

    unsigned long array[MTC_DEFAULT_RANDOM_INIT] = { 0 };
    uint32_t a, b;

    for (unsigned int i = 0; i < MTC_DEFAULT_RANDOM_INIT; i++) {
        get_urandom_uint32(&a);
        get_hardware_uint32(&b);
        // array[i] = mersenne_temper(a) ^ mersenne_temper(b);
        array[i] = a ^ b;
    }
    mtc_init_by_array(context, array, MTC_DEFAULT_RANDOM_INIT);
}

/**
 * @brief Safely initialize the Mersenne Twister pool
 * and properly seed it.
 * 
 * @param context mt19937c_ctx* instance.
 */
void mtc_safe_init(mt19937c_ctx* context) {

    uint32_t a, b;
    uint32_t seed;
    uint32_t seed_array[POOL32MT_BLOCK_SIZE];

    /**
     * Here we'll fill only the first block of the pool
     * because the diffusion will permute all seed values across
     * the entire pool.
     */
    if (get_hardware_seed(seed_array, POOL32MT_BLOCK_SIZE) != 0) {
        for (unsigned int i = 0; i < POOL32MT_BLOCK_SIZE; i++) {
            get_hardware_uint32(&a);
            get_urandom_uint32(&b);
            pool32mt_insert(&context->pool, a ^ b);
        }

    } else {
        for (unsigned int i = 0; i < POOL32MT_BLOCK_SIZE; i++) {
            get_urandom_uint32(&b);
            pool32mt_insert(&context->pool, seed_array[i] ^ b);
        }
    }

    pool32mt_diffuse(&context->pool, 1);
    
    if (get_hardware_seed(&seed, 1) != 0) {
        get_urandom_uint32(&seed);
    }

    mtc_init_genrand(context, seed ^ b);
}

/* initializes mt[N] with a seed. */
void mtc_init_genrand(mt19937c_ctx* context, unsigned long s) {

    mt19937c_ctx* self = context;

    context->set(self, 0, s & 0xffffffffUL);

    for (self->mti=1; self->mti<N; self->mti++) {
        context->set(self, context->mti, (1812433253UL * (context->get(self, context->mti-1) ^ (context->get(self, context->mti-1) >> 30)) + context->mti)); 
        context->set(self, context->mti, context->get(self, context->mti) & 0xffffffffUL);
    }
}

/* initialize by an array with array-length */
/* init_key is the array for initializing keys */
/* key_length is its length */
/* slight change for C++, 2004/2/26 */
void mtc_init_by_array(mt19937c_ctx* context, unsigned long init_key[], int key_length)
{
    int i, j, k;
    mt19937c_ctx* self = context;
    mtc_init_genrand(context, 19650218UL);
    
    i=1; j=0;
    k = (N>key_length ? N : key_length);
    for (; k; k--) {
        context->set(self, i, (context->get(self, i) ^
                    ((context->get(self, i-1) ^ (context->get(self, i-1)>>30)) * 
                    1664525UL)) + init_key[j] + j); /* non linear */

        context->set(self, i, context->get(self, i) &
                    0xffffffffUL); /* for WORDSIZE > 32 machines */

        i++; j++;
        if (i >= N) { context->set(self, 0, context->get(self, N-1)); i = 1; }
        if (j>=key_length) j=0;
    }
    for (k=N-1; k; k--) {
        context->set(self, i, (context->get(self, i) ^ 
                              ((context->get(self, i-1) >> 30)) * 1566083941UL));

         /* non linear */
        context->set(self, i, context->get(self, i) & 0xffffffffUL); /* for WORDSIZE > 32 machines */

        i++;
        if (i >= N) {
            context->set(self, 0, context->get(self, N-1));
            i = 1;
        }
    }

    context->set(self, 0, 0x80000000UL); /* MSB is 1; assuring non-zero initial array */ 
}

/**
 * @brief Generate a 32-bit (unsigned) random integer.
 * 
 * This function should never generate the value 0.
 * 
 * @param context mt19937c_ctx* instance.
 * @return unsigned long (output)
 */
unsigned long mtc_genrand_int32(mt19937c_ctx* context)
{
    unsigned long y = 0;
    static unsigned long mag01[2]={0x0UL, MATRIX_A};

    /* This loop will force a non-zero generation */
    do {
        int* mti = &context->mti;
        mt19937c_ctx* self = context;
        int kk = 0;

        /* mag01[x] = x * MATRIX_A  for x=0,1 */

        if (*mti >= N) { /* generate N words at one time */

            if (*mti == N+1)                                    /* if mt_init_genrand() has not been called, */
                mtc_init_genrand(context, MTC_DEFAULT_SEED);    /* a default initial seed is used */

            for (kk=0;kk<N-M;kk++) {
                y = (context->get(self, kk)&UPPER_MASK)|
                    (context->get(self, kk+1)&LOWER_MASK);

                context->set(self, kk, context->get(self, kk+M) ^ 
                            (y >> 1) ^ mag01[y & 0x1UL]);
            }
            for (;kk<N-1;kk++) {
                y = (context->get(self, kk)&UPPER_MASK)|
                    (context->get(self, kk+1)&LOWER_MASK);

                context->set(self, kk, context->get(self, kk+(M-N)) ^
                            (y >> 1) ^ mag01[y & 0x1UL]);
            }
            y = (context->get(self, N-1)&UPPER_MASK)|
                (context->get(self, 0)&LOWER_MASK);

            context->set(self, N-1, context->get(self, M-1) ^ 
                        (y >> 1) ^ mag01[y & 0x1UL]);

            *mti = 0;
        }

        y = context->get(self, (*mti)++);

        /* Tempering */
        y ^= (y >> 11);
        y ^= (y << 7) & 0x9d2c5680UL;
        y ^= (y << 15) & 0xefc60000UL;
        y ^= (y >> 18);
#if (MTC_NEVER_GENERATE_ZERO == 1)
    } while (y == 0);
#else
    } while (0);
#endif

    return y;
}

/* generates a random number on [0,0x7fffffff]-interval */
long mtc_genrand_int31(mt19937c_ctx* context)
{
    return (long)(mtc_genrand_int32(context)>>1);
}

/* generates a random number on [0,1]-real-interval */
double mtc_genrand_real1(mt19937c_ctx* context)
{
    return mtc_genrand_int32(context)*(1.0/4294967295.0); 
    /* divided by 2^32-1 */ 
}

/* generates a random number on [0,1)-real-interval */
double mtc_genrand_real2(mt19937c_ctx* context)
{
    return mtc_genrand_int32(context)*(1.0/4294967296.0); 
    /* divided by 2^32 */
}

/* generates a random number on (0,1)-real-interval */
double mtc_genrand_real3(mt19937c_ctx* context)
{
    return (((double)mtc_genrand_int32(context)) + 0.5)*(1.0/4294967296.0); 
    /* divided by 2^32 */
}

/* generates a random number on [0,1) with 53-bit resolution */
double mtc_genrand_res53(mt19937c_ctx* context) 
{ 
    unsigned long a = mtc_genrand_int32(context) >> 5, b = mtc_genrand_int32(context) >> 6; 
    return (a * 67108864.0 + b) * (1.0 / 9007199254740992.0); 
} 
/* These real versions are due to Isaku Wada, 2002/01/09 added */

/*
 * Calculate a uniformly distributed random number less than upper_bound
 * avoiding "modulo bias".
 *
 * Uniformity is achieved by generating new random numbers until the one
 * returned is outside the range [0, 2**32 % upper_bound).  This
 * guarantees the selected random number will be inside
 * [2**32 % upper_bound, 2**32) which maps back to [0, upper_bound)
 * after reduction modulo upper_bound.
 *
 * original source: arc4random_uniform (arc4random_uniform.c)
 */
uint32_t mtc_uniform32(mt19937c_ctx *context, uint32_t upper)
{
    uint32_t r, min;
	unsigned iterations = 0;

    if (upper < 2)
        return 0;

    /* 2**32 % x == (2**32 - x) % x */
    min = -upper % upper;

    /*
	 * This could theoretically loop forever but each retry has
	 * p > 0.5 (worst case, usually far better) of selecting a
	 * number inside the range we need, so it should rarely need
	 * to re-roll.
	 */
    for (;;)
    {
		iterations++;
        r = mtc_genrand_int32(context);
        if (r >= min)
            break;
		
		/* safety check to preventing infinite loop */
		if (iterations > 1000)
			return 0;
    }

    return r % upper;
}
