// Copyright (c) 2022 Murilo Augusto
// 
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

#ifndef _INTEGER_H
#define _INTEGER_H

uint32_t uint32_from_bytearray(uint8_t *bytes);
uint32_t uint32_big_from_bytearray(uint8_t *bytes);
uint32_t uint32_mod_reduction(uint32_t value, uint32_t min, uint32_t max);
uint32_t uint32_fast_reduction(uint32_t value, uint32_t min, uint32_t max);

#endif /* _INTEGER_H */