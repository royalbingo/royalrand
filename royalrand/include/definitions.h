/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo@bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DEF_H_
#define _DEF_H_

#include <endian.h>
#include <stdbool.h>
#include <string.h>

#include "types.h"

// #define __DEBUG__ 0
// #define _TEST_THREAD_ 1

// #define EXTERNAL
#define USE_HARDWARE_RNG
// #define FORCE_16_BIT
// #define FORCE_32_BIT 1

static inline void memzero_explicit(void *s, size_t count) {
    memset(s, 0, count);
}

#endif /*_DEF_H_  */
