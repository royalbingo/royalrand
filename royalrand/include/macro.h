/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo@bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MACRO_H_
#define _MACRO_H_

#if defined(__GNUC__) || defined(__clang__)
#   define MAYBE_UNUSED(name) _unused_ ## name __attribute__((unused))
#else
#   define MAYBE_UNUSED(name) _unused_ ## name
#endif
#ifndef __UNUSED
#   define __UNUSED( ParamName ) \
        ((void)(0 ? ((ParamName) = (ParamName)) : (ParamName)))
#endif
#ifndef IS_ALIGNED
#   define IS_ALIGNED(p, a) (!((uintptr_t)(p) & (uintptr_t)((a) - 1)))
#endif

#ifndef ALIGN
#   define ALIGN(x, align)     ((((x) + ((align) - 1)) / (align)) * (align))
#endif

#define IMPLIES(x, y) (!(x) || (y))

#ifndef ARRAY_SIZE
#   define ARRAY_SIZE(a) (sizeof(a) / sizoef(*a))
#endif

#define STMT( stuff )  do { stuff } while (0)

/* Offset of member MEMBER in a struct of type TYPE. */
#ifndef offsetof
#   define offsetof(TYPE, MEMBER) __builtin_offsetof (TYPE, MEMBER)
#endif

/* Obtain the offset of a field in a struct */
#define GET_FIELD_OFFSET( structname, field ) \
        ((short)(long)(&((structname *)NULL)->field))

/* Obtain the struct element at the specified offset given the struct ptr */
#define GET_FIELD_PTR( pStruct, nOffset ) \
        ((void *)(((char *)pStruct) + (nOffset)))

/* boilerplate to allocate memory for struct. */
#define ALLOC_STRUCT( StructName ) ((StructName *)malloc( sizeof( StructName )))

/**
Determine whether the given number is between the other two numbers
(both inclusive).
*/
#define IS_BETWEEN( numToTest, numLow, numHigh ) \
        ((unsigned char)((numToTest) >= (numLow) && (numToTest) <= (numHigh)))

/* when condition is true, sizeof(char[-1]), produces an error and compilation fails */
#define BUILD_BUG_ON(condition) ((void)sizeof(char[1 - 2*!!(condition)]))

#define IS_LITTLE_ENDIAN()  (((*(short *)"21") & 0xFF) == '2')
#define IS_BIG_ENDIAN()     (((*(short *)"21") & 0xFF) == '1')
#define DIV_ROUND_UP(n,d) (((n) + (d) - 1) / (d))

#ifndef swap
#   define swap(a, b) (((a) ^= (b)), ((b) ^= (a)), ((a) ^= (b)))
#endif

#define _INTERNAL __attribute__((visibility("internal")))
#define _PUBLIC __attribute__((visibility("default")))
#define _PRIVATE __attribute__((visibility("hidden")))
#define NO_EXPORT _PRIVATE
#define __must_check __attribute__((warn_unused_result))

/**
 * abs - return absolute value of an argument
 * @x: the value. If it is unsigned type, it is converted to signed type first.
 * char is treated as if it was signed (regardless of whether it really is)
 * but the macro's return type is preserved as char.
 *
 * Return: an absolute value of x.
 */
#define abs(x) __abs_choose_expr(x, long long, \
        __abs_choose_expr(x, long, \
        __abs_choose_expr(x, int, \
        __abs_choose_expr(x, short, \
        __abs_choose_expr(x, char, \
        __builtin_choose_expr( \
            __builtin_types_compatible_p(typeof(x), char), \
            (char)({ signed char __x = (x); __x<0?-__x:__x; }), \
            ((void)0)))))))

#define __abs_choose_expr(x, type, other) __builtin_choose_expr( \
    __builtin_types_compatible_p(typeof(x), signed type) || \
    __builtin_types_compatible_p(typeof(x), unsigned type), \
    ({ signed type __x = (x); __x < 0 ? -__x : __x; }), other)

// #define likely(x) __builtin_exp ect(!!(x), 1)
// #define unlikely(x) __builtin_exp ect(!!(x), 0)
#define likely(x)       __builtin_expect(!!(x), 1)
#define unlikely(x)     __builtin_expect(!!(x), 0)

#define ACCESS_ONCE(x) (*(volatile typeof(x) *)&(x))

/* Optimization barrier */
/* The "volatile" is due to gcc bugs */
#define barrier() __asm__ __volatile__("": : :"memory")

#endif /* _MACRO_H_ */
