/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo@bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CRYPTO_BLAKE2_H
#define _CRYPTO_BLAKE2_H

#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "definitions.h"
#include "macro.h"


/*
 * BLAKE2b implementation.
 */

// initialization vector.
static const uint64_t blake2b_IV[8] = {
    0x6a09e667f3bcc908, 0xbb67ae8584caa73b, 0x3c6ef372fe94f82b,
    0xa54ff53a5f1d36f1, 0x510e527fade682d1, 0x9b05688c2b3e6c1f,
    0x1f83d9abfb41bd6b, 0x5be0cd19137e2179
};


// permutation table.
static const uint8_t blake2b_sigma[12][16] = {
    { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 },
    { 14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3 },
    { 11, 8, 12, 0, 5, 2, 15, 13, 10, 14, 3, 6, 7, 1, 9, 4 },
    { 7, 9, 3, 1, 13, 12, 11, 14, 2, 6, 5, 10, 4, 0, 15, 8 },
    { 9, 0, 5, 7, 2, 4, 10, 15, 14, 1, 11, 12, 6, 8, 3, 13 },
    { 2, 12, 6, 10, 0, 11, 8, 3, 4, 13, 7, 5, 15, 14, 1, 9 },
    { 12, 5, 1, 15, 14, 13, 4, 10, 0, 7, 6, 3, 9, 2, 8, 11 },
    { 13, 11, 7, 14, 12, 1, 3, 9, 5, 0, 15, 4, 8, 6, 2, 10 },
    { 6, 15, 14, 9, 11, 3, 0, 8, 12, 2, 13, 7, 1, 4, 10, 5 },
    { 10, 2, 8, 4, 7, 6, 1, 5, 15, 11, 9, 14, 3, 12, 13, 0 },
    { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 },
    { 14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3 }
};


// constants.
enum blake2b_constant {
    BLAKE2B_BLOCKBYTES = 128,
    BLAKE2B_OUTBYTES = 64,
    BLAKE2B_KEYBYTES = 64,
    BLAKE2B_SALTBYTES = 16,
    BLAKE2B_PERSONALBYTES = 16,
};


// parameters.
typedef struct _blake2b_param {
    uint8_t digest_length;                   /* 1  */
    uint8_t key_length;                      /* 2  */
    uint8_t fanout;                          /* 3  */
    uint8_t depth;                           /* 4  */
    uint32_t leaf_length;                    /* 8  */
    uint64_t node_offset;                    /* 16 */
    uint8_t node_depth;                      /* 17 */
    uint8_t inner_length;                    /* 18 */
    uint8_t reserved[14];                    /* 32 */
    uint8_t salt[BLAKE2B_SALTBYTES];         /* 48 */
    uint8_t personal[BLAKE2B_PERSONALBYTES]; /* 64 */
} blake2b_param;


// state.
typedef struct _blake2b_state {
    uint64_t h[8];                   // Chained state
    uint64_t t[2];                   // total number of bytes
    uint64_t f[2];                   // last block flag
    uint8_t buf[BLAKE2B_BLOCKBYTES]; // input buffer
    size_t buflen;                   // size of buffer
    size_t outlen;                   // digest size
} blake2b_state;


int blake2b_init(blake2b_state*, size_t, const void*, size_t);
int blake2b_update(blake2b_state*, const void*, size_t);
int blake2b_final(blake2b_state*, void*);
int blake2b(void*, size_t, const void*, size_t, const void*, size_t);
void blake2b_increment_counter(blake2b_state*, const uint64_t);


/*
 * BLAKE2s implementation.
 */

enum blake2s_lengths {
    BLAKE2S_BLOCK_SIZE = 64,
    BLAKE2S_HASH_SIZE = 32,
    BLAKE2S_KEY_SIZE = 32,

    BLAKE2S_128_HASH_SIZE = 16,
    BLAKE2S_160_HASH_SIZE = 20,
    BLAKE2S_224_HASH_SIZE = 28,
    BLAKE2S_256_HASH_SIZE = 32,
};


struct blake2s_state {
    /* 'h', 't', and 'f' are used in assembly code, so keep them as-is. */
    uint32_t h[8];
    uint32_t t[2];
    uint32_t f[2];
    uint8_t buf[BLAKE2S_BLOCK_SIZE];
    unsigned int buflen;
    unsigned int outlen;
};


enum blake2s_iv {
	BLAKE2S_IV0 = 0x6A09E667UL,
	BLAKE2S_IV1 = 0xBB67AE85UL,
	BLAKE2S_IV2 = 0x3C6EF372UL,
	BLAKE2S_IV3 = 0xA54FF53AUL,
	BLAKE2S_IV4 = 0x510E527FUL,
	BLAKE2S_IV5 = 0x9B05688CUL,
	BLAKE2S_IV6 = 0x1F83D9ABUL,
	BLAKE2S_IV7 = 0x5BE0CD19UL,
};


static inline void __blake2s_init(struct blake2s_state *state, size_t outlen,
                                  const void *key, size_t keylen) {

    state->h[0] = BLAKE2S_IV0 ^ (0x01010000 | keylen << 8 | outlen);
    state->h[1] = BLAKE2S_IV1;
    state->h[2] = BLAKE2S_IV2;
    state->h[3] = BLAKE2S_IV3;
    state->h[4] = BLAKE2S_IV4;
    state->h[5] = BLAKE2S_IV5;
    state->h[6] = BLAKE2S_IV6;
    state->h[7] = BLAKE2S_IV7;
    state->t[1] = 0;
    state->f[0] = 0;
    state->f[1] = 0;
    state->buflen = 0;
    state->outlen = outlen;

    if (keylen) {
        memcpy(state->buf, key, keylen);
        memset(&state->buf[keylen], 0, BLAKE2S_BLOCK_SIZE - keylen);
        state->buflen = BLAKE2S_BLOCK_SIZE;
    }
}


static inline void blake2s_init(struct blake2s_state *state, const size_t outlen) {
    __blake2s_init(state, outlen, NULL, 0);
}


static inline void blake2s_init_key(struct blake2s_state *state, const size_t outlen,
                                    const void *key, const size_t keylen) {
    __blake2s_init(state, outlen, key, keylen);
}


void blake2s_update(struct blake2s_state *state, const uint8_t* in, size_t inlen);
void blake2s_final(struct blake2s_state *state, uint8_t* out);

static inline void blake2s(uint8_t* out, const uint8_t* in, const uint8_t* key,
                           const size_t outlen, const size_t inlen, const size_t keylen) {
    struct blake2s_state state;
    __blake2s_init(&state, outlen, key, keylen);
    blake2s_update(&state, in, inlen);
    blake2s_final(&state, out);
}


/* BLAKE2S Internal */

void blake2s_compress_generic(struct blake2s_state* state, const uint8_t *block,
                              size_t nblocks, const uint32_t inc);

void blake2s_compress(struct blake2s_state *state, const uint8_t *block,
                      size_t nblocks, const uint32_t inc);

bool blake2s_selftest(void);

static inline void blake2s_set_lastblock(struct blake2s_state* state) {
    state->f[0] = -1;
}

static inline void __blake2s_update(struct blake2s_state *state, const uint8_t* in,
                                    size_t inlen, bool force_generic) {

    const size_t fill = BLAKE2S_BLOCK_SIZE - state->buflen;
    
    if (unlikely(!inlen))
        return;

    if (inlen > fill) {
        memcpy(state->buf + state->buflen, in, fill);
        if (force_generic)
            blake2s_compress_generic(state, state->buf, 1, BLAKE2S_BLOCK_SIZE);
        else
            blake2s_compress(state, state->buf, 1, BLAKE2S_BLOCK_SIZE);

        state->buflen = 0;
        in += fill;
        inlen -= fill;
    }
    if (inlen > BLAKE2S_BLOCK_SIZE) {
        const size_t nblocks = DIV_ROUND_UP(inlen, BLAKE2S_BLOCK_SIZE);
        /* hash one less (full) block than strictly possible */
        if (force_generic)
            blake2s_compress_generic(state, in, nblocks - 1, BLAKE2S_BLOCK_SIZE);
        else
            blake2s_compress(state, in, nblocks - 1, BLAKE2S_BLOCK_SIZE);
        in += BLAKE2S_BLOCK_SIZE * (nblocks - 1);
        inlen -= BLAKE2S_BLOCK_SIZE * (nblocks - 1);
    }
    memcpy(state->buf + state->buflen, in, inlen);
    state->buflen += inlen;
}


static inline void __blake2s_final(struct blake2s_state *state, uint8_t *out,
                                   bool force_generic) {
    blake2s_set_lastblock(state);
    memset(state->buf + state->buflen, 0, BLAKE2S_BLOCK_SIZE - state->buflen); // padding.

    if (force_generic)
        blake2s_compress_generic(state, state->buf, 1, state->buflen);
    else
        blake2s_compress(state, state->buf, 1, state->buflen);
    memcpy(out, state->h, state->outlen);
}


#endif /* _CRYPTO_BLAKE2_H */
