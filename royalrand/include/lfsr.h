/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo@bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _LFSR_H_
#define _LFSR_H_
#include <stdint.h>

typedef struct _lfsr_ctx {
    uint64_t state;
} lfsr_ctx;

lfsr_ctx* lfsr(uint64_t state);
void lfsr_free(lfsr_ctx *context);

uint64_t lfsr64_fast(uint64_t _state, unsigned int nbits);
uint32_t lfsr_fast(uint32_t _state, unsigned int nbits);
uint64_t lfsr_step(lfsr_ctx* context);

#endif /* _LFSR_H_ */
