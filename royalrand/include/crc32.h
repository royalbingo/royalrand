#ifndef CRC32_H
#define CRC32_H
#include <stdint.h>
#include <stddef.h>

uint32_t crc32(const void *buf, size_t size);
static uint32_t singletable_crc32c(uint32_t crc, const void *buf, size_t size);
static uint32_t crc32c_sb8_64_bit(uint32_t crc, const unsigned char *p_buf, uint32_t length, uint32_t init_bytes);
static uint32_t multitable_crc32c(uint32_t crc32c, const unsigned char *buffer, unsigned int length);
uint32_t calculate_crc32c(uint32_t crc32c, const unsigned char *buffer, unsigned int length);

#endif /* CRC32_H */