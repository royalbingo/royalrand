/**
 * @file mt19937c.h
 * @author Murilo Augusto (murilo74@protonmail.com)
 * @brief Customization of MT19937 (header).
 * @version 0.1
 * @date 2022-05-16
 * 
 * @copyright Copyright (c) 2022 Murilo Augusto
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 */

#ifndef _MT19937C_H_
#define _MT19937C_H_

#include <stdint.h>
#include <pthread.h>
#include "pool.h"


/* Period parameters */  
#define N 624
#define M 397
#define MATRIX_A 0x9908b0dfUL   /* constant vector a */
#define UPPER_MASK 0x80000000UL /* most significant w-r bits */
#define LOWER_MASK 0x7fffffffUL /* least significant r bits */

#define MTC_DEFAULT_RANDOM_INIT         8
#define MTC_DEFAULT_SEED                5489UL
#define MTC_NEVER_GENERATE_ZERO         1

typedef struct _mt19937c_ctx _mt19937c_ctx;
typedef uint32_t (*pool_getter)(_mt19937c_ctx* self, unsigned int index);
typedef void (*pool_setter)(_mt19937c_ctx* self, unsigned int index, uint32_t value);

/* MT19937 custom context */
typedef struct _mt19937c_ctx {
    pool32mt_t pool;
    pool_getter get;
    pool_setter set;
    int mti;
} mt19937c_ctx;

/* Context management */
mt19937c_ctx* mt19937c(void);
void mt19937c_free(mt19937c_ctx* context);

void mtc_init_genrand(mt19937c_ctx* context, unsigned long s);
void mtc_init_by_array(mt19937c_ctx* context, unsigned long init_key[], int key_length);
void mtc_random_init(mt19937c_ctx* context);
void mtc_safe_init(mt19937c_ctx* context);

unsigned long mtc_genrand_int32(mt19937c_ctx* context);
long mtc_genrand_int31(mt19937c_ctx* context);
double mtc_genrand_real1(mt19937c_ctx* context);
double mtc_genrand_real2(mt19937c_ctx* context);
double mtc_genrand_real3(mt19937c_ctx* context);
double mtc_genrand_res53(mt19937c_ctx* context);
uint32_t mtc_uniform32(mt19937c_ctx *context, uint32_t upper);

#endif /* _MT19937C_H_ */
