/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo@bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _RDRAND_H_
#define _RDRAND_H_

#include <stdint.h>

#define RDRAND_RETRY 10
#define RDSEED_RETRY 10

extern int rdrand16_step(uint16_t *therand);
extern int rdseed16_step(uint16_t *therand);

extern int rdrand32_step(uint32_t *therand);
extern int rdseed32_step(uint32_t *therand);

extern int rdrand64_step(uint64_t *therand);
extern int rdseed64_step(uint64_t *therand);

extern int rdrand_get_uint32_retry(uint32_t retry_limit, uint32_t *dest);
extern int rdseed_get_uint32_retry(uint32_t retry_limit, uint32_t *dest);
extern int rdrand_get_uint64_retry(uint32_t retry_limit, uint64_t *dest);
extern int rdseed_get_uint64_retry(uint32_t retry_limit, uint64_t *dest);

extern int rdrand_check_support();
extern int rdseed_check_support();

extern int rdrand_get_n_uint32_retry(uint32_t n, uint32_t retry_limit, uint32_t *dest);
extern int rdseed_get_n_uint32_retry(uint32_t n, uint32_t retry_limit, uint32_t *dest);
extern int rdrand_get_n_uint64_retry(uint32_t n, uint32_t retry_limit, uint64_t *dest);
extern int rdseed_get_n_uint64_retry(uint32_t n, uint32_t retry_limit, uint64_t *dest);

#endif /* _RDRAND_H_ */
