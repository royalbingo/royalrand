/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo@bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CRYPT_H_
#define _CRYPT_H_

#include <stdint.h>
#include <stddef.h>
#include "definitions.h"

#define HARDWARE_SANITY_LOOP      16

typedef unsigned ulong32;

#define SWAP(x,y) do \
   { unsigned char swap_temp[sizeof(x) == sizeof(y) ? (signed)sizeof(x) : -1]; \
     memcpy(swap_temp,&y,sizeof(x)); \
     memcpy(&y,&x,       sizeof(x)); \
     memcpy(&x,swap_temp,sizeof(x)); \
    } while(0)

void get_urandom_uint32(uint32_t*);
void get_external_uint32(uint32_t*);
void get_ns_uint32(uint32_t*);
void get_hardware_uint32(uint32_t*);
int get_hardware_seed(uint32_t *dest, uint32_t width);
uint32_t get_epoch(void);
void get_time_entropy(uint32_t *number);
int get_linux_random(void* buf, size_t len);
int get_entropy_sysctl(void *buf, size_t len);

//uint64_t rotr64(const uint64_t, const unsigned);

static inline uint64_t rotr64(const uint64_t word, const unsigned offset) {
    return (word >> offset) | (word << (64 - offset));
}

/**
 * @brief Given a `size` of an input this calculates the padding needed to fill
 * the last block of size `block_size`.
 * 
 * @param size (size_t) Input size.
 * @param block_size (size_t) Block size.
 * @return size_t padding needed to fill the final block.
 */
static inline size_t calculate_padding(size_t size, size_t block_size) {
    return (block_size - size % block_size) % block_size;
}

/**
 * @brief Given a `size` of an input this calculates how many blocks of size
 * `block_size` is needed to fill.
 * 
 * Obs: Don't forget to check the padding, if needed of course.
 * 
 * @param size (size_t) Input size.
 * @param block_size (size_t) Block size.
 * @return size_t amount of blocks needed to fit.
 */
static inline size_t calculate_blocks(size_t size, size_t block_size) {
    size_t result = size / block_size;
    return (size % block_size != 0) ? result+1 : result; 
}

static inline uint32_t mersenne_temper(uint32_t y) {
    y ^= (y >> 11);
    y ^= (y << 7) & 0x9d2c5680UL;
    y ^= (y << 15) & 0xefc60000UL;
    y ^= (y >> 18);
    return y;
}

#define STORE64H(x, y)                          \
do { ulong64 ttt = __builtin_bswap64 ((x));     \
      XMEMCPY ((y), &ttt, 8); } while(0)

#define LOAD64H(x, y)                           \
do { XMEMCPY (&(x), (y), 8);                    \
      (x) = __builtin_bswap64 ((x)); } while(0)

#define STORE32L(x, y)        \
  do { ulong32 ttt = (x); memcpy(y, &ttt, 4); } while(0)

#define LOAD32L(x, y)         \
  do { memcpy(&(x), y, 4); x &= 0xFFFFFFFF; } while(0)

#define STORE64L(x, y)        \
  do { ulong64 ttt = (x); memcpy(y, &ttt, 8); } while(0)

#define LOAD64L(x, y)         \
  do { memcpy(&(x), y, 8); } while(0)

uint64_t load64(const void*);

void store64(void*, uint64_t);
void store32(void*, uint32_t);

#define BSWAP(x)  ( ((x>>24)&0x000000FFUL) | ((x<<24)&0xFF000000UL)  | \
                    ((x>>8)&0x0000FF00UL)  | ((x<<8)&0x00FF0000UL) )

static inline ulong32 ROL(ulong32 word, int i)
{
    asm ("roll %%cl,%0"
        : "=r" (word)
        : "0" (word),"c" (i));
    return word;
}

#define ROLc(word,i) ({ \
   ulong32 ROLc_tmp = (word); \
   __asm__ ("roll %2, %0" : \
            "=r" (ROLc_tmp) : \
            "0" (ROLc_tmp), \
            "I" (i)); \
            ROLc_tmp; \
   })

#define RORc(word,i) ({ \
   ulong32 RORc_tmp = (word); \
   __asm__ ("rorl %2, %0" : \
            "=r" (RORc_tmp) : \
            "0" (RORc_tmp), \
            "I" (i)); \
            RORc_tmp; \
   })

static inline ulong32 ROR(ulong32 word, int i)
{
    asm ("ror1 %%cl,%0"
        : "=r" (word)
        : "0" (word),"c" (i));
    return word;
}

void _pool32mt_block_permute(uint32_t* x, unsigned int nrounds);
void _pool32mt_block_swap(uint32_t* x);

#endif /* _CRYPT_H_ */
