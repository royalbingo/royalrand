// Linear Congruential Generator - header file
// Copyright (c) 2022 Murilo Augusto
// 
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

#ifndef _LCG_H_
#define _LCG_H_

typedef struct _lcg_param {
    uint64_t m;   /* Modulus    */
    uint64_t a;   /* Multiplier */
    uint64_t c;   /* Increment  */
} lcg_param;

typedef struct _lcg_ctx {
    uint64_t seed;
    lcg_param param;
} lcg_ctx;

lcg_ctx* lcg(uint64_t a, uint64_t c, uint64_t m, uint64_t seed);
void lcg_free(lcg_ctx* context);
uint64_t lcg_step(lcg_ctx* context);
float lcg_float_step(lcg_ctx* context);
lcg_ctx* lcg_create(void);
int lcg_set_param(lcg_ctx *context, uint64_t a, uint64_t c, uint64_t m, uint64_t seed);

#endif /* _LCG_H_ */