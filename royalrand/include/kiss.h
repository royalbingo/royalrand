/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This code is an adaptation of George Marsaglia, 1999 original implementation
 * written in C.
 * Source can be found at http://www.cs.yorku.ca/~oz/marsaglia-rng.html
 * 
 * And also an adaptation of KISS11 (MWC (Multiply-With-Carry) RNG), 2011
 * also by Marsaglia.
 * Source & details can be found at https://groups.google.com/g/sci.crypt/c/PLWrRP91jms/m/hWSG3WIAMDoJ
 * 
 * George Marsaglia <geo@stat.fsu.edu>
 */ 
#ifndef KISS_H
#define KISS_H
#define UNI     (KISS*2.328306e-10)
#define VNI     ((long) KISS)*4.656613e-10
#define UC      (unsigned char) /*a cast operation*/
#define LFIB4   (state->c++,state->t[state->c]=state->t[state->c]+state->t[UC(state->c+58)]+state->t[UC(state->c+119)]+state->t[UC(state->c+178)])

typedef unsigned long UL;

/* General context for kiss. */
typedef struct _kiss_state_t {
    UL bro, jsr, jcong, a, b, v, w, x, y, z;
    unsigned char c;
    UL t[256];
} kiss_state_t;

void destroy_kiss(kiss_state_t *kiss_state);
void settable(kiss_state_t *state, UL i1,UL i2,UL i3,UL i4,UL i5, UL i6);
kiss_state_t *create_kiss(void);
UL kiss(kiss_state_t *state);
UL swb(kiss_state_t *state);
UL fib(kiss_state_t *state);
UL lfib4(kiss_state_t *state);
UL mwc(kiss_state_t *state);
UL cong(kiss_state_t *state);
UL shr3(kiss_state_t *state);

/*
 KISS11 adapt. (MWC, George Marsaglia, 2011 impl.)
*/

#define CNG ( self->cng=69069*self->cng+13579 )
#define XS ( self->xs^=(self->xs<<13), self->xs^=(self->xs>>17), self->xs^=(self->xs<<5) )

typedef struct _b32MWC_t {
    unsigned long Q[4194304];
    unsigned long carry;
    int j;
    unsigned long int cng, xs;
} b32MWC_t;

unsigned long b32MWC(b32MWC_t *self);
unsigned long b32MWC_KISS(b32MWC_t *self);
b32MWC_t *create_b32MWC(void);
void destroy_b32MWC(b32MWC_t *state);

#endif /* KISS_H */