/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo@bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _POOL_H_
#define _POOL_H_
#include <stdint.h>
#include <pthread.h>

/* Recomendation: 
 * Pool size in general must be multiple of 16 or 32.
 */
#define POOL16_SIZE     192     /* uint16_t bytes */
#define POOL32_SIZE     512     /* uint32_t bytes */

/* Mersenne Twister Pools */
#define POOL32MT_SIZE   1248    /* uint32_t bytes */
#define POOL16MT_SIZE   1248    /* uint16_t bytes */

#define POOL32MT_BLOCK_SIZE     (POOL32MT_SIZE/2)
#define POOL16MT_BLOCK_SIZE     (POOL16MT_SIZE/2)

#define POOL_SWAP       1           /* swap pool flag */
#define POOL_DONT_SWAP  0           /* not swap pool flag */
#define POOL16MT_MAXIT  128         /* max iteration until encrypt */

#define API_INPUT_MAX_SIZE      65536   /* bytes */
#define POOL32MT_MAXIT          472     /* max iteration before re-encrypt ~ 216.498 n/ps */
#define POOL32MT_BLOCK_A    0
#define POOL32MT_BLOCK_B    (POOL32MT_SIZE/2)

#define POOL16_RESET    128
#define POOL32_RESET    128


/* Pool locks */

/**
 * @brief Lock structure for the Mersenne Twister pool.
 * The state could be:
 *  
 *      -1: POOL32MT_BLOCK_A LOCK
 *       0: UNLOCKED
 *       1: POOL32MT_BLOCK_B LOCK
 * 
 * You can't lock both blocks. Only one per time.
 */
typedef struct _pool32_lock {
    char state;
} pool32mt_lock;

/**
 * @brief Lock structure for the Mersenne Twister pool.
 * The state could be:
 *  
 *      -1: POOL32MT_BLOCK_A LOCK
 *       0: UNLOCKED
 *       1: POOL32MT_BLOCK_B LOCK
 * 
 * You can't lock both blocks. Only one per time.
 */
typedef struct _pool16_lock {
    char state;
} pool16mt_lock;


/* Pools */
#pragma pack(push, 1)
typedef struct _pool16 {
    uint16_t pool[POOL16_SIZE];
    uint16_t count;
    uint16_t cursor;
    uint16_t iteration;
} pool16_t;

typedef struct _pool32 {
    uint32_t pool[POOL32_SIZE];
    uint16_t count;
    uint16_t cursor;
    uint16_t iteration;
} pool32_t;

typedef struct _pool32mt {
    uint32_t pool[POOL32MT_SIZE];
    uint32_t count;
    uint16_t cursor;
    uint32_t iteration;
    pool32mt_lock lock;         // TODO: use only spinlock, instead ??
    pthread_spinlock_t spinlock;
    uint32_t main_block;
    uint16_t active_threads;
} pool32mt_t;

typedef struct _pool16mt {
    uint16_t pool[POOL16MT_SIZE];
    uint16_t count;
    uint16_t cursor;
    uint16_t iteration;
    pool16mt_lock lock;
    uint32_t main_block;
} pool16mt_t;
#pragma pack(pop)

/* Auxiliary data structures */
#pragma pack(push, 1)
struct pool32mt_touch_routine_args {
    pool32mt_t* pool;
    int rounds;
};
#pragma pack(pop)


static inline uint32_t pool32mt_get_main_block(pool32mt_t* pool) {
    return pool->main_block;
}

static inline uint32_t pool32mt_get_secondary_block(pool32mt_t* pool) {
    if (pool->main_block == POOL32MT_BLOCK_A)
        return POOL32MT_BLOCK_B;
    return POOL32MT_BLOCK_A;
}

/* pool16 */
void pool16_insert(pool16_t*, uint16_t);
void pool16_insert_entropy(pool16_t*);
void pool16_init(pool16_t*);

/* pool32 */
void pool32_insert(pool32_t*, uint32_t);
void pool32_insert_entropy(pool32_t*);
void pool32_init(pool32_t*);
void pool32_chacha(pool32_t*);

/* pool32mt */
void pool32mt_init(pool32mt_t*);
void pool32mt_insert(pool32mt_t*, uint32_t);
void pool32mt_add_entropy(pool32mt_t* pool, const void* source, size_t size);
void pool32mt_chacha(pool32mt_t* pool, int swap);
void pool32mt_aes(pool32mt_t* pool, int swap);
void pool32mt_swap(pool32mt_t*);
void pool32mt_insert_entropy(pool32mt_t*);
void pool32mt_fill(pool32mt_t*, uint32_t);
void pool32mt_fill_block(pool32mt_t* pool, unsigned int block, uint32_t value);
void pool32mt_xor(pool32mt_t*, uint32_t);
void pool32mt_block_permute(pool32mt_t* pool, unsigned int block, unsigned int nrounds);
void pool32mt_block_swap(pool32mt_t* pool, unsigned int block);
void pool32mt_diffuse(pool32mt_t* pool, unsigned int nrounds);
void pool32mt_diffuse_block(pool32mt_t* pool, unsigned int block, unsigned int nrounds);
void pool32mt_set_lock(pool32mt_t* pool, uint32_t block);
void pool32mt_unlock(pool32mt_t* pool);
void pool32mt_auto_unlock(pool32mt_t* pool);
uint32_t pool32mt_auto_lock(pool32mt_t* pool);
void pool32mt_touch(pool32mt_t* pool);
void pool32mt_shift_right(pool32mt_t* pool, const uint32_t offset);
void pool32mt_shift_left(pool32mt_t* pool, const uint32_t offset);
void pool32mt_rotate_left(pool32mt_t* pool, const uint32_t offset);
void pool32mt_rotate_right(pool32mt_t* pool, const uint32_t offset);
void aes128_random_encrypt(const void* in, void* out);
void aes128_ctr_random(const void* in, void* out, size_t size);

// special methods/functions.
void *__pool32mt_touch_routine(void* _arguments);

#endif /* _POOL_H_ */
