/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo@bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _HC256_H_
#define _HC256_H_

#include <stdint.h>
#include <string.h>
#include "ecrypt-portable.h"

#define XCHG(x, y) (t) = (x); (x) = (y); (y) = (t);

#define SIG0(x)(ROTR32((x),  7) ^ ROTR32((x), 18) ^ ((x) >>  3))
#define SIG1(x)(ROTR32((x), 17) ^ ROTR32((x), 19) ^ ((x) >> 10))

typedef struct hc_ctx_t {
  uint32_t ctr;
  union {
    uint32_t T[2048];
    struct {
      uint32_t P[1024];
      uint32_t Q[1024];
    };
  };
} hc_ctx;

#ifdef __cplusplus
extern "C" {
#endif

void hc256_setkeyx(hc_ctx*, void*);
void hc256_cryptx(hc_ctx*, void*, uint32_t);

void hc256_setkey(hc_ctx*, void*);
void hc256_crypt(hc_ctx*, void*, uint32_t);

#ifdef __cplusplus
}
#endif

#endif /* _HC256_H_ */
