// Copyright (c) 2022 Murilo Augusto
// 
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

#ifndef _MT19937_H_
#define _MT19937_H_

#include "pool.h"

/* Period parameters */  
#define N 624
#define M 397
#define MATRIX_A 0x9908b0dfUL   /* constant vector a */
#define UPPER_MASK 0x80000000UL /* most significant w-r bits */
#define LOWER_MASK 0x7fffffffUL /* least significant r bits */

/* MT19937 context */
typedef struct _mt19937_ctx {
    unsigned long mt[N];    /* the array for the state vector  */
    int mti;                /* mti==N+1 means mt[N] is not initialized */
} mt19937_ctx;

/* Context management */
mt19937_ctx* mt19937(void);
void mt19937_free(mt19937_ctx* context);

void mt_init_genrand(mt19937_ctx* context, unsigned long s);
void mt_init_by_array(mt19937_ctx* context, unsigned long init_key[], int key_length);

unsigned long mt_genrand_int32(mt19937_ctx* context);
long mt_genrand_int31(mt19937_ctx* context);
double mt_genrand_real1(mt19937_ctx* context);
double mt_genrand_real2(mt19937_ctx* context);
double mt_genrand_real3(mt19937_ctx* context);
double mt_genrand_res53(mt19937_ctx* context);
uint32_t mt_uniform32(mt19937_ctx *context, uint32_t upper);

#endif /* _MT19937_H_ */
