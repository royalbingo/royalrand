/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo@bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _AES_H_
#define _AES_H_

#define AES_BLOCK_SIZE 16
#define AES_MAXNR 14

#define AES_ENCRYPT 1
#define AES_DECRYPT 0

typedef struct aes_key {
    uint32_t key[(AES_MAXNR+1)*4];
    int rounds;
} AES_KEY;

int AES_set_encryption_key(const uint8_t *keychar, const int bits, AES_KEY* key);
int AES_set_decryption_key(const uint8_t *keychar, const int bits, AES_KEY* key);

void AES_encrypt(const uint8_t* in, uint8_t* out, const AES_KEY *key);
void AES_decrypt(const uint8_t* in, uint8_t* out, const AES_KEY *key);

void AES_cbc_encrypt(const uint8_t* in, uint8_t* out, unsigned long size,
                     const AES_KEY* key, uint8_t* iv, int forward_encrypt);

void AES_ctr(const uint8_t* in, uint8_t* out, size_t size,
             const uint8_t* nonce, const AES_KEY* key);


#endif /* _AES_H_ */
