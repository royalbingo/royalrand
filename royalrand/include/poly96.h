// Copyright (c) 2022 Murilo Augusto
// 
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

#ifndef _POLY96_H_
#define _POLY96_H_

#define a0      0x4b24716eUL
#define a1      0xfbc6cd96UL
#define a2      0xab7ab0cUL
#define B10     0x2fa51fb4UL
#define B11     0x2e1e2000UL
#define B12     0x03000000UL
#define B20     0x78d849e0UL
#define B21     0x55db0000UL

typedef struct _stateVector {
    unsigned long state0;
    unsigned long state1;
    unsigned long state2;
} stateVector_t;

stateVector_t* poly96(unsigned long state0, unsigned long state1, unsigned long state2);
unsigned long mk_tempering(unsigned long y0, unsigned long y1, unsigned long y2);
void poly96_free(stateVector_t* context);
double poly96_step(stateVector_t* context);

#endif /* _POLY96_H_ */