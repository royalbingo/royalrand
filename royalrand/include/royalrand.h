/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ROYALRAND_H
#define ROYALRAND_H

#include <stdint.h>
/* This header file extends macro.h */
#include "macro.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef uint64_t r_digit;

typedef enum {
    R_ZPOS = 0,     /* positive */
    R_NEG  = 1      /* negative */
} r_sign;

typedef enum {
    R_LT = -1,    /* less than */
    R_EQ = 0,     /* equal */
    R_GT = 1      /* greater than */
} r_ord;

typedef enum {
    R_OKAY  = 0,    /* no error */
    R_ERR   = -1,   /* unknown error */
    R_MEM   = -2,   /* out of memory */
    R_VAL   = -3,   /* invalid input */
    R_ITER  = -4,   /* maximum iterations reached */
    R_BUF   = -5,   /* buffer overflow, supplied buffer too small */
    R_OVF   = -6    /* numeric overflow */
} r_err;

typedef enum {
    R_LITTLE_ENDIAN     = -1,
    R_NATIVE_ENDIAN     = 0,
    R_BIG_ENDIAN        = 1
} r_endian;

typedef enum {
    R_LSB_FIRST = -1,
    R_MSB_FIRST = 1
} r_order;

/*
 * R_WUR - warn unused result
 * --------------------------
 * 
 * The result of functions annotated with R_WUR must be
 * checked and cannot be ignored.
 *
 */
#if defined(__GNUC__) && __GNUC__ >= 4
#   define R_WUR __attribute__((warn_unused_result))
#else
#   define R_WUR
#endif

#ifdef __cplusplus
}
#endif

#endif /* ROYALRAND_H */