#ifndef XORSHIFT_H
#define XORSHIFT_H
#include <stdint.h>

struct xorshift32_state {
    uint32_t a;
};

struct xorshift64_state {
    uint64_t a;
};

/* struct xorshift128_state can alternatively be defined as a pair
   of uint64_t or a uint128_t where supported */
struct xorshift128_state {
    uint32_t x[4];
};

uint32_t xorshift128(struct xorshift128_state *state);
uint64_t xorshift64(struct xorshift64_state *state);
uint32_t xorshift32(struct xorshift32_state *state);

#endif /* XORSHIFT_H */