
  ┏━━━┳━━━┳┓╋╋┏┳━━━┳┓╋╋╋╋╋╋╋╋╋╋╋╋┏┓
  ┃┏━┓┃┏━┓┃┗┓┏┛┃┏━┓┃┃╋╋╋╋╋╋╋╋╋╋╋╋┃┃
  ┃┗━┛┃┃╋┃┣┓┗┛┏┫┃╋┃┃┃╋╋┏━┳━━┳━┓┏━┛┃
  ┃┏┓┏┫┃╋┃┃┗┓┏┛┃┗━┛┃┃╋┏┫┏┫┏┓┃┏┓┫┏┓┃
  ┃┃┃┗┫┗━┛┃╋┃┃╋┃┏━┓┃┗━┛┃┃┃┏┓┃┃┃┃┗┛┃
  ┗┛┗━┻━━━┛╋┗┛╋┗┛╋┗┻━━━┻┛┗┛┗┻┛┗┻━━┛

    ┃ Cryptographically-secure
    ┃ Pseudorandom Number
    ┃ Generator


    ##### ENTROPY SOURCES ####                                                                      [General View]
    #                        #
    #  +------------------+  #  C   +-----+                                 BLOCK B
    #  | System Interface |  #      | AES |             MAIN ENTROPY POOL     /
    #  |                  | -#-->---| 128 |                 4992 bytes       /
    #  |  ex, hw, os, r   |  #      +--+--+      +---------------+----------/----+           +----------------------+
    #  +------------------+  #         |         |               |               |           |        Custom        |
    #                        #         +-----+->-|   2496 bytes  |   2496 bytes  |---->+---->|   Mersenne Twister   |
    #  +------------------+  #  C   +-----+  |   |               |               | E   |   A |       (MT19937)      |
    #  |       API        |  #      | AES |  |   +-------/-------^---------------+     |     +----------------------+
    #  |                  | -#-->---| CTR |  |          /        |           A         |
    #  |   add_entropy()  |  #      +--+--+  |         /         +-----------<-----(+)-+
    #  +------------------+  #         |     |     BLOCK A               Feed back   \
    #                        #      +--+---+ |                                       XOR with LFSR.
    ##########################      |BLAKE3|-+                                                               Figure 1
                                    +------+


The entropy is collected (C) from the System Interface or the API; The entropy then is encrypted with AES with a 128-bit
random key; The 16 bytes that outcomes from AES are added (A) to the pool in a random position (specified by an internal
cursor position).

When the Mersenne Twister requests a new value from the pool, this value is then extracted (E) to the MT routines (A).
Right before the extraction to the MT, an XOR operation is applied to the value using some value extracted from a linear
congruential generator (LCG) and then fed back to the pool (A).

 Entropy source types:

 The entropy comes from two different sources. `System Interface` and from the `API` as described below.

    `System Interface`: is composed by four sources of entropy (32 bit);

        ex, hw, os, r
         |   |   |  |
         |   |   |  +- LRNG;
         |   |   +---- Operating System RNG;
         |   +-------- Hardware RNG;
         +------------ External RNG;

    `Application Programming Interface (API)`: Public API that can be used to add whatever entropy you want to the pool.

 Twisted Generalized Feedback Shift Register (TGFSR):
    -  TGFSR state: 128 words (32bit)
    -  TGFSR period: 2**(128*32)-1



  The Generator and Entropy Pool Mechanism:

+--------------------------------------------------+
|                   4992 bytes                     | One of the generators used in this project
|      ________________^________________           | is a custom version of the Mersenne Twister
|     /                                 \          | Generator (MT19937).
|     +----------------+----------------+          |
|     |   2496 bytes   |   2496 bytes   |          | The customized version is implemented using
|     |       (A)      |       (B)      |          | a custom mechanism to extract, process and
|     |                |                |          | manage the entropy consumed by the internal
|     +----------------+----------------+          | state of the generator.
|                                                  |
|             (A)              (B)                 | The entropy are extracted as demonstrated
|              |                |                  | on `Figure 1`, after that all entropy are
|              v                +-> Auxiliary      | passed to a stream cipher AES in CTR mode.
|     This block is used             block         |
|    as internal state for                         | Every time that a entropy are added to the
|      MT 19937 generator                          | pool (could be in A or B) the entirely pool
|                                                  | is refreshed using 20 round of ChaCha and
|                                         Figure 2 | the pool swap with the other side.
+--------------------------------------------------+

 The default MT19937 uses as internal state an array of unsigned 32-bit integers with size of
 2496 bytes or 624 positions (uint32_t).

 The problem is: this generator is not cryptographically secure. The reason is that observing
 a sufficient number of iterations (624 in this case) allows one to predict all future iterations.

 The solution for this problem is creating a new implementation of the internal state and by
 controling all iterations.

 In our implementation the MT19937 still uses the 2496 bytes array as internal state, but now
 this block is part of a new big entropy pool that's controlled by a custom mechanism that changes
 it's composition every time that an iteration occurs or an entropy is added somehow.

 This mechanism uses various techniques to shift, shuffle, re-order and change the array values
 every time that an iteration occurs. (iteration could be both extraction and insertion to the pool).

 Everytime that the iteration counter reaches 128 iteration, the internal state block is swapped
 from the block (A) to the (B) and the previous block is locked and encrypted multiples times.

 The encryption method is also random-choiced by a LCG mechanism that can encrypt the block using
 multiple rounds of ChaCha, AES in CTR mode or HC-256 cipher.

 After that the internal state swaps again to acquire the block (A) and the block (B) is encrypted
 one more time.

 This proccess is repeated on every 128 iteration event.

 ...
