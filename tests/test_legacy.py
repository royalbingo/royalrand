
# Royal Rand - Crypto utility for Royal Bingo
# Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
#
# This file is part of Royal Rand.
#
# Royal Rand is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Royal Rand is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.

"""
Unit tests for legacy python interface.

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@ The legacy interface was not written in C. @
@ This is purely written in Python.          @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
"""

from interface import Mersenne_Twister
from interface import xor_array
from interface import get_simple_uint_entropy
from interface import get_uint_entropy
from interface import get_uint_array_entropy


def test_mersenne_twister_trivial():
    mt = Mersenne_Twister().init(6364136223846793005)
    assert mt.generate_int32() == 2821725841
    assert mt.generate_res53() == 0.02828642370038459

def test_mersenne_twister_trivial_initialization():
    mt = Mersenne_Twister().init_by_array((
        1664525, 1013904223, 22695477, 1103515245,
        12345, 1103515245, 134775813, 1140671485
    ), 8)

    assert mt.generate_int32() == 2123986733

def test_xor_array_trivial():
    a = (1, 2, 3, 4, 5)
    b = (6, 7, 8, 9, 1)
    assert xor_array(a, b) == [7, 5, 11, 13, 4]

def test_get_simple_uint_entropy():
    e = get_simple_uint_entropy(4096)
    assert e.bit_length() - 4096 > 1365

def test_get_uint_entropy_low_bytes():
    
    hits = 0
    
    for i in range(32):
        e = get_uint_entropy(4)
        if e.bit_length() == 32: hits += 1
    
    # in rare cases this will fail.
    assert hits >= 12

def test_get_uint_array_entropy():
    e = get_uint_array_entropy(8, 8)
    e1 = get_uint_array_entropy(0, 8)
    
    bits = 0
    for i in e:
        bits += i.bit_length()
    
    assert bits >= 498
    assert not all(e1)  # everything must be zero here.