
define banner
	@echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
	@echo "@                                                                          @"
	@echo "@    Royal Rand - Makefile                                                 @"
	@echo "@                                                                          @"
	@echo "@     available targets                                                    @"
	@echo "@     -------------------------------------------------------------------- @"
	@echo "@      build          build royalrand (this requires an entry point)       @"
	@echo "@      static         build a static library                               @"
	@echo "@      python         build the python extension (requires static library) @"
	@echo "@      clean          clean royalrand build                                @"
	@echo "@      python-install install python extension                             @"
	@echo "@      python-clean   clean python extension build                         @"
	@echo "@      check          perform a self-test in royalrand library             @"
	@echo "@      dist           statically compilation + python extension            @"
	@echo "@                                                                          @"
	@echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
endef


all:
	$(call banner)
.PHONY: all

build:
	$(MAKE) -C royalrand/
.PHONY: build

static:
	$(MAKE) -C royalrand/ static
.PHONY: static

python:
	$(MAKE) -C royalrand/ python
.PHONY: python

python-install:
	$(MAKE) -C royalrand/ python-install
.PHONY: python-install

clean:
	$(MAKE) -C royalrand/ clean
.PHONY: clean

python-clean:
	$(MAKE) -C royalrand/ python-clean
.PHONY: python-clean
