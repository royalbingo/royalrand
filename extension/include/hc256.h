/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EXT_HC256_H
#define EXT_HC256_H

#define PY_SSIZE_T_CLEAN
#include <stdint.h>
#include <Python.h>
#include "structmember.h"
#include "../include/pydef.h"
#include "../include/royalrand/hc256.h"
#include "../include/royalrand/macro.h"

typedef struct {
    PyObject_HEAD
    hc_ctx *ctx;
    int is_initialized;
} HC256_Type;

extern RR_NO_EXPORT PyTypeObject HC256;

/* Clinic */
PyDoc_STRVAR(
    hc256_doc,
    ""
);

PyDoc_STRVAR(
    hc256_encrypt_doc,
    "encrypt(data: int) -> int\n"
    "--\n\n"
    "Encrypt 'data' using HC-256 stream cipher."
);

#endif /* EXT_HC256_H */
