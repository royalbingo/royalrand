/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EXT_MERSENNE_TWISTER_H
#define EXT_MERSENNE_TWISTER_H

#define PY_SSIZE_T_CLEAN
#include <stdint.h>
#include <Python.h>
#include "structmember.h"
#include "pydef.h"
#include "docs.h"
#include "royalrand/mt19937c.h"
#include "royalrand/mt19937.h"
#include "royalrand/pool.h"


#define is_initialized__doc__                                   \
    "is_initialized()\n"                                        \
    "--\n\n"                                                    \
    "Check if MT19937C pool is initialized."                    \

#define safe_init__doc__                                        \
    "safe_init()\n"                                             \
    "--\n\n"                                                    \
    "Safely initialize the MT19937C internal state."            \


#define init__doc__                                             \
    "init(seed)\n"                                              \
    "--\n\n"                                                    \
    "Initialize the MT19937 with the specified seed."           \


/* Custom Mersenne Twister (header)
 * -----------------------
 * This uses a custom pool (pool32mt_t)
 * as internal state.
 */ 

typedef struct {
    PyObject_HEAD
    mt19937c_ctx *ctx;      /* MT19937C context */
    int is_initialized;     /* initialization flag (access only by getter)*/
} MT19937C_Type;

extern RR_NO_EXPORT PyTypeObject MT19937C;

/* Mersenne Twister (header)
 * -----------------------
 * This is the original implementation
 * of the MT19937; This uses a 624 vector
 * as internal state.
 */ 

typedef struct {
    PyObject_HEAD
    mt19937_ctx *ctx;  /* MT19937 context */
    int is_initialized;
} MT19937_Type;

extern RR_NO_EXPORT PyTypeObject MT19937;


#endif /* EXT_MERSENNE_TWISTER_H */