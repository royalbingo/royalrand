/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PY_MODULE_DOCS_H
#define PY_MODULE_DOCS_H

#define PY_SSIZE_T_CLEAN
#include <Python.h>

PyDoc_STRVAR(
    uniform32__doc__,
    "uniform32(upper_bound)\n"
    "--\n\n"
    "Generate a 32-bit random integer [0, upper_bound) without bias."
    "\n"
    "Calculate a uniformly distributed random number less than\n"
    "upper avoiding \"modulo bias\"."
    "\n\n"
    "Uniformity is achieved by generating new random numbers until\n"
    "the one returned is outside the range [0, 2**32 % upper_bound).\n"
    "This guarantees the selected random number will be inside\n"
    "[2**32 % upper_bound, 2**32) which maps back to [0, upper_bound)\n"
    "after reduction modulo upper_bound.");

PyDoc_STRVAR(mtc_init__doc__,
    "init(seed)\n"
    "--\n\n"
    "Initialize the generator with speficied seed.\n"
    "\n"
    ".. Warning ..\n"
    "Do not use this function to initialize MT for cryptography purposes.\n"
    "For a safe initialization use safe_init(), instead."
    );

PyDoc_STRVAR(mtc_safe_init__doc__,
    "safe_init()\n"
    "--\n\n"
    "Safely initialize the internal state and the pools of MT19937C.\n"
    "\n"
    "This function is intended for cryptography uses of MT.\n"
    );

PyDoc_STRVAR(mtc_add_entropy_from_long__doc__,
    "add_entropy(value)\n"
    "--\n\n"
    "Insert entropy into the MT19937C pools.\n"
    "\n"
    "Notes\n"
    "-----\n"
    "The data passed to this function is fully encrypted right before\n"
    "being added to a random position in the pool.\n"
    "\n"
    );

PyDoc_STRVAR(mtc_genrand_int32__doc__,
    "genrand_int32()\n"
    "--\n\n"
    "Generate a random 32-bit unsigned integer.\n"
    "\n"
    "Warning\n"
    "-------\n"
    "Using modulo arithmetic to reduce the output may cause\n"
    "modulo bias. Use uniform32() to avoid it.\n"
    "\n"
    );

PyDoc_STRVAR(mtc_genrand_res53__doc__,
    "genrand_res53()\n"
    "--\n\n"
    "Generates a random number on [0,1) with 53-bit resolution.\n"
    "\n");

#endif /* PY_MODULE_DOCS_H */