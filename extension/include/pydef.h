/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PY_DEFINITIONS_H
#define PY_DEFINITIONS_H

#define RR_SYM_INTERNAL __attribute__((visibility("internal")))
#define RR_SYM_PUBLIC __attribute__((visibility("default")))
#define RR_SYM_PRIVATE __attribute__((visibility("hidden")))
#define RR_NO_EXPORT RR_SYM_PRIVATE

/* module definitions */
#define PY_MOD_NAME "royalrand"
#define PY_MOD_DESC "Royal Rand Library Interface"

/* methods definitions */
#define FOO__doc__ "Foo description."

#endif /* PY_DEFINITIONS_H */
