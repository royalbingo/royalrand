/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EXT_LCG_H
#define EXT_LCG_H

#define PY_SSIZE_T_CLEAN
#include <stdint.h>
#include <Python.h>
#include "structmember.h"
#include "../include/pydef.h"
#include "../include/royalrand/lcg.h"

#define LCG_MAX_VALUE   (9223372036854775807L)

typedef struct {
    PyObject_HEAD
    lcg_ctx *ctx;  /* lcg context */
    int is_initialized;
} LCG_Type;

extern RR_NO_EXPORT PyTypeObject LCG;

#endif /* EXT_LCG_H */