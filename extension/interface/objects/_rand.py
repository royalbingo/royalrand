
# Royal Rand - Crypto utility for Royal Bingo
# Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
#
# This file is part of Royal Rand.
#
# Royal Rand is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Royal Rand is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import random
import warnings as _warn

from ctypes import *

try:
    import rdrand
    import _rdrand
except ImportError:
    from ._dummy import Dummy_RDRAND
    from ._dummy import Dummy_CRDRAND
    rdrand = Dummy_RDRAND
    _rdrand = Dummy_CRDRAND

from typing import List
from typing import Generator

IS_RDRAND_AVAILABLE = all((_rdrand.is_rdrand_supported(), _rdrand.is_rdseed_supported()))

# Beep warning, because we may need rdrand.
if not IS_RDRAND_AVAILABLE:
    _warn.warn("rdrand and rdseed is not available!", RuntimeWarning)

def lcg(modulus: int, a, int, c: int, seed: int) -> Generator[int, None, None]:
    """ Linear Congruentional Generator """
    while True:
        seed = (a * seed + c) % modulus
        yield seed


def lfsr(_state: int, *, nbits=None):

    state = _state
    output = ''
    first_state = state
    counter = 0

    while 1:
        output += f'{state & 1}'
        newbit = (state ^ (state >> 1)) & 1
        state = (state >> 1) | (newbit << 3)
        counter += 1

        if nbits == counter:
            break

        if state == first_state:
            break

    return int(output, 2)



def range_below(amount: int, ubound: int, *, not_repeat=True) -> List[int]:
    """Generate `amount` random numbers bellow `ubound`."""

    result = []

    if not IS_RDRAND_AVAILABLE:
        _sysrand = random.SystemRandom()
        while len(result) != amount:
            n = _sysrand._randbelow(ubound)
            if not_repeat:
                if n not in result:
                    result.append(n)
            else:
                result.append(n)
    else:
        if not_repeat:
            while len(result) != amount:
                temp = rdrand.randint(0, ubound)
                if temp not in result:
                    result.append(temp)
        else:
            while len(result) != amount:
                result.append(rdrand.randint(0, ubound))

    return result


def randint(a: int, b: int) -> int:
    """ Return random integer in range [a, b], including both end points."""
    if not IS_RDRAND_AVAILABLE:
        return random.randint(a, b)
    return rdrand.randint(a, b)


