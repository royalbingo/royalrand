
# Royal Rand - Crypto utility for Royal Bingo
# Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
#
# This file is part of Royal Rand.
#
# Royal Rand is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Royal Rand is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from copy import deepcopy as dc
from ctypes import *

from ._rand import rdrand
from ._rand import range_below
from ._rand import randint
from ._rand import IS_RDRAND_AVAILABLE


""" Exceptions for Pool module """
class PoolError(Exception):
    pass

class PoolInitError(PoolError):
    pass

class PoolTypeError(PoolError):
    pass

class PoolNotInitialized(PoolError):
    pass


class RandPair(Structure):
    _fields_ = [("value", c_uint32),
                ("xor", c_uint32)]

    def __repr__(self) -> str:
        v = getattr(self, "value")
        x = getattr(self, "xor")
        return f"RandPair({v}, {x})"


class Pool:
    """
    Random Pool implementation.

    Re-assign items to array is prohibited.
    """
    def __init__(self) -> None:
        self._array = None
        self._elements = 0

    def __repr__(self) -> str:
        rep = " ?/? "
        i = "not initialized"

        if self._array is not None:
            i = "initialized"
            rep = f' {self._elements}/{len(self._array)} '

        m = f"<Pool object ({i}){rep}at {hex(id(self))}>"
        return m
    
    def _ensure_init(self) -> None:
        """ Raise error if pool isn't initialized """
        if self._array is None:
            raise PoolNotInitialized("Pool must be initialized before use")

    def _Pool_INC(self):
        self._ensure_init()
        self._elements += 1

    def _Pool_DEC(self):
        self._ensure_init()
        self._elements -= 1

    @staticmethod
    def _fetch_random_seed(*, _amount=4) -> c_uint32:
        """ Fetch random seed from rdrand or /dev/urandom """

        if _amount < 1:
            raise PoolError("_amount must be positive.")

        if _amount % 2 != 0:
            raise PoolError("_amount must be multiple of 2.")

        bsize = _amount * sizeof(c_int)
        init_seed = -1
        xor_value = int.from_bytes(open("/dev/random", "rb").read(bsize), "little")

        if IS_RDRAND_AVAILABLE:
            init_seed = abs(rdrand.rdseed())
        else:
            init_seed = int.from_bytes(
                open("/dev/urandom", "rb").read(bsize),
                "little"
            )
        return c_uint32(init_seed ^ xor_value)

    def init_pool(self, *, limit=32, empty=False) -> Pool:
        """ returns 0 if ok otherwise 1 """
        if self._array is not None:
            raise PoolInitError("Pool already initialized!")

        if limit < 1:
            raise PoolInitError("limit must be positive.")
        
        a = Pool._fetch_random_seed()
        b = Pool._fetch_random_seed()

        self._array = (RandPair * limit)()

        if not empty:
            self._array[0] = c_uint32(a.value^b.value), b
            self._Pool_INC()

        return self

    def add(self, _value: c_uint32) -> None:
        """ Add a pair to pool """
        self._ensure_init()
        value = None

        if isinstance(_value, int):
            value = c_uint32(_value)
        
        if isinstance(_value, c_uint):
            value = _value.value

        if value is None:
            raise PoolTypeError(
                    f"Incorrect type for value. Expected type " + 
                    f"'int' or 'c_uint', got {type(_value).__name__!r}"
            )

        xor: c_uint32 = Pool._fetch_random_seed()
        pair = RandPair(c_uint32(value.value ^ xor.value), xor)

        if self._elements == self._array._length_:
            # Shift array to left and append the pair to the end.
            for i in range(1, self._array._length_):
                self._array[i-1] = self._array[i]
            self._array[self._array._length_ - 1] = pair
        else:
            # Just append the pair to the end and increase '_elements'.
            self._array[self._elements] = pair
            self._Pool_INC()

    def mix(self, /, times=1) -> None:
        """ Randomly mix the pool. """
        self._ensure_init()
        
        if times < 1:
            raise PoolError("'times' must be greater than equal 1.")

        def _mix():
            rnd_indexes = range_below(*(self._array._length_,)*2)
            for i, j in zip(range(self._array._length_), rnd_indexes):
                self._array[i], self._array[j] = dc(self._array[j]), \
                                                 dc(self._array[i])
        for _ in range(times):
            _mix()

    def _get_list(self):
        """ Friendly show the content of array (hex). """
        self._ensure_init()
        return [hex(self._array[i].value) for i in range(self._elements)]

    def get_list(self) -> list:
        """ return a list of elements. """
        self._ensure_init()
        return [self._array[i] for i in range(self._elements)]

if __name__ == '__main__':
    if 1:
        pool = Pool()
        pool.init_pool(limit=6)
        
        for a in range(32):
            pool.add(a * a)

        print(pool._get_list())
        pool.mix(32)
        print(pool._get_list())
        
