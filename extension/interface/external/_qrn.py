
# Royal Rand - Crypto utility for Royal Bingo
# Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
#
# This file is part of Royal Rand.
#
# Royal Rand is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Royal Rand is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.

""" ANU Quantum Numbers - Quantum Random Numbers """
from __future__ import annotations
import requests

from typing import Union
from typing import Tuple
from typing import List


"""
ANU QRN - Exceptions
"""
class QRN_BaseException(BaseException):
    pass

class BadAPIKey(QRN_BaseException):
    pass

class InvalidParameter(QRN_BaseException):
    pass

class ConnectionError(QRN_BaseException):
    pass

class QRN_Error(QRN_BaseException):
    pass


"""
QRN Class.
"""
class QRN:

    QRN_URL = "https://api.quantumnumbers.anu.edu.au"

    def __init__(self) -> None:
        self.__QRN_KEY: Union[str, None] = None

    def set_key(self, api_key: str) -> QRN:
        """ Define a QRN Key """
        self.__QRN_KEY = str(api_key)
        return self

    def check_status(self) -> None:
        """ Check status of QRN and key """
        raise NotImplementedError

    def _build_payload(self, _length: int, _type: str, _bsize: int) -> Tuple[dict, dict]:
        """ Return (params, headers) """
        return (
            {"length": int(_length), "type": _type, "size": _bsize},
            {"x-api-key": self.__QRN_KEY}
        )

    def get_uint8(self, length: int) -> List:
        """ Get uint8 from ANU QRN. """
        if not 1 <= length <= 1024:
            raise InvalidParameter(
                f"bad length value. ({length} must be in the range [1,1024])"
            )

        params, headers = self._build_payload(length, "uint8", 1)    
        response = requests.get(QRN.QRN_URL, headers=headers, params=params)

        if response.status_code != 200:
            raise ConnectionError(f"Bad status code received ({response.status_code}).")

        content = response.json()
        if content["success"] == True:
            return content["data"]

        raise QRN_Error(content["message"])
