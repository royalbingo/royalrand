
# Royal Rand - Crypto utility for Royal Bingo
# Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
#
# This file is part of Royal Rand.
#
# Royal Rand is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Royal Rand is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.

""" Constants used by the interface """

# stdint.h
CHAR_BIT    = 8
SCHAR_MIN   = -128
SCHAR_MAX   = 127
UCHAR_MAX   = 255
CHAR_MIN    = -128
CHAR_MAX    = 127
MB_LEN_MAX  = 16
BPF         = 53
SHRT_MIN    = -32768
SHRT_MAX    = 32767
USHRT_MAX   = 65535
INT_MIN     = -2147483648
INT_MAX     = 2147483647
UINT_MAX    = 4294967295
LONG_MIN    = -9223372036854775808
LONG_MAX    = 9223372036854775807
ULONG_MAX   = 18446744073709551615

# masks for casting
uint32_t = 0xFFFFFFFF
uint64_t = 0xFFFFFFFFFFFFFFFF

