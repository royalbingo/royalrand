#
# Created on Mon Aug 08 2022
#
# The MIT License (MIT)
# Copyright (c) 2022 Murilo Augusto
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

import random
import os

from .objects import *
from .ciphers import ChaCha
from ._constants import *

from typing import List
from typing import Optional


if IS_RDRAND_AVAILABLE:
    import rdrand

_sysrand = random.SystemRandom()


__all__ = [
        'xor_array',
        'get_simple_uint_entropy',
        'get_simple_uint_array_entropy',
        'get_uint_entropy',
        'get_uint_array_entropy',
        'uint_to_bytes',
        'uint_from_bytes',
        'int_to_bytes',
        'int_from_bytes',
        'rand64',
        'rand32',
]


def xor_array(a: List, b: List) -> List:
    """ XOR array a with b """

    sz_a, sz_b = len(a), len(b)
    
    if sz_a != sz_b:
        raise ValueError("Arrays must have same length.")
    
    ret = [0] * sz_a
    for i in range(sz_a):
        ret[i] = a[i] ^ b[i]
    return ret[:]


def get_simple_uint_entropy(_bytes: int) -> int:
    """ Return `_bytes` of entropy using SystemRandom. """
    if _bytes < 1:
        return 0

    # other may cause a crash.
    if _bytes > 4096:
        raise ValueError("_bytes must be at least 4096 bytes.")
    return int.from_bytes(os.urandom(_bytes), "big")


def get_simple_uint_array_entropy(_bytes: int, length: int) -> List:
    """ Return a list with `length` elements of size `_bytes`."""
    if length < 1:
        raise ValueError("length must be positive")

    if _bytes < 1:
        return [0] * length

    return [get_simple_uint_entropy(_bytes)
            for _ in range(length)]


def get_uint_entropy(_bytes: int) -> int:
    """ Return `_bytes` of entropy using CSPRNG """

    # Steps to get the expected result.
    # 1. Get `_bytes` of entropy from SystemRandom device.
    # 2. Get `_bytes` of entropy from RDRAND interface (if available).
    # 3. Fit everything in a 16 byte array (`a` and `b`).
    # 4. XOR array `a` with `b`.
    # 5. Create ChaCha 16 byte block of xor'ed array.
    # 6. Create byte array with 16 byte chacha block.
    # 7. Return integer from bytes.

    if _bytes < 1:
        return 0

    a = [get_simple_uint_entropy(_bytes) for _ in range(16)]
    b = range_below(16, 0xff)  # this uses rdrand if available.
    c = ChaCha.block(xor_array(a, b), mfactor=4)
    ret = bytearray(x & 0xff for x in c[:_bytes])

    return int.from_bytes(ret, 'big')


def get_uint_array_entropy(_bytes: int, length: int) -> List:
    """ Return `_bytes` of entropy using CSPRNG """
    
    if length < 1:
        return []

    if _bytes < 1:
        return [0] * length

    return [get_uint_entropy(_bytes) for _ in range(length)]


def uint_to_bytes(x: int) -> bytes:
    return x.to_bytes((x.bit_length() + 7) // 8, 'big')


def uint_from_bytes(xbytes: bytes) -> int:
    return int.from_bytes(xbytes, 'big')


def int_to_bytes(x: int) -> bytes:
    return x.to_bytes(length=(8 + (x + (x < 0)).bit_length()) // 8,
                              byteorder='big', signed=True)


def int_from_bytes(binary_data: bytes) -> Optional[int]:
        return int.from_bytes(binary_data, byteorder='big', signed=True)


def rand32():
    """ return uint32_t random number."""
    if IS_RDRAND_AVAILABLE:
        return rdrand.rand32() & uint32_t
    return _sysrand.getrandbits(uint32_t.bit_length()) & uint32_t


def rand64():
    """ return uint64_t random number."""
    if IS_RDRAND_AVAILABLE:
        return rdrand.rand64() & uint64_t
    return _sysrand.getrandbits(uint64_t.bit_length()) & uint64_t


