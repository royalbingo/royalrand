
# Royal Rand - Crypto utility for Royal Bingo
# Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
#
# This file is part of Royal Rand.
#
# Royal Rand is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Royal Rand is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.

""" Stream cipher: Salsa20 implementation """
from ctypes import *
from typing import Tuple
from typing import Union


class Salsa20:

    # default rounds to 10.
    ROUNDS = 10

    @staticmethod
    def ROTL(a: int, b: int) -> int:
        return (((a) << (b)) | ((a) >> (32 - (b))))

    @staticmethod
    def QR(a: int, b: int, c: int, d: int) -> Tuple[int, int, int, int]:
        """ quarter round """
        b ^= Salsa20.ROTL(a + d, 7)
        c ^= Salsa20.ROTL(b + a, 9)
        d ^= Salsa20.ROTL(c + b, 13)
        a ^= Salsa20.ROTL(d + c, 19)
        return (a, b, c, d)

    @staticmethod
    def block(initial: list, *, inplace=False, mfactor=2) -> 'Array[c_uint32]':
        """ Salsa20 - 16 bytes blocks fixed """
        
        sz = len(initial)
        if sz != 16:
            raise ValueError(f"array must be length 16, not {sz}.")

        x = (c_uint32 * 16)()

        for i in range(16):
            x[i] = c_uint32(initial[i])
        
        # 10 loops x 2 rounds/loop = 20 rounds.
        for i in range(Salsa20.ROUNDS * mfactor):
            # Odd round.
            x[ 0], x[ 4], x[ 8], x[12] = Salsa20.QR(x[ 0], x[ 4], x[ 8], x[12])
            x[ 5], x[ 9], x[13], x[ 1] = Salsa20.QR(x[ 5], x[ 9], x[13], x[ 1])
            x[10], x[14], x[ 2], x[ 6] = Salsa20.QR(x[10], x[14], x[ 2], x[ 6])
            x[15], x[ 3], x[ 7], x[11] = Salsa20.QR(x[15], x[ 3], x[ 7], x[11])

            # Even round.
            x[ 0], x[ 1], x[ 2], x[ 3] = Salsa20.QR(x[ 0], x[ 1], x[ 2], x[ 3])
            x[ 5], x[ 6], x[ 7], x[ 4] = Salsa20.QR(x[ 5], x[ 6], x[ 7], x[ 4])
            x[10], x[11], x[ 8], x[ 9] = Salsa20.QR(x[10], x[11], x[ 8], x[ 9])
            x[15], x[12], x[13], x[14] = Salsa20.QR(x[15], x[12], x[13], x[14])

        if inplace:
            for i in range(16):
                initial[i] = x[i]
        return x[:]
        

if __name__ == '__main__':
    arr = [i for i in range(16)]
    print(Salsa20.block(arr))

