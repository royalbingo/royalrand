
# Royal Rand - Crypto utility for Royal Bingo
# Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
#
# This file is part of Royal Rand.
#
# Royal Rand is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Royal Rand is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.

""" Stream cipher: ChaCha implementation """
from ctypes import *
from typing import Tuple
from typing import Union


class ChaCha:

    # default rounds to 10.
    ROUNDS = 10

    @staticmethod
    def ROTL(a: int, b: int) -> int:
        return (((a) << (b)) | ((a) >> (32 - (b))))

    @staticmethod
    def QR(a: int, b: int, c: int, d: int) -> Tuple[int, int, int, int]:
        """ quarter round """
        a += b; d ^= a; d = ChaCha.ROTL(d, 16)
        c += d; b ^= c; b = ChaCha.ROTL(b, 12)
        a += b; d ^= a; d = ChaCha.ROTL(d,  8)
        c += d; b ^= c; b = ChaCha.ROTL(b,  7)
        return (a, b, c, d)

    @staticmethod
    def block(initial: list, *, inplace=False, mfactor=2) -> 'Array[c_uint32]':
        """ ChaCha - 16 bytes blocks fixed """
        
        sz = len(initial)
        if sz != 16:
            raise ValueError(f"array must be length 16, not {sz}.")

        x = (c_uint32 * 16)()

        for i in range(16):
            x[i] = c_uint32(initial[i])
        
        # 10 loops x 2 rounds/loop = 20 rounds.
        for i in range(ChaCha.ROUNDS * mfactor):
            # Odd round.
            x[ 0], x[ 4], x[ 8], x[12] = ChaCha.QR(x[ 0], x[ 4], x[ 8], x[12])
            x[ 1], x[ 5], x[ 9], x[13] = ChaCha.QR(x[ 1], x[ 5], x[ 9], x[13])
            x[ 2], x[ 6], x[10], x[14] = ChaCha.QR(x[ 2], x[ 6], x[10], x[14])
            x[ 3], x[ 7], x[11], x[15] = ChaCha.QR(x[ 3], x[ 7], x[11], x[15])

            # Even round.
            x[ 0], x[ 5], x[10], x[15] = ChaCha.QR(x[ 0], x[ 5], x[10], x[15])
            x[ 1], x[ 6], x[11], x[12] = ChaCha.QR(x[ 1], x[ 6], x[11], x[12])
            x[ 2], x[ 7], x[ 8], x[13] = ChaCha.QR(x[ 2], x[ 7], x[ 8], x[13])
            x[ 3], x[ 4], x[ 9], x[14] = ChaCha.QR(x[ 3], x[ 4], x[ 9], x[14])

        if inplace:
            for i in range(16):
                initial[i] = x[i]
        return x[:]
        

if __name__ == '__main__':
    arr = [i for i in range(16)]
    print(ChaCha.block(arr))

