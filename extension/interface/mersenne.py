
# Royal Rand - Crypto utility for Royal Bingo
# Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
#
# This file is part of Royal Rand.
#
# Royal Rand is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Royal Rand is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.

""" PRNG: Mersenne Twister Implementation """
from __future__ import annotations
from ctypes import *
from math import floor as _floor
from typing import Sequence

from ._constants import *


__all__ = ['Mersenne_Twister']

class Mersenne_Twister:
    """ Mersenne Twister implementation """

    # Constants
    N = 624
    M = 397
    MATRIX_A = 0x9908b0df
    UPPER_MASK = 0x80000000
    LOWER_MASK = 0x7fffffff

    def __init__(self) -> None:
        # state vector array.
        self.mt_array = [0] * self.N

        # mt initialization flag.
        self.mt_arrayi = self.N + 1 

    def init(self, s: int) -> Mersenne_Twister:
        self.mt_array[0] = s & 0xffffffff

        self.mt_arrayi = 1
        while self.mt_arrayi < self.N:
            self.mt_array[self.mt_arrayi] = (1812433253 * (self.mt_array[self.mt_arrayi-1] ^ \
                               (self.mt_array[self.mt_arrayi-1] >> 30)) + self.mt_arrayi)
            self.mt_array[self.mt_arrayi] &= 0xffffffff
            self.mt_arrayi += 1

        return self

    def init_by_array(self, init_key: Sequence[int], key_length: int) -> Mersenne_Twister:
        self.init(19650218)
        
        k = (self.N if self.N > key_length else key_length)
        i = 1
        j = 0

        while k > 0:
            # non linear.
            self.mt_array[i] = (self.mt_array[i] ^ ((self.mt_array[i-1] ^ \
                               (self.mt_array[i-1] >> 30)) * 1664525)) + \
                               init_key[j] + j  

            # for WORDSIZE > 32 machines.
            self.mt_array[i] &= 0xffffffff

            i+=1; j+=1
            
            if i >= self.N:
                self.mt_array[0] = self.mt_array[self.N - 1]
                i = 1

            if j >= key_length:
                j = 0

            k -= 1

        k = self.N - 1
        while k:
            # non linear.
            self.mt_array[i] = (self.mt_array[i] ^ ((self.mt_array[i-1] ^ \
                               (self.mt_array[i-1] >> 30)) * 1566083941)) - i
            self.mt_array[i] &= 0xffffffff
            i += 1

            if i >= self.N:
                self.mt_array[0] = self.mt_array[self.N - 1]
                i = 0
            k -= 1
        
        # MSB is 1; assuming non-zero initial array.
        self.mt_array[0] = 0x80000000
        return self

    def generate_int32(self) -> int:
        """ Generates a random number on [0, 0xffffffff]-interval """
        mag01 = (0x00, self.MATRIX_A)

        if self.mt_arrayi >= self.N:
            kk = 0

            if self.mt_arrayi == self.N + 1:
                # not initialized. use default initial seed.
                self.init(5489)

            while kk < self.N-self.M:
                y = (self.mt_array[kk] & self.UPPER_MASK) | \
                    (self.mt_array[kk+1] & self.LOWER_MASK)
                
                self.mt_array[kk] = self.mt_array[kk + self.M] ^ \
                                    (y >> 1) ^ mag01[y & 0x01]
                kk += 1
            
            while kk < self.N - 1:
                y = (self.mt_array[kk] & self.UPPER_MASK) | \
                    (self.mt_array[kk+1] & self.LOWER_MASK)
                
                self.mt_array[kk] = self.mt_array[kk+(self.M - self.N)] ^ \
                                    (y >> 1) ^ mag01[y & 0x01]
                kk += 1

            y = (self.mt_array[self.N-1] & self.UPPER_MASK) | \
                (self.mt_array[0] & self.LOWER_MASK)
            
            self.mt_array[self.N-1] = self.mt_array[self.M-1] ^ \
                                      (y >> 1) ^ mag01[y & 0x01]
            self.mt_arrayi = 0

        y = self.mt_array[self.mt_arrayi]
        self.mt_arrayi += 1

        # Tempering.
        y ^= (y >> 11)
        y ^= (y << 7) & 0x9d2c5680
        y ^= (y << 15) & 0xefc60000
        y ^= (y >> 18)

        return y

    def generate_int31(self) -> int:
        return self.generate_int32() >> 1

    def generate_real1(self) -> float:
        return self.generate_int32() * (1.0/4294967295.0)

    def generate_real2(self) -> float:
        return self.generate_int32() * (1.0/4294967296.0)
    
    def generate_real3(self) -> float:
        return (self.generate_int32() + 0.5) * (1.0/4294967296.0)

    def generate_res53(self) -> float:
        """generates a random number on [0,1) with 53-bit resolution"""
        a = self.generate_int32() >> 5
        b = self.generate_int32() >> 6
        return (a * 67108864.0 + b) * (1.0 / 9007199254740992.0)

    def randbelow(self, n: int, maxsize=1<<BPF) -> int:
        """ return random int in range [0,n]."""
        if n == 0:
            return 0

        rem = maxsize % n
        limit = (maxsize - rem) / maxsize
        r = self.generate_real1()

        while r >= limit:
            r = self.generate_real1()
        return _floor(r * maxsize) % n


if __name__ == '__main__':
    init = (0x123, 0x234, 0x345, 0x456)
    m = Mersenne_Twister()
    m.init_by_array(init, len(init))

    for i in range(1000):
        r = m.generate_real2()
        print(r)
        if i % 5 == 4:
            print("\n")

