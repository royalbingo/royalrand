/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#define PY_SSIZE_T_CLEAN
#include <stdint.h>
#include <math.h>
#include <Python.h>
#include "structmember.h"
#include "../include/lcg.h"
#include "../include/pydef.h"
#include "../include/royalrand/lcg.h"


/* Linear Congruential Generator
 * -----------------------------
 */ 

static void LCG_dealloc(LCG_Type *self)
{
    lcg_free(self->ctx);
    Py_TYPE(self)->tp_free((PyObject*)self);
}

static PyObject* LCG_new(PyTypeObject *type, PyObject *args, PyObject *kwargs)
{
    LCG_Type *self;
    self = (LCG_Type*)type->tp_alloc(type, 0);
    
    if (self != NULL)
    {
        self->ctx = lcg_create();
        if (self->ctx == NULL) {
            Py_DECREF(self);
            return NULL;
        }
    }

    self->is_initialized = 0;
    return (PyObject*)self;
}

/* The output must be freed */
static char *get_repr_string(PyObject *self)
{
    if (((LCG_Type*)self)->is_initialized == 0) { return NULL; }
    size_t size = 0;
    char *buffer;
    
    size += (size_t)((ceil(log10(((LCG_Type*)self)->ctx->param.a))+1)*sizeof(char));
    size += (size_t)((ceil(log10(((LCG_Type*)self)->ctx->param.c))+1)*sizeof(char));
    size += (size_t)((ceil(log10(((LCG_Type*)self)->ctx->param.m))+1)*sizeof(char));
   
    if ((size + 64) < 0) {
        PyErr_SetString(PyExc_ValueError, "LCG: Parameters are too big to display.");
        return NULL;
    }

    buffer = calloc(1, size + 64);
    if (buffer == NULL) { return NULL; }

    PyOS_snprintf(buffer, size + 64, "<Linear Congruential Generator (a=%ld, c=%ld, m=%ld)>",
                ((LCG_Type*)self)->ctx->param.a,
                ((LCG_Type*)self)->ctx->param.c,
                ((LCG_Type*)self)->ctx->param.m
    );

    return buffer;
}

static PyObject* LCG_repr(PyObject *self)
{
    if (((LCG_Type*)self)->is_initialized == 0)
        return PyUnicode_FromString("<Linear Congruential Generator>");

    char *buffer = get_repr_string(self);
    if (buffer == NULL) { return NULL; }
    PyObject *string = PyUnicode_FromString(buffer);

    free(buffer);
    return string;
}

static int LCG_init(LCG_Type *self, PyObject *args, PyObject *kwargs)
{
    uint64_t a, c, m, seed;
    static char *kwlist[] = {"a", "c", "m", "seed", NULL};
    
    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "kkkk", kwlist, &a, &c, &m, &seed))
        return -1;
    
    /* uint64_t overflow/zero check */
    int overflow = 0;
    if ((a <= 0) || (a >= LCG_MAX_VALUE)) { overflow++; }
    if ((c <= 0) || (c >= LCG_MAX_VALUE)) { overflow++; }
    if ((m <= 0) || (m >= LCG_MAX_VALUE)) { overflow++; }
    if ((seed < 0) || (seed >= LCG_MAX_VALUE)) { overflow++; }

    if (overflow != 0) {
        PyErr_SetString(PyExc_ValueError, "Parameters should be a positive integer and not exceed 64 bit.");
        return -1;
    }

    lcg_set_param(self->ctx, a, c, m, seed);
    self->is_initialized = 1;

    return 0;
}

static PyObject *LCG__step(LCG_Type *self)
{
    return PyLong_FromUnsignedLong(lcg_step(self->ctx));
}

static PyObject *LCG__float_step(LCG_Type *self)
{
    return PyFloat_FromDouble(lcg_float_step(self->ctx));
}

static PyMemberDef LCG_members[] = {
    {NULL} /* sentinel */
};

static PyMethodDef LCG_methods[] = {
    {"step", (PyCFunction)LCG__step, METH_NOARGS, "Return the next step of LCG."},
    {"float_step", (PyCFunction)LCG__float_step, METH_NOARGS, "Return the next step of LCG in float."},
    {NULL} /* sentinel */
};

RR_NO_EXPORT PyTypeObject LCG = {
    PyVarObject_HEAD_INIT(NULL, 0)
    .tp_name = "royalrand.LCG",
    .tp_doc = PyDoc_STR("Linear Congruential Generator"),
    .tp_basicsize = sizeof(LCG_Type),
    .tp_itemsize = 0,
    .tp_flags = Py_TPFLAGS_DEFAULT,
    .tp_new = LCG_new,
    .tp_init = (initproc) LCG_init,
    .tp_dealloc = (destructor) LCG_dealloc,
    .tp_members = LCG_members,
    .tp_methods = LCG_methods,
    .tp_repr = (reprfunc) LCG_repr,
};
RR_NO_EXPORT PyTypeObject LCG;