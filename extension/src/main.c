/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#define PY_SSIZE_T_CLEAN
#include <stdint.h>
#include <Python.h>
#include "structmember.h"
#include "../include/pydef.h"
#include "../include/royalrand/mt19937c.h"
#include "../include/royalrand/mt19937.h"
#include "../include/royalrand/pool.h"
#include "../include/royalrand/hc256.h"
#include "../include/mersenne_twister.h"
#include "../include/hc256.h"
#include "../include/lcg.h"


static PyMethodDef methods[] = {
    // { "foo", foo, METH_NOARGS, FOO__doc__},
    {NULL, NULL, 0, NULL} /* sentinel */
};

static struct PyModuleDef module_definitions = {
    PyModuleDef_HEAD_INIT,
    PY_MOD_NAME,
    PY_MOD_DESC,
    -1,
    methods
};

PyMODINIT_FUNC PyInit_royalrand(void)
{
    PyObject *module;

    /* type registration */
    if (PyType_Ready(&MT19937C) < 0)
        goto mod_err;
    
    if (PyType_Ready(&MT19937) < 0)
        goto mod_err;

    if (PyType_Ready(&LCG) < 0)
        goto mod_err;
    
    if (PyType_Ready(&HC256) < 0)
        goto mod_err;

    /* module creation */
    module = PyModule_Create(&module_definitions);

    if (module == NULL)
        goto mod_err;

    /* MT19937C */
    Py_INCREF(&MT19937C);
    if (PyModule_AddObject(module, "MT19937C", (PyObject*)&MT19937C) < 0) {
        Py_DECREF(&MT19937C);
        goto err;
    }

    /* MT19937 */
    Py_INCREF(&MT19937);
    if (PyModule_AddObject(module, "MT19937", (PyObject*)&MT19937) < 0) {
        Py_DECREF(&MT19937);
        goto err;
    }

    /* LCG */
    Py_INCREF(&LCG);
    if (PyModule_AddObject(module, "LCG", (PyObject*)&LCG) < 0) {
        Py_DECREF(&LCG);
        goto err;
    }

    /* HC-256 */
    Py_INCREF(&HC256);
    if (PyModule_AddObject(module, "HC256", (PyObject*)&HC256) < 0) {
        Py_DECREF(&HC256);
        goto err;
    }

    return module;

err:
    Py_DECREF(module);
    return NULL;

mod_err:
    if (!PyErr_Occurred()) {
        PyErr_SetString(PyExc_RuntimeError, "cannot load module.");
    }
    Py_DECREF(module);
    return NULL;
}
