/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#define PY_SSIZE_T_CLEAN
#include <stdint.h>
#include <Python.h>
#include "structmember.h"
#include "../include/pydef.h"
#include "../include/royalrand/mt19937c.h"
#include "../include/royalrand/mt19937.h"
#include "../include/royalrand/pool.h"
#include "../include/mersenne_twister.h"


/* Custom Mersenne Twister
 * -----------------------
 * This uses a custom pool (pool32mt_t)
 * as internal state.
 */ 

static void MT19937C_dealloc(MT19937C_Type *self)
{
    mt19937c_free(self->ctx);
    Py_TYPE(self)->tp_free((PyObject*)self);
}

static PyObject* MT19937C_new(PyTypeObject *type, PyObject *args, PyObject *kwargs)
{
    MT19937C_Type *self;
    self = (MT19937C_Type*)type->tp_alloc(type, 0);
    
    if (self != NULL)
    {
        self->ctx = mt19937c();
        if (self->ctx == NULL) {
            Py_DECREF(self);
            return NULL;
        }
    }

    self->is_initialized = 0;
    return (PyObject*)self;
}

static PyObject* MT19937C_repr(PyObject *self)
{
    if (((MT19937C_Type*)self)->is_initialized == 0)
        return PyUnicode_FromString("<MT19937C [uninitialized]>");
    return PyUnicode_FromString("<MT19937C [initialized]>");
}

static int MT19937C_init(MT19937C_Type *self, PyObject *args, PyObject *kwargs)
{
    return 0;
}

static PyObject* MT19937C_init_genrand(MT19937C_Type *self, PyObject *args, PyObject *kwargs)
{
    printf("[DEBUG] Initializing MT19937C (parsing)...\n");
    uint64_t seed = 0;

    if (!PyArg_ParseTuple(args, "k", &seed))
    {
        PyErr_SetString(PyExc_ValueError, "Argument must be an unsigned integer.");
        return NULL;
    }

    printf("[DEBUG] Initializing MT19937C with seed %ld ...\n", seed);
    mtc_init_genrand(self->ctx, seed);
    self->is_initialized = 1;

    Py_RETURN_NONE;
}


static PyObject* MT19937C__safe_init(MT19937C_Type *self, PyObject *Py_UNUSED(ignored))
{
    if (self == NULL) {
        PyErr_SetString(PyExc_RuntimeError, "MT19937C is NULL!");
        return NULL;
    }

    if (self->is_initialized != 1) {
        mtc_safe_init(self->ctx);
        self->is_initialized = 1;
        Py_RETURN_TRUE;
    }
    Py_RETURN_FALSE;
}

/* This function will take only one argument as entropy.
 * The argument must be an integer containing the entropy.
 *
*/
static PyObject* MT19937C__add_entropy_from_long(MT19937C_Type *self, PyObject *args)
{
    uint64_t entropy = 0;
    if (!PyArg_ParseTuple(args, "k", &entropy))
    {
        //PyErr_SetString(PyExc_ValueError, "Argument must be an integer.");
        return NULL;
    }

    pool32mt_add_entropy(&self->ctx->pool, (const void*)&entropy, sizeof(uint64_t));

    Py_RETURN_NONE;
}

static PyObject *MT19937C__genrand_int32(MT19937C_Type *self)
{
#ifdef PROTECT_AGAINST_UNINITIALIZATION
    if (self->is_initialized == 0)
    {
        PyErr_SetString(PyExc_RuntimeError, "MT19937C must be initialized before you generate numbers.");
        return NULL;
    }
#endif

    /* mt19937c already checks for zero. Is this really necessary ?? */
    unsigned long value = 0;
    value = mtc_genrand_int32(self->ctx);
    if (value == 0)
    {
        /* TODO: Custom exception might be better here. */
        PyErr_SetString(PyExc_RuntimeError, "genrand_int32 returned 0.");
        return NULL;
    }
    return PyLong_FromUnsignedLong(value);
    // return PyLong_FromUnsignedLong(mtc_genrand_int32(self->ctx));
}

static PyObject *MT19937C__is_initialized(MT19937C_Type *self)
{
    if (self->is_initialized == 0)
        Py_RETURN_FALSE;
    Py_RETURN_TRUE;
}

static PyObject *MT19937C__genrand_res53(MT19937C_Type *self)
{
#ifdef PROTECT_AGAINST_UNINITIALIZATION
    if (self->is_initialized == 0)
    {
        PyErr_SetString(PyExc_RuntimeError, "MT19937C must be initialized before you generate numbers.");
        return NULL;
    }
#endif

    /* zero check already made by genrand_int32 (mt19937c.c) */
    return PyFloat_FromDouble(mtc_genrand_res53(self->ctx));
}

static PyObject *MT19937C__uniform32(MT19937C_Type *self, PyObject *args)
{
    uint32_t upper = 0;
    if (!PyArg_ParseTuple(args, "I", &upper)) {
        return NULL;
    }
#ifdef PROTECT_AGAINST_UNINITIALIZATION
    if (self->is_initialized == 0)
    {
        PyErr_SetString(PyExc_RuntimeError, "MT19937C must be initialized before you generate numbers.");
        return NULL;
    }
#endif
    return PyLong_FromLong(mtc_uniform32(self->ctx, upper));
}

static PyMemberDef MT19937C_members[] = {
    {NULL} /* sentinel */
};

static PyMethodDef MT19937C_methods[] = {
    {"is_initialized", (PyCFunction)MT19937C__is_initialized, METH_NOARGS, is_initialized__doc__},
    {"safe_init", (PyCFunction)MT19937C__safe_init, METH_NOARGS, mtc_safe_init__doc__},
    {"genrand_int32", (PyCFunction)MT19937C__genrand_int32, METH_NOARGS, mtc_genrand_int32__doc__},
    {"genrand_res53", (PyCFunction)MT19937C__genrand_res53, METH_NOARGS, mtc_genrand_res53__doc__},
    {"add_entropy_from_long", (PyCFunction)MT19937C__add_entropy_from_long, METH_VARARGS, mtc_add_entropy_from_long__doc__},
    {"uniform32", (PyCFunction)MT19937C__uniform32, METH_VARARGS, uniform32__doc__},
    {"init", (PyCFunction)MT19937C_init_genrand, METH_VARARGS, mtc_init__doc__},
    {NULL} /* sentinel */
};

RR_NO_EXPORT PyTypeObject MT19937C = {
    PyVarObject_HEAD_INIT(NULL, 0)
    .tp_name = "royalrand.MT19937C",
    .tp_doc = PyDoc_STR("MT19937 Custom"),
    .tp_basicsize = sizeof(MT19937C_Type),
    .tp_itemsize = 0,
    .tp_flags = Py_TPFLAGS_DEFAULT,
    .tp_new = MT19937C_new,
    .tp_init = (initproc) MT19937C_init,
    .tp_dealloc = (destructor) MT19937C_dealloc,
    .tp_members = MT19937C_members,
    .tp_methods = MT19937C_methods,
    .tp_repr = (reprfunc) MT19937C_repr,
};
RR_NO_EXPORT PyTypeObject MT19937C;

/* Mersenne Twister
 * -----------------------
 * This is the original implementation
 * of the MT19937; This uses a 624 vector
 * as internal state.
 */ 

static void MT19937_dealloc(MT19937_Type *self)
{
    mt19937_free(self->ctx);
    Py_TYPE(self)->tp_free((PyObject*)self);
}

static PyObject* MT19937_new(PyTypeObject *type, PyObject *args, PyObject *kwargs)
{
    MT19937_Type *self;
    self = (MT19937_Type*)type->tp_alloc(type, 0);
    
    if (self != NULL)
    {
        self->ctx = mt19937();
        if (self->ctx == NULL) {
            Py_DECREF(self);
            return NULL;
        }
    }

    self->is_initialized = 0;
    return (PyObject*)self;
}

static PyObject* MT19937_repr(PyObject *self)
{
    if (((MT19937_Type*)self)->is_initialized == 0)
        return PyUnicode_FromString("<MT19937 [uninitialized]>");
    return PyUnicode_FromString("<MT19937 [initialized]>");
}

static int MT19937_init(MT19937_Type *self, PyObject *args, PyObject *kwargs)
{
    uint64_t seed;
    static char *kwlist[] = {"seed", NULL};
    
    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "k", kwlist, &seed))
        return -1;
    
    mt_init_genrand(self->ctx, seed);
    self->is_initialized = 1;

    return 0;
}

static PyObject* MT19937__genrand_int32(MT19937_Type *self)
{
    return PyLong_FromUnsignedLong(mt_genrand_int32(self->ctx));
}

static PyObject* MT19937__genrand_res53(MT19937_Type *self)
{
    return PyFloat_FromDouble(mt_genrand_res53(self->ctx));
}

static PyObject *MT19937__uniform32(MT19937_Type *self, PyObject *args)
{
    uint32_t upper = 0;
    if (!PyArg_ParseTuple(args, "I", &upper)) {
        return NULL;
    }
    return PyLong_FromLong(mt_uniform32(self->ctx, upper));
}

static PyMemberDef MT19937_members[] = {
    {NULL} /* sentinel */
};

static PyMethodDef MT19937_methods[] = {
    {"genrand_int32", (PyCFunction)MT19937__genrand_int32, METH_NOARGS, mtc_genrand_int32__doc__},
    {"genrand_res53", (PyCFunction)MT19937__genrand_res53, METH_NOARGS, mtc_genrand_res53__doc__},
    {"uniform32", (PyCFunction)MT19937__uniform32, METH_VARARGS, uniform32__doc__},
    {NULL} /* sentinel */
};

RR_NO_EXPORT PyTypeObject MT19937 = {
    PyVarObject_HEAD_INIT(NULL, 0)
    .tp_name = "royalrand.MT19937",
    .tp_doc = PyDoc_STR("MT19937"),
    .tp_basicsize = sizeof(MT19937_Type),
    .tp_itemsize = 0,
    .tp_flags = Py_TPFLAGS_DEFAULT,
    .tp_new = MT19937_new,
    .tp_init = (initproc) MT19937_init,
    .tp_dealloc = (destructor) MT19937_dealloc,
    .tp_members = MT19937_members,
    .tp_methods = MT19937_methods,
    .tp_repr = (reprfunc) MT19937_repr,
};
RR_NO_EXPORT PyTypeObject MT19937;
