/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdlib.h>
#include "include/generic.h"

char *create_generic_context(size_t _size)
{
    char *context = (char*)malloc(_size * sizeof(char));
    
    if (context == NULL)
    {
        perror("Cannot create generic context. No enough memory!");
        return NULL;
    }

    return context;
}

void destroy_generic_context(char *__generic_context)
{
    if (__generic_context != NULL)
        free(__generic_context);
    __generic_context = NULL;
}