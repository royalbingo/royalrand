/* * Royal Rand - Crypto utility for Royal Bingo
 * Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
 *
 * This file is part of Royal Rand.
 *
 * Royal Rand is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Royal Rand is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.
 */

#define PY_SSIZE_T_CLEAN
#include <stdint.h>
#include <Python.h>
#include "structmember.h"
#include "../include/pydef.h"
#include "../include/hc256.h"
#include "../include/generic.h"
#include "../include/royalrand/hc256.h"
#include "../include/royalrand/macro.h"


static void HC256_dealloc(HC256_Type *self)
{
    Py_TYPE(self)->tp_free((PyObject*)self);
}

static PyObject* HC256_new(PyTypeObject *type, PyObject *args, PyObject *kwargs)
{
    HC256_Type *self;
    self = (HC256_Type*)type->tp_alloc(type, 0);
    hc_ctx *context = malloc(sizeof(hc_ctx));
    
    if (self != NULL)
    {
        self->ctx = context;
        if (self->ctx == NULL) {
            Py_DECREF(self);
            return NULL;
        }
    }

    self->is_initialized = 0;
    return (PyObject*)self;
}

static PyObject* HC256_repr(PyObject *self)
{
    if (((HC256_Type*)self)->is_initialized == 0)
        return PyUnicode_FromString("<HC256>");
    return PyUnicode_FromString("<HC256 [initialized]>");
}

static int HC256_init(HC256_Type *self, PyObject *args, PyObject *kwargs)
{
    struct {
        uint32_t key;
        uint32_t iv;
    } hc_key;

    static char *kwlist[] = {"key", "iv", NULL};
    
    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "II", kwlist, &hc_key.key, &hc_key.iv))
        return -1;

    hc256_setkey(self->ctx, &hc_key);

    self->is_initialized = 1;

    return 0;
}

static PyObject* HC256__crypt(HC256_Type *self, PyObject *args, PyObject *kwargs)
{
    uint32_t stream;

    if (!PyArg_ParseTuple(args, "I", &stream))
        return NULL;

    if (self->is_initialized == 0)
        Py_RETURN_NONE; 
    
    hc256_crypt(self->ctx, &stream, sizeof(uint32_t));

    return PyLong_FromUnsignedLong(stream);
}

static PyMemberDef HC256_members[] = {
    {NULL} /* sentinel */
};

static PyMethodDef HC256_methods[] = {
    {"encrypt", (PyCFunction)HC256__crypt, METH_VARARGS, hc256_encrypt_doc},
    {NULL} /* sentinel */
};

RR_NO_EXPORT PyTypeObject HC256 = {
    PyVarObject_HEAD_INIT(NULL, 0)
    .tp_name = "royalrand.HC256",
    .tp_doc = PyDoc_STR("HC-256 Stream cipher."),
    .tp_basicsize = sizeof(HC256_Type),
    .tp_itemsize = 0,
    .tp_flags = Py_TPFLAGS_DEFAULT,
    .tp_new = HC256_new,
    .tp_init = (initproc) HC256_init,
    .tp_dealloc = (destructor) HC256_dealloc,
    .tp_members = HC256_members,
    .tp_methods = HC256_methods,
    .tp_repr = (reprfunc) HC256_repr,
};
RR_NO_EXPORT PyTypeObject HC256;
