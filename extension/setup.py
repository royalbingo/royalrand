
# Royal Rand - Crypto utility for Royal Bingo
# Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
#
# This file is part of Royal Rand.
#
# Royal Rand is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Royal Rand is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.

from setuptools import Extension
from setuptools import setup
import os
import sys


SOURCES = [
    'main.c',
    'mersenne_twister.c',
    'lcg.c',
    'hc256.c',
]

STATIC_LIBRARIES = [
    'libroyalrand.a'
]

INCLUDES = [
    'include',
    'include/royalrand',
]

EXTRA_COMPILE_ARGS = [
    "-DPROTECT_AGAINST_UNINITIALIZATION",
]

module = Extension(
    "royalrand",
    sources=[os.path.join("src", x) for x in SOURCES],
    include_dirs=INCLUDES,
    extra_objects=STATIC_LIBRARIES,
    extra_compile_args=EXTRA_COMPILE_ARGS,
)

if __name__ == '__main__':

    if sys.platform == 'win32':
        print("Windows platform is not supported!", file=sys.stderr)
        exit(1)
    
    setup(
        name="royalrand",
        version="1.0.0",
        description="Cryptographically-secure Utility Library",
        author="Murilo Augusto",
        ext_modules=[module],
    )
