# Royal Rand - Library
This implements the **TODO** list of Royal Rand Cryptography Library.

### Core
- [X] Support for atomic operations.  `complex`
  - [X] Multithreading support for random pools operations.  
  - [X] Multiprocessing support for random pools operations.  

### Cryptographic algorithms/systems
- [ ] Elliptic-curve cryptography (ECC) support. `advanced`   
- [ ] PKI support. `advanced`
- [ ] Diffie–Hellman support. `advanced` 

### Random Number Generators
- [X] Kiss  `simple`
- [ ] Xorshift `simple`
- [ ] Dual_EC_DRBG  (legacy) `advanced`

### Cryptographic Hash Function (CHF) ✓
- [x] bCrypt  `simple`
