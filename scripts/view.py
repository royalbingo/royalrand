
# Royal Rand - Crypto utility for Royal Bingo
# Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
#
# This file is part of Royal Rand.
#
# Royal Rand is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Royal Rand is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.

import plotly.express as px
import math
import random
import rdrand
import time


UINT32_MAX = 0xffffffff
UINT32_MIN = 0x7fffffff


""" Middle-square method implementation """
class MiddleSquare:
    def __init__(self, seed, *, digits=4):

        if math.floor(math.log(seed, 10) + 1) != digits:
            raise ValueError(f"MiddleSquare seed must be length of {digits}.")

        self.seed = seed
        self.digits = digits

    def nextRand(self):
        n = str(self.seed ** 2)

        while len(n) < self.digits * 2:
            n = "0" + n

        start = math.floor(self.digits/2)
        end = start + self.digits
        self.seed = int(n[start:end])

        return self.seed

    def nextRandFloat(self) -> float:
        return self.nextRand() / ((10 ** self.digits) - 1)


""" Linear Congruential Generator implementation """
class LCG1:
    def __init__(self, a, c, m, seed):
        self.a = a
        self.c = c
        self.m = m
        self.seed = seed

    def nextRand(self):
        self.seed = (self.a * self.seed + self.c) % self.m
        return self.seed

    def nextRandFloat(self):
        return self.nextRand() / self.m

# Default conditions.
condition_float = lambda val: 1 if val < 0.5 else 0
condition_uint32 = lambda val: 1 if val < 0x7ffffff else 0

def create_array(_w, _h, method, condition, *, args=None) -> list:
    """ Create array (w x h) prepared for plotly.

        Args:
            _w, _h (int, int): width and height of array.

            method (callable): method to generate the random value.

            condition (callable): condition for frequency validation.
                                  It should return only 0 or 1.

            args (tuple, optional): Arguments to be passed to `method`
                                    to generating the random values.

    """

    result = []

    if not callable(method):
        raise ValueError("method must be callable.")

    if not callable(condition):
        raise ValueError("condition must be callable.")

    for w in range(_w):
        t = []
        for h in range(_h):
            r = method(*args) if args else method()
            t.append(condition(r))
        result.append(t)

    return result

if __name__ == '__main__':

    # plot setup.
    w, h, = [600] * 2

    # Generator setup.
    seed = 1002003004
    md = MiddleSquare(seed, digits=10)

    #_sysrand = random.SystemRandom()
    #lcg = LCG1(1664525, 101304223, 2**32, 958736)  # numerical recipes
    #lcg = LCG1(12345, 12345, 12345, 599665)  # poor 1

    # Time measurement of array creation.
    t0 = time.monotonic_ns()
    array = create_array(w, h, md.nextRandFloat, condition_float)
    tf = round((time.monotonic_ns() - t0) / 1e9, 2)  # ms

    # Label setup.
    title = f'Middle Square [{seed=}] (float) ({w}x{h}) [{tf} ms]'

    # Plot creation.
    fig = px.imshow(array, color_continuous_scale="gray", labels=dict(x=title))
    fig.write_image(f"{title.replace(' ', '_')}.jpeg", width=1080, height=1080, scale=6)
    fig.show()
