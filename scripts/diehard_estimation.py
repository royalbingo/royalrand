#!/usr/bin/env python3

# Royal Rand - Crypto utility for Royal Bingo
# Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
#
# This file is part of Royal Rand.
#
# Royal Rand is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Royal Rand is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.

import sys
import math

# Modify this for 32/64 bit integers.
INT = 32


DH_INT_DIGITS = len(str(int('0b' + ('1' * INT), 2)))
DH_HEADER_SEP = len("#==================================================================\n") * 2
DH_HEADER_TITLE = len("# MT19937c (royal rand)  seed = \n") + DH_INT_DIGITS
DH_HEADER_DESC = len(f"type: d\ncount: \nnumbit: {INT}\n") # + LEN_SAMPLES


def bad_args():
    print(f"Please rerun: {sys.argv[0]} <samples>", file=sys.stdout)
    exit(1)


def convert_size(size_bytes):

    if size_bytes == 0:
        return "0B"

    size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes / p, 2)

    return "%s %s" % (s, size_name[i])


def estimate_samples(samples: int):
    LEN_SAMPLES = len(str(samples))
    HEADER = DH_HEADER_SEP + DH_HEADER_TITLE + DH_HEADER_DESC + LEN_SAMPLES
    SAMPLES = (samples * DH_INT_DIGITS) + samples
    BYTES = HEADER + SAMPLES
    return BYTES


if __name__ == '__main__':

    if len(sys.argv) != 2:
        bad_args()

    arg1 = sys.argv[1].replace('.', '').replace(',', '')
    if not arg1.isdecimal():
        bad_args()

    samples = int(arg1)
    est_bytes = estimate_samples(samples)
    result = convert_size(est_bytes)
    print(f'{samples}: ~ {result} ({est_bytes} bytes)')
