
# Royal Rand - Crypto utility for Royal Bingo
# Copyright (C) 2021-2022 Murilo Augusto <murilo at bad1337.com>
#
# This file is part of Royal Rand.
#
# Royal Rand is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Royal Rand is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Royal Rand.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations
import random


POOL_SIZE = 512

def get_rand(_sz=32):
    return random.SystemRandom().getrandbits(_sz)

def printp(pool: Pool) -> None:
    for i in range(POOL_SIZE):
        if not i % 8:
            print("")
        print(f' {pool.pool[i]:<12}', end="")
    print("")

class Pool:
    def __init__(self):
        self.pool = [0xffffffff] * POOL_SIZE
        self.cursor = 0
        self.iteration = 0
        self.entropy_count = 0
        self.table = [0x0, 0x3b6e20c8, 0x76dc419, 0x4db26158,
                      0xedb88320, 0xd6d6a3e8, 0x9b64c2b0, 0xa00ae278]

    def init(self, _magiconst: int = 0x692fe) -> Pool:
        for i in range(POOL_SIZE):
            self.pool[i] = get_rand() ^ _magiconst; 
        return self

    def mix(self, magic: int) -> None:
        """ mix the pool """
        

    def add(self, buf, z=None) -> None:
        if z is None: z=0
        else: z = int(z) % POOL_SIZE

        self.mix(z)
        temp = buf
        temp = temp ^ self.pool[z]
        temp = temp ^ self.pool[(z + 1) % POOL_SIZE]
        temp = temp ^ self.pool[(z + 7) % POOL_SIZE]
        temp = temp ^ self.pool[(z + 14) % POOL_SIZE]
        temp = temp ^ self.pool[(z + 20) % POOL_SIZE]
        temp = temp ^ self.pool[(z + 26) % POOL_SIZE]
        temp = (temp >> 3) ^ self.table[temp & 7]
    
        print(f"p[{z:<3}] = {buf} -> {temp}")

if __name__ == '__main__':
    pool = Pool().init()
    for i in range(POOL_SIZE+10):
        pool.add(0, i)

